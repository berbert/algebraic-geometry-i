\section{Relative $\Spec$ Construction}
Let $\mathcal B$ quasi-coherent $\MO_X$-algebra, i.e. $\mathcal B$ is quasi-coherent $\MO_X$-module such that $\mathcal B(U)$ is endowed with $\MO_X(U)$-algebra structure for all open $U \subset X$ compatible with restriction.
\begin{proposition} \label{represent}
	There exists a scheme $\Spec \mathcal B$ and morphism $\pi \colon \Spec \mathcal B \to X$ such that for all $f \colon T \to X$ there are bijections (functorial in $T$) \[\Hom_X(T,\Spec \mathcal B)=\Hom_{\MO_X-\operatorname{Alg}}(\mathcal B,f_\ast\MO_T) \,. \]
	Hence $\Spec \mathcal B$ represents the functor \[T/X \longmapsto \Hom_{\MO_X-\operatorname{Alg}}(\mathcal B,f_\ast \MO_T) \,. \]
\end{proposition}
\begin{lemma} \label{lemEx}
	Let $X=\Spec A$, let $\F$ be an $\MO_X$-module and $M$ an $A$-module. Then \[\Hom_{\MO_X}(\widetilde M,\F) \xrightarrow{\Gamma(\Spec A,-)} \Hom_{A-\operatorname{Mod}}(M,\F(X)) \]
\end{lemma}
\begin{proof}
	The inverse is given as follows. Let $u \in \Hom_{A-\operatorname{Mod}}(M,\F(X))$. Define $\widetilde u \colon \widetilde M \to \F$ to be the morphism induced by $u_f \colon M_f \xrightarrow{(\operatorname{res}_{D(f)} \circ u)_f} \F(D(f))$. To show it's inverse consider \[\begin{tikzcd}
	M \arrow{r}{u} \arrow{d} & \F(X) \arrow{d} \\
	M_f \arrow{r}{\exists !} & \F(D(f))
	\end{tikzcd} \]
\end{proof}
\begin{proof}[Proof of \Cref{represent}]
	\textbf{Affine case.} Let $X=\Spec A$ and $B=\mathcal B(X)$ which is an $A$-algebra with $\widetilde B = \mathcal B$. 
	Define $\Spec \mathcal B \coloneqq \Spec B$ and $\pi \colon \Spec B \to \Spec A$ to be the map induced by $A \to B$. 
	
	Here, \[\Hom_X(T,\Spec B)=\Hom_{A-\operatorname{Alg}}(B,\MO_T(T)) =\Hom_{\MO_X-\operatorname{Alg}}(\widetilde B,f_\ast \MO_T) \] by \Cref{HomSch} and \Cref{lemEx} using $\MO_T(T)=(f_\ast\MO_T)(X)$. 
	
	\textbf{General case.} We define $Y=\Spec \mathcal B$ by gluing. Let $X=\bigcup U_i$ be an open affine cover with $U_i = \Spec A_i$. Let $\varphi_i \colon \res{\mathcal B}{U_i} \to \widetilde B_i$ be an isomorphism for an $A_i$-algebra $B_i=\mathcal B(U_i)$. Let $\pi_i \colon Y_i = \Spec B_i \to U_i$ .
	
	Let $V=\Spec(A^\prime) \subset U_i \cap U_j$ such that $V=D(a) \subset U_i=\Spec A_i$, $a \in A_i$ and $V=D(b)\subset U_j = \Spec A_j$, $b \in A_j$. 
	
	Note that $(B_i)_a \cong \mathcal B(V) \cong (B_j)_b$. This isomorphism is equal to $\res{\varphi_j}{V} \circ (\res{\varphi_i}{V})^{-1}$, so it satisfies the cocycle condition. Furthermore, $\res{Y_i}{V} = \pi_i^{-1}(V) \xrightarrow{\sim} \pi_j^{-1}(V)=\res{Y_j}{V}$. 
	
	Those $V$'s cover $U_i \cap U_j$. We get an isomorphism 
	\[
	\psi_{ji} \colon \res{Y_i}{U_i \cap U_j} \longrightarrow \res{Y_j}{U_i \cap U_j}\] Define $Y=(\bigsqcup_iY_i)/\sim_{\psi_{ji}}$. Here the cocycle condition for $\psi_{ji}$ is fulfilled (check). 
	
	Let $f \colon T \to X$ and $T_i = f^{-1}(U_i)$. Then 
	\begin{align*}
	&\Hom_X(T,\Spec \mathcal B) \\ =&\setdef{ \begin{tikzcd}[ampersand replacement = \&]
		T_i \arrow{rr}{h_i} \arrow{dr}[swap]{\res{f}{T_i}} \&\& Y_i \arrow{dl}  \\ \& U_i
		\end{tikzcd} \in \Hom_{U_i}(T_i, \Spec \res{\mathcal B}{U_i})}{\res{h_i}{T_i \cap T_j}=\res{h_j}{T_i \cap T_j}} \\
	=& \setdef{\widetilde h_i \in \Hom_{\MO_{U_i}-\operatorname{Alg}}(\widetilde B_i,(\res{f}{T_i})_\ast\MO_{T_i})}{\res{\widetilde h_i}{U_i \cap U_j}=\res{\widetilde h_j}{U_i \cap U_j}} \\
	= & \Hom_{\MO_X-\operatorname{Alg}}(\mathcal B,f_\ast\MO_T)
	\end{align*}
	since $\Hom_{\MO_X-\operatorname{Alg}}(\mathcal B,f_\ast \MO_T)$ is a sheaf. 
\end{proof}
\begin{remark} \label{algrem1}
	Let $\pi \colon \Spec \mathcal B \to X$. Then 
	\[
	\pi_\ast \MO_{\Spec \mathcal B}(U) = \MO_{\Spec \mathcal B}(\pi^{-1}(U))=\MO_{\Spec \mathcal B(U)}(\Spec \mathcal B(U))=\mathcal B(U)\]
	since $\res{\Spec \mathcal B}{U} = \Spec \mathcal B(U)$. Therefore, $\pi_\ast \MO_{\Spec \mathcal B}=\mathcal B$. 
\end{remark}
\begin{remark}
	We have \[\Hom_X(\Spec \mathcal B, \Spec \mathcal B^\prime)\cong \Hom_{\MO_X-\operatorname{Alg}}(\mathcal B^\prime,\pi_\ast \MO_{\Spec \mathcal B})= \Hom_{\MO_X-\operatorname{Alg}}(\mathcal B^\prime,\mathcal B) \,.\] Given $\varphi \colon \mathcal B^\prime \to \mathcal B$ a morphism of $\MO_X$-algebras, we get an induced morphism of schemes $\Spec(\varphi) \colon \Spec \mathcal B \to \Spec \mathcal B^\prime$. 
\end{remark}
\begin{definition}[affine morphism]
	A morphism $f \colon X \to Y$ is called \textbf{affine} is for every open affine $U \subset Y$, $f^{-1}(U)$ is also affine. 
\end{definition}
\begin{corollary} \label{eqVB}
	The functor \[\mathcal B \to \Spec \mathcal B \] defines a contravariant equivalence between 
	\begin{enumerate}
		\item category of quasi-coherent $\MO_X$-algebras
		\item category of $X$-schemes $\pi \colon E \to X$ such that $\pi$ is affine. 
	\end{enumerate}
\end{corollary}
\begin{proof}
	Define $\Phi \colon (\pi \colon E \to X) \mapsto \pi_\ast \MO_E$. By \Cref{algrem1} we have that $\Phi \circ \Spec \sim \id$. 
	
	Conversely, 
	\begin{align*}
	\Hom_{\MO_X-\operatorname{Alg}}(\pi_\ast \MO_E,\pi_\ast\MO_E) & \cong \Hom(E,\Spec \pi_\ast \MO_E) \\
	\id & \mapsto (\varphi \colon E \to \Spec \pi_\ast \MO_E)
	\end{align*} 
	Over $U=\Spec A$, $\res{\varphi}{U}\cong \id$. Therefore, $\varphi$ is an isomorphism.
\end{proof}
\section{Locally Free Sheaves and Vector Bundles II} \label{VecBunLocFr}
Let $\mathcal E$ be a locally free sheaf of rank $r$. Let \[\mathbb V(\mathcal E) \coloneqq \Spec (\Sym^\bullet (\mathcal E^\vee)) \] where $\Sym^\bullet \mathcal E$ is the sheaf associated to $U \mapsto \Sym^\bullet_{\MO_X(U)} \mathcal E(U)=\bigoplus_{k \ge 0}\mathcal E^{\otimes k}/\mathcal I$ where $\mathcal I$ is the ideal generated by elements of the form $a \otimes b - b \otimes a$. If $U$ is open affine, then $\Sym^\bullet(\mathcal E)(U) = \Sym^\bullet_{\MO_X(U)} \mathcal E(U)$ since $\mathcal E$ is quasi-coherent. 
\begin{lemma}
	$\mathbb V(\mathcal E)$ is a vector bundle of rank $r$. 
\end{lemma}
\begin{proof}
	Let $X=\bigcup_i U_i$ such that $U_i=\Spec A_i$ and $\varphi_i \colon \res{\mathcal E}{U_i} \to \MO_{U_i}^{\oplus r}$ is an isomorphism. 
	Consider \[\Sym^\bullet(\varphi_i^\vee) \colon \Sym^\bullet \MO_{U_i}^{\oplus r} = \MO_{U_i}[x_1, \dots , x_r] \coloneqq A[x_1, \dots , x_r] \xrightarrow{\sim} \Sym^\bullet(\res{\mathcal E^\vee}{U_i}) \] and \[\psi_i \colon \Spec(\Sym^\bullet(\varphi_i^\vee)) \colon \res{\mathbb V(\mathcal E)}{U_i} \xrightarrow{\sim} U_i \times \A^r = \Spec \MO_{U_i}[x_1, \dots , x_r] = \Spec A[x_1, \dots , x_r] \] On overlaps we have \[\begin{tikzcd}
	U_{ij} \times \A^r & \res{\mathbb V(\mathcal E)}{U_{ij}} \arrow{l}{\res{\psi_j}{U_{ij}}} \arrow{r}[swap]{\res{\psi_i}{U_{ij}}} & U_{ij} \times \A^r \arrow[bend right,swap]{ll}{\psi_{ji}=\psi_j \circ \psi_i^{-1}}
	\end{tikzcd} \]
	We have \begin{align*}
	\psi_{ji}^\# &= (\psi_i^{-1})^\# \circ \psi_j^\# = \Sym^\bullet(\varphi_i^\vee)^{-1} \circ \Sym^\bullet(\varphi_j^\vee) = \Sym^\bullet((\varphi_i^{-1})^\vee \circ \varphi_j^\vee) \\&= \Sym^\bullet((\varphi_j \circ \varphi_i^{-1})^\vee) 
	\end{align*} so linear over $U_{ij}$. 
\end{proof}
\begin{lemma}
	Consider \begin{align*}
	\mathbb V \colon \qquad \qquad \mathcal E & \longmapsto \mathbb V(\mathcal E)\\
	(\mathcal E \xrightarrow{\varphi} \F) & \longmapsto (\mathbb V(\mathcal E) \to \mathbb V(\F))
	\end{align*}
	defines a covariant equivalence between
	\begin{enumerate}
		\item locally free sheaves of rank $r$ on $X$ and
		\item vector bundles of rank $r$ on $X$.
	\end{enumerate}
	The inverse is given by taking sections. \[(\pi \colon E \to X) \longmapsto (\Hom_X(\bullet,E) \colon U \mapsto \Hom_X(U,E)=\setdef{s \colon U \to E}{\pi \circ s = \id_U}) \]
\end{lemma}
\begin{proof}
	By \Cref{eqVB} $\mathbb V$ is an equivalence with inverse $E \mapsto [\pi_\ast \MO_E]_{\deg 1}^\vee$.
	Also, $\Hom_U(U,\res{E}{U})=\Hom_U(U,\Spec \Sym^\bullet(\res{\mathcal E^\vee}{U})) = \Hom_{\MO_U-\operatorname{Alg}}(\Sym^\bullet(\res{\mathcal E^\vee}{U}),\MO_U)$. For all $A$-algebras $B$ and $A$-modules $M$ we have $\Hom_{A-\operatorname{Alg}}(\Sym^\bullet M, B)=\Hom_{A-\operatorname{Mod}}(M,B)$
\end{proof}
\begin{note}
	We often identify locally free sheaves with associated vector bundles and use them interchangeably. Same for invertible sheaves and line bundles. 
\end{note}
\begin{example}
	Let $X=\PR_\C^n=\Proj A$ where $A=\C[x_0,x_1, \dots , x_n]$. Consider \[\begin{tikzcd}
		0 \arrow{r} & A(-1) \arrow{r}{\varphi} & A \oplus \dots \oplus A \arrow{r} & Q \arrow{r} & 0 \\
		& 1 \arrow[mapsto]{r} & (x_0,x_1, \dots , x_n)
	\end{tikzcd} \]
	Applying $\sim$ we obtain \[\begin{tikzcd}
		0 \arrow{r} & \MO_X(-1) \arrow{r} & \MO_X^{\oplus n+1} \arrow{r} & \widetilde Q \arrow{r} & 0
	\end{tikzcd} \]
	with $\mathbb V(\MO_X^{\oplus n+1})=\Spec \MO_X[y_0, \dots , y_n] = \PR^n_\C \times \A_\C^{n+1}$. 
	What is $\mathbb V(\MO_X(-1)) \to \mathbb V(\MO_X^{\oplus n+1})=\PR^n \times \A^{n+1}$? Dualize \[\begin{tikzcd}
		0 & A(1) \arrow{l} & A \oplus \dots \oplus A \arrow{l}{\varphi^\vee} \\
		& x_i & y_i = (0,\dots , 1, \dots , 0) \arrow[mapsto]{l}
	\end{tikzcd} \]
	Here $\Sym^\bullet(A \oplus \dots \oplus A)=A[y_0, \dots , y_n]$ where $\deg y_i=0$. Furthermore, \[\Sym^\bullet (A(1))=A \oplus A(1) \oplus A(2) \oplus \dots = A \oplus At \oplus At^2 \oplus \dots = A[t] \] where $t$ is the a formal symbol of degree $-1$.
	
	Consider \begin{align*}
		\Sym^\bullet(\varphi^\vee)\colon \Sym^\bullet(A(1)) =A[t] & \longleftarrow A[y_0, \dots , y_n] \\
		tx_i & \longmapsfrom y_i \,.
	\end{align*}
	Apply $\sim$ and consider a basic open $D(x_0)$ \[\Sym^\bullet \MO_X^{\oplus n+1}(D(x_0)) = \widetilde{\Sym^\bullet A^{n+1}}(D(x_0)) = \C\left[\frac{x_1}{x_0}, \dots , \frac{x_n}{x_0} \right][y_0, \dots , y_n] \,.\]
	Furthermore, \begin{align*}
	(\Sym^\bullet \MO_X(1))(D(x_0))=&\widetilde{A[t]}(D(x_0)) =(A[t]_{x_0})_0 = \C\left[x_0, \dots , x_n,t, \frac{1}{x_0}\right]_0 \\
	=& \C \left[ \frac{x_1}{x_0}, \dots , \frac{x_n}{x_0},tx_0 \right] \end{align*} with $\lambda = tx_0$ 
	\begin{align*}
		\mathbb V(\varphi)^\# \colon \C\left[\frac{x_1}{x_0}, \dots , \frac{x_n}{x_0} \right][y_0, \dots , y_n] & \longrightarrow \C\left[\frac{x_1}{x_0}, \dots , \frac{x_n}{x_0}, \lambda\right] \\
		x_i/x_0 & \longmapsto x_i/x_0 \\
		y_0 & \longmapsto tx_0 =\lambda \\
		y_i & \longmapsto tx_i = tx_0 \frac{x_i}{x_0} = \lambda \frac{x_i}{x_0}
	\end{align*}
	Applying $\Spec$ we obtain 
	\begin{align*}
		D(x_0) \times \A_\lambda^1 & \longrightarrow D(x_0) \times \A^{n+1} \\
		\left(\left[1,\frac{x_1}{x_0}, \dots , \frac{x_n}{x_0}\right],\lambda\right) & \longmapsto \left(\left[1,\frac{x_1}{x_0}, \dots , \frac{x_n}{x_0}\right],\left(\lambda,\frac{x_1}{x_0} \lambda , \dots , \frac{x_n}{x_0} \lambda \right)\right)
	\end{align*}
	Here $\setdef{\left(\lambda,\frac{x_1}{x_0} \lambda , \dots , \frac{x_n}{x_0} \lambda \right)}{\lambda \in \C}=\setdef{\lambda(x_0, \dots , x_n)}{\lambda \in \C}$ is the line defined by the point $[x_0, \dots , x_n] \in \PR^n$. Cut out by equations $x_iy_j-x_jy_i$ for all $i,j$. 
	
	\textbf{Conclusion:} \[\mathbb V(\MO_X(-1)) \hookrightarrow \mathbb V(\MO_X^{n+1})=\PR^n \times \A^{n+1} \] is a closed immersion with image $Z=V(x_iy_j-x_jy_i;i,j)$ and $\mathbb V(\MO_X(-1))(\C)= \setdef{(p,v) \in \PR^n \times \C^{n+1}}{v \in L_p}$. Hence, $\mathbb V(\MO_X(-1))$ is the tautological line bundle. 
\end{example}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeo_master"
%%% End:
