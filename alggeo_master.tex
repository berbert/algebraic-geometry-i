\documentclass[oneside]{book}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{microtype} %praise microtype
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[colorlinks=true,linkcolor=blue]{hyperref}          
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{fancyhdr}
\usepackage{enumitem}
\usepackage{geometry}
\usepackage{cleveref}
\usepackage[onehalfspacing]{setspace}
\usepackage{pgf,tikz}
\usepackage{tikz-cd}
\usepackage{faktor}
\usepackage{comment}
\usepackage{mathrsfs}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows}
\usepackage{titlesec}
\usepackage{pgfplots}
\usepackage[useregional,showdow]{datetime2}
\usepackage{verbatim}
\usepackage{endnotes}

\makeatletter
\DeclareFontEncoding{LS1}{}{}
\DeclareFontSubstitution{LS1}{stix}{m}{n}
\DeclareMathAlphabet{\mathcal}{LS1}{stixscr}{m}{n}
\makeatother

\renewcommand{\subset}{\subseteq}
\renewcommand{\supset}{\supseteq}
\makeatletter
\tikzset{
	edge node/.code={%
		\expandafter\def\expandafter\tikz@tonodes\expandafter{\tikz@tonodes #1}}}
\makeatother
\tikzset{
	supseteq/.style={
		draw=none,
		edge node={node [sloped, allow upside down, auto=false]{$\supseteq$}}},
	Supseteq/.style={
		draw=none,
		every to/.append style={
			edge node={node [sloped, allow upside down, auto=false]{$\supseteq$}}}
	}
}

\DTMlangsetup[en-GB]{abbr}

\title{Algebraic Geometry I \& II -- Lecture Notes}
\author{Lecturer: Georg Oberdieck \\ Notes: Branko Juran and Berthold Lorke}
\date{}
\pagestyle{fancy}
\addtolength{\textheight}{2cm}
\fancyhead[L]{Algebraic Geometry \thepart}
\fancyhead[R]{Section \thesection}
\geometry{right=30mm, bottom=30mm, left=30mm, top=30mm}
\newtheorem{lemma}{Lemma}[chapter]
\newtheorem{theorem}[lemma]{Theorem}
\newtheorem{proposition}[lemma]{Proposition}
\Crefname{theorem}{Theorem}{Theorems}
\newtheorem{corollary}[lemma]{Corollary}
\newtheorem*{exercise}{Exercise}
\theoremstyle{definition}
\newtheorem{definition}[lemma]{Definition}
\newtheorem{notation}[lemma]{Notation}
\newtheorem{remark}[lemma]{Remark}
\newtheorem*{example}{Example}
\newtheorem*{note}{Note}
\newtheorem*{claim}{Claim}
\numberwithin{equation}{section}
\numberwithin{figure}{section}

\newcommand{\F}{\mathcal{F}}
\newcommand{\G}{\mathcal{G}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\PR}{\mathbb{P}}
\newcommand{\ai}{\mathfrak{a}}
\newcommand{\bi}{\mathfrak{b}}
\newcommand{\mi}{\mathfrak{m}}
\newcommand{\pid}{\mathfrak{p}}
\newcommand{\qi}{\mathfrak{q}}
\newcommand{\MO}{\mathcal O}
\newcommand{\A}{\mathbb A}
\newcommand{\MA}{\mathcal A}
\newcommand{\I}{\mathcal I}
\newcommand{\chara}{\operatorname{char}}
\newcommand{\UC}{\underline{\mathcal U}}
\newcommand{\VC}{\underline{\mathcal V}}
\newcommand{\dd}{\operatorname{d}}
\newcommand{\pt}{\mathrm{pt}}
\newcommand{\Extc}{\mathcal{E\mkern-4.5mu x\mkern-2.5mu t\mkern-1mu}}
\newcommand{\Homc}{\mathcal{H\mkern-7mu o\mkern-2.5mu m\mkern-1.5mu}}
\newcommand{\cat}[1]{\mathbf{#1}}
\newcommand{\strich}{\textrm{--}}
\renewcommand{\L}{\mathcal L}
\newcommand{\Ca}{\mathcal{C}}
\newcommand{\Usmo}{U_{\operatorname{smooth}}}
\newcommand{\E}{\mathcal E}

\DeclareMathOperator{\Spec}{Spec}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\Maps}{Maps}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\can}{can}
\DeclareMathOperator{\supp}{supp}
\DeclareMathOperator{\GL}{GL}
\DeclareMathOperator{\End}{End}
\DeclareMathOperator{\im}{Im}
\DeclareMathOperator{\Set}{\textbf{Set}}
\DeclareMathOperator{\obj}{obj}
\DeclareMathOperator{\Frac}{Frac}
\DeclareMathOperator{\RatMap}{RatMap}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\topdim}{topdim}
\DeclareMathOperator{\Nil}{Nil}
\DeclareMathOperator{\Jac}{Jac}
\DeclareMathOperator{\Sch}{\textbf{Sch}}
\DeclareMathOperator{\op}{op}
\DeclareMathOperator{\Aff}{\textbf{Aff}}
\DeclareMathOperator{\Ring}{\textbf{Ring}}
\DeclareMathOperator{\Gal}{Gal}
\DeclareMathOperator{\codim}{codim}
\DeclareMathOperator{\Proj}{Proj}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\Sym}{Sym}
\DeclareMathOperator{\Der}{Der}
\DeclareMathOperator{\pr}{pr}
\DeclareMathOperator{\cha}{char}
\DeclareMathOperator{\Bl}{Bl}
\DeclareMathOperator{\Tot}{Tot}
\DeclareMathOperator{\Ann}{Ann}
\DeclareMathOperator{\Ass}{Ass}
\DeclareMathOperator{\Pic}{Pic}
\DeclareMathOperator{\Div}{Div}
\DeclareMathOperator{\CaCl}{CaCl}
\DeclareMathOperator{\cro}{CrossRatio}
\DeclareMathOperator{\PGL}{PGL}
\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\spann}{span}
\DeclareMathOperator{\Ext}{Ext}
\DeclareMathOperator{\td}{td}
\DeclareMathOperator{\CaDiv}{CaDiv}
\DeclareMathOperator{\Cl}{Cl}
\DeclareMathOperator{\Mod}{Mod}
\DeclareMathOperator{\an}{an}
\DeclareMathOperator{\hol}{hol}
\DeclareMathOperator{\Coh}{Coh}
\DeclareMathOperator{\Gr}{Gr}
\DeclareMathOperator{\Tor}{Tor}
\DeclareMathOperator{\depth}{depth}


\newcommand{\res}[2]{\left. #1 \right|_{#2}}
\newcommand{\set}[1]{\left\{#1\right\}}
\DeclarePairedDelimiter{\card}{\lvert}{\rvert}
\DeclarePairedDelimiter{\amount}{\#}{}
\newcommand{\setdef}[2]{\left\{#1 \,\middle|\, #2\right\}}

\newcommand{\pol}[4]{#1[#2_#3,\dotsc,#2_#4]}
\newcommand{\orbit}[2]{#1 \mathrel{\backslash\!\!\backslash} #2}
\setlist[enumerate]{label=(\alph*)}

\begin{document}
% \newcommand{lecturebegin}[4]{\vspace{5mm}Lecture on \DTMdisplaydate{#1}{#2}{#3}{#4}.\vspace{5mm}}
\large
\setlength{\parindent}{0mm}
\fancyheadoffset{0 cm} 
\maketitle
\setcounter{page}{2}
\setcounter{chapter}{-1}
\tableofcontents
% \lecturebegin{2018}{10}{08}{1}
\input{alggeoI_2018-10-11}
\input{alggeoI_2018-10-15}
\input{alggeoI_2018-10-18}
\input{alggeoI_2018-10-22}
\input{alggeoI_2018-10-25}
\input{alggeoI_2018-10-29}
\input{alggeoI_2018-11-05}
\input{alggeoI_2018-11-08}
\input{alggeoI_2018-11-12}
\input{alggeoI_2018-11-15}
\input{alggeoI_2018-11-19}
\input{alggeoI_2018-11-22}
\input{alggeoI_2018-11-26}
\input{alggeoI_2018-11-29}
\input{alggeoI_2018-12-03}
\input{alggeoI_2018-12-06}
\input{alggeoI_2018-12-10}
\input{alggeoI_2018-12-13}
\input{alggeoI_2018-12-17}
\input{alggeoI_2018-12-20}
\input{alggeoI_2019-01-07}
\input{alggeoI_2019-01-10}
\input{alggeoI_2019-01-14}
\input{alggeoI_2019-01-17}
\input{alggeoI_2019-01-21}
\input{alggeoI_2019-01-24}
\input{alggeoI_2019-01-28}
\input{alggeoI_2019-01-31}
\input{alggeoII_2019-04-01}
\input{alggeoII_2019-04-04}
\input{alggeoII_2019-04-08}
\input{alggeoII_2019-04-11}
\input{alggeoII_2019-04-15}
\input{alggeoII_2019-04-18}
\input{alggeoII_2019-04-25}
\input{alggeoII_2019-04-29}
\input{alggeoII_2019-05-02}
\input{alggeoII_2019-05-06}
\input{alggeoII_2019-05-09}
\input{alggeoII_2019-05-13}
\input{alggeoII_2019-05-16}
\input{alggeoII_2019-05-20}
\input{alggeoII_2019-05-23}
\input{alggeoII_2019-05-27}
\input{alggeoII_2019-06-03}
\input{alggeoII_2019-06-06}
\input{alggeoII_2019-06-17}
\input{alggeoII_2019-06-24}
\input{alggeoII_2019-06-27}
\input{alggeoII_2019-07-01}
\input{alggeoII_2019-07-04}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
