\chapter{Projective Spectrum}
\section{The Zariski Topology on $\Proj A$}
\begin{definition}[graded ring]
  A \textbf{graded ring} is a ring $A$ with a direct decomposition $A = \bigoplus_{d=0}^\infty A_d$ into abelian groups such that $A_d \cdot A_e \subseteq A_{d+e}$ for all $d,e \geq 0$.
  Elements $f \in A_d$ are called \textbf{homogeneous of degree $\deg(f) = d$}.
\end{definition}
\begin{example}~ \vspace{-\topsep}
  \begin{enumerate}
  \item $A = \pol{k}{x}{0}{n}$ with $\deg x_i = a_i \geq 0$, usually
    $\deg x_i = 1$.
  \item Let $A$ be a graded algebra and $I \subseteq A$ a homogeneous ideal.
    Then $A/I$ is a graded algebra via $(A/I)_d = A_d / I_d$, where $I_d = A_d \cap I$.
  \item
    Let $A$ be a graded algebra and  $f \in A$ be homogeneous.
    Then the localization $A_f$ is graded via $(A_f)_d = \setdef{\frac{a}{f^l}}{a \in A \text{ homogeneous, } \deg(a) - l \deg(f) = d}$
    $A_f = \bigoplus_{d \in \Z} (A_f)_d$ is a $\Z$-graded ring.
    A little bit of a dangerous notation:  $A_{(f)} \coloneqq (A_f)_0$, the degree zero part of $f$.

    Example for this:  $A = \pol{k}{x}{0}{n}$, $(A_{x_i})_0 = k[\frac{x_0}{x_i},\dotsc,\frac{x_n}{x_i}]$.
  \end{enumerate}
\end{example}
\begin{definition}[projective spectrum]
  Let $A$ be a graded ring.
  Let $A_+ \coloneqq \bigoplus_{d >0} A_d$ and let \[\Proj A \coloneqq \setdef{\pid \subseteq A}{\pid \text{ is a homogeneous prime ideal s.t. } A_+ \not\subseteq \pid} \,.\] be the \textbf{projective spectrum of $A$}.
\end{definition}
\begin{example}~ \vspace{-\topsep}
  \begin{enumerate}
  \item $A = k[x]$, $\deg x = 1$, $A_+ = (x)$, $\Proj A = \set{(0)}$
  \item $A = A_0$, $A_+ = (0)$, $\Proj A = \emptyset$.
  \item $A = k[x,y]$, $k$ algebraically closed.
    Then $$\Proj A \subseteq \Spec A = \set{(0)} \sqcup  \setdef{(f)}{f \in k[x,y] \text{ irred.}} \sqcup \setdef{(x-a,y-b)}{a,b \in k} \, .$$
    A maximal ideal $(x-a,y-b)$ is homogeneous iff $a=b=0$, $A_+ \subseteq (x,y) \notin \Proj A$.
    $(0)$ is homogeneous, and $(f)$ with $f$ irreducible is homogeneous iff $f$ is homogeneous.
  \end{enumerate}
\end{example}
\begin{lemma}
  Let $k$ be algebraically closed.
  Let $0 \neq f \in k[x,y]$.
  Then $f$ is homogeneous and reducible iff $f = a_0x + a_1y$ for some $(a_0,a_1) \in k^2\setminus \set{(0,0)}$.
\end{lemma}
\begin{proof}
  Let $f$ be homogeneous and irreducible.
  If $f = \sum_{i=0}^d a_i x^{d-i}$, then $\hat f = \sum_{i=0}^d a_i (\frac{x}{y})^{d-i}y^i$ is irreducible in $k\left[\frac{x}{y}\right]$, hence of degree $\leq 1$.
\end{proof}
\begin{example}~ \vspace{-\topsep}
  \begin{enumerate}
  \item $k[x,y]$ with $k$ algebraically closed. Then
    \[\Proj A = \set{(0)} \sqcup \setdef{(-bx + ay)}{(a,b) \in
      (k^2\setminus \set{(0,0)}) / k^\times}\] and 
    $\Proj A \subseteq \Spec A= \mathbb A^2_k$.
    %[Picture]
  \item
    $A = \R[x,y]$.
    Then $\Proj A = \set{(0)} \sqcup \setdef{(-bx +ay)}{(a,b) \in (\R^2\setminus \set{(0,0)}) / \R^\times} \sqcup \setdef{(-bx +ay)(-\bar b x + \bar a y)}{(a,b) \in (\C\setminus \R)^2 / \R^\times}$
  \item
    $A = k[x,y,z]$, $k$ algebraically closed, conic $x^2 + y^2 = 0$ in $\mathbb{A}^2_\R$. Then
    \begin{align*} 
    	\Proj k[x,y,z] \subseteq \Spec k[x,y,z] =& \set{(0)} \sqcup  \setdef{(f)}{f \in k[x,y] \text{ irred.}} \\ &\sqcup \setdef{p \subseteq k[x,y,z]}{\pid \text{ prime}, \operatorname{codim} \pid = 2} \\ & \sqcup \setdef{(x-a,y-b,z-c)}{(a,b,c) \in l^3}
    \end{align*}
    Which ones in the last set are homogeneous?
  \end{enumerate}
\end{example}
\begin{lemma}
  The ideal $\pid \subseteq k[x,y,z]$ is a homogeneous prime ideal of codimension $2$ iff $\pid = \pid_{a,b,c} = (bx-ay,cx-az,cy-bz)$.
\end{lemma}
\begin{proof}[``Proof'' (by picture)]
  The fact that $\pid$ is homogeneous implies $\pid = (f_1,\dotsc,f_l)$ with $f_i$ homogeneous.
  Since $f_i$ is homogeneous, $f_i(tx) = t^{\deg f_i} f_i (x)$ for all $t \in k$.
  We have $x \in V(\pid)$ iff $f_i (x) = 0$ for all $i$.
  Then $f_i(tx) = 0$ for all $i$, which implies $tx \in V(\pid)$ for all $t \in k$.
  Let $x_0 \in V(\pid)$ be a closed point.
  Let $L_{x_0} = \setdef{tx_0}{t \in k} \subseteq V(\pid)$.
  But $V(\pid)$ has dimension $1$ and is irreducible.
  Hence, $L_{x_0} = V(\pid)$.
  %[PICTURE]
\end{proof}
Therefore in the example we have \[\Proj k[x,y,z] = \set{(0)} \sqcup  \setdef{(f)}{f \in k[x,y] \text{ irred.}} \sqcup \setdef{p_{a,b,c}}{a,b,c \in (k^3\setminus \set{0}) / k^\times} \,. \]
%\section{$\Proj$ as a Scheme}

Let $A$ be a graded ring, $\ai \subseteq A$ a homogeneous ideal, $V(\ai) = \setdef{\pid \in \Proj A}{\ai \subseteq \pid}$.

\begin{lemma}[Zariski topology]
  The collection of subsets $\setdef{V(\ai)}{\ai \subseteq A \text{ homogeneous}}$ are the closed subsets of a topology on $\Proj A$, called the \textbf{Zariski topology}.
\end{lemma}

\begin{definition}[basic open subsets]
  Let $f \in A$ be homogeneous.
  Then \[D(f) \coloneqq \setdef{\pid \in \Proj A}{f \notin \pid}\] is called a \textbf{basic open subset} of the Zariski topology on $\Proj A$.
\end{definition}
\begin{lemma}
  The $D(f)$'s form a basis of the Zariski topology.
\end{lemma}
Consider $A \xrightarrow{\mathrm{loc}} A_f \supseteq (A_f)_0$.
\begin{proposition}
  The map
  \begin{align*}
    \Psi \colon D(f) &\longrightarrow \Spec (A_f)_0  \\
    \pid &\longmapsto (\pid A_f)_0 = \pid A_f \cap (A_f)_0
  \end{align*}
  is a homeomorphism.
\end{proposition}
\begin{proof} We construct an inverse $\Theta$.
  Let $\qi \in \Spec (A_f)_0$.
  Let $\tilde \qi_d = \setdef{a \in (A_f)_d}{\frac{a^{\deg f}}{f^d} \in \qi}$.
  We have $\tilde \qi = \bigoplus_{d \in \Z} \tilde \qi_d \subseteq A_f$ is a homogeneous prime ideal.
  Let $\Theta (\qi) = \varphi^{-1}(\tilde \qi)$, where $\varphi \colon A \to A_f$ is the localization.

  Now $\Psi(\Theta(\qi)) = \tilde \qi \cap (A_f)_0 = \setdef{a \in (A_f)_0}{\frac{a^{\deg f}}{f^0} \in \qi} = \qi$, as $\frac{a^{\deg f}}{f^0} \in \qi \iff a \in \qi$.

  Now to show is $ \Theta (\Psi (\pid)) = \pid$.

  ``$\supseteq$'':  $a \in \pid_d \implies \frac{a^{\deg f}}{f^d} \in \qi \implies a \in \Theta(\Psi(\pid))$.

  ``$\subseteq$'': Let $a \in A_d$ such that $\frac{a^{\deg f}}{f^d} \in \qi$.
  Then $a^{\deg f} \in \pid A_f \implies a \in \pid A_f \implies a \in \pid$.

  So now we have shown that $\Psi$ is bijective.
  Let $g \in A$ be homogeneous such that $D(g) \subseteq D(f)$.
  Then
  \begin{align*}
    \pid \in D(g) &\iff g \notin \pid \\
               &\iff g \notin \pid A_f \\
               &\iff \bar g = \frac{g^{\deg f}}{f^{\deg g}} \notin (\pid A_f)_0 \; ,
  \end{align*}
  therefore $\Psi(D(g)) = D(\bar g) \subseteq \Spec (A_f)_0$.
\end{proof}
\begin{example}
  $A = \pol{k}{x}{0}{n}$, $f = x_i$.
  Then $\Proj A \supseteq D(x_i) \stackrel{\Psi}{\cong} \Spec k\left[\frac{x_0}{x_i}, \dotsc,\frac{x_n}{x_i}\right]$.
  $A_+ \not\subseteq \pid$ for all $\pid \in \Proj A \implies \Proj A = \bigcup_i D(x_i)$.
\end{example}
\section{The Structure Sheaf on $\Proj A$}
%$D(f) \cong \Spec (A_f)$ (homeo).
Let $\mathfrak B = \setdef{D(f)}{f \in A \text{ homogeneous}}$ which is a basis.
Define a presheaf on $\mathfrak B$ by $\MO_{\Proj A}^{\mathfrak{B}} (D(f)) = (A_f)_0$.

\begin{lemma}
  If $D(g) \subseteq D(f)$, $g$ homogeneous, then $\MO_{\Proj A}^{\mathfrak B} (D(g)) = \MO_{\Spec (Af)_0} (D(\bar g))$, where $\bar g = \frac{g^{\deg f}}{f^{\deg g}}$.
\end{lemma}
\begin{proof}
  Right hand side $ = ((A_f)_0)_{\bar g} = ((A_f)_g)_0 = (A_g)_0$ as $g^m = f\cdot a$ for some $m \geq 1$.
\end{proof}
\begin{corollary}
  $\Psi_\ast \colon \res{\MO^{\mathfrak B}_{\Proj A}}{D(f)} = \MO_{\Spec (A_f)_0}$ (considered as a sheaf on the basic open subsets of $\Spec (A_f)_0$.)
  Hence $\MO_{\Proj A}^{\mathfrak B}$ is a sheaf on $\mathfrak B$.
\end{corollary}
\begin{definition}[structure sheaf on $\Proj A$]
  The structure sheaf on $\Proj A$ is the sheaf associated to $\MO_{\Proj A}^{\mathfrak B}$.
\end{definition}
\begin{lemma}
  $(\Proj A ,\MO_{\Proj A})$ is a scheme
\end{lemma}
\begin{proof}
  $\left(D(f),\res{\MO_{\Proj A}}{D(f)}\right) \cong \Spec (A_f)_0$ (as locally ringed spaces).
\end{proof}
\begin{definition}[projective space]
  Let $R$ be a ring.
  Then $\mathbb{P}_R^n \coloneqq \Proj \pol{R}{x}{0}{n}$ with $\deg x_i = 1$ is the \textbf{projective $n$-space over $R$}.
  $\PR_R^n = \bigcup_{i=0}^n D(x_i)$.
  \[\left(D(x_i), \res{\MO_{\PR_R^n}}{D(x_i)}\right) \cong \Spec R\left[\frac{x_0}{x_i},\dotsc\frac{x_n}{x_i}\right] = \mathbb{A}^n_R \]
\end{definition}
\begin{theorem}
  $\PR_R^n \to \Spec R$ is proper.
\end{theorem}
\begin{example}~ \vspace{-\topsep}
  \begin{enumerate}
  \item
    Let $I \subseteq A$ be homogeneous.
    Then $\Proj(A/I) \hookrightarrow \Proj A$ is a closed immersion.
  \item
    Warning: Not every graded ring homomorphism $\varphi \colon A \to B$ induces a morphism $\Proj B \to \Proj A$.

    Example: $\varphi \colon k[x,y] \to k[x,y]$, $x \mapsto x$, $y \mapsto 0$.
    Here  $\varphi^{-1}((-bx+ay)) = (0)$ if $a \neq 0$, but $\varphi^{-1}((x)) = (x,y) \notin \Proj k[x,y]$.
    %[PICTURE]
  \end{enumerate}
\end{example}
\begin{definition}[weighted projective space]
  Let $A = \pol{k}{x}{0}{n}$, $\deg x_i = a_i \geq 1$.

  Then we define $\PR^n(a_0,\dotsc,a_n) \coloneqq \Proj A$, the \textbf{weighted projective space}.
\end{definition}
\begin{example}
  $\PR(1,1,n) = \Proj k[x,y,z]$, $\deg x = \deg y = 1$, $\deg z = n$.

  $D(x) = \Spec k\left[x,y,z, \frac{1}{x}\right]_0 = \Spec k\left[\frac{y}{x},\frac{z}{x^n}\right] = \A^2_k$, $D(y) \cong \A^2_k$, $D(z) = \Spec k\left[x,y,z,\frac{1}{z}\right]_0$.

  $x^ay^bz^c \in k[x,y,z,\frac{1}{z}]_0 = k\left[X^n,X^{n-1}Y,\dotsc,Y^n\right]$ (where $X = \frac{x}{z^{1/n}}$, $Y=\frac{y}{z^{1/n}}$.) $\iff a,b \geq 0, \, a+b+cn = 0$.
  If $a+b+cn = 0$, then $a+b \equiv 0 \mod n$.

  Case $n=2$:  $D(z) = \Spec k[X^2,XY,Y^2] = k[x_0,x_1,x_2]/ (x_1^2 - x_0x_2)$.
  \begin{figure}[h!]
  	  \centering
  	\begin{tikzpicture}
  	\draw (1,0) -- (-1,3);
  	\draw (-1,0) -- (1,3);
  	\filldraw[fill=white,draw=black] (0,0) ellipse (1 and 0.5);
  	\filldraw[fill=white,draw=black] (0,3) ellipse (1 and 0.5);
  	\end{tikzpicture}
  	\caption{$\Spec k[X^2,XY,Y^2] = \Spec k[x_0,x_1,x_2]/(x_1^2-x_0x_2)$ in $k^3$}
  \end{figure}
	
  Case $n\geq 3$:  ($k = \C$)  $\C[X^n,X^{n-1}Y,X^{n-2}Y^2,\dotsc,Y^n]=k[X,Y]^{\Z_n}$.
  %$\Spec k[X,Y]^{\Z_n}$  \cong \C^2/\Z_n"$, which is not defined.
  $\Z_n$ acts on $k[X,Y]$ via $X \mapsto \xi X$, $Y \mapsto \xi Y$, $\xi = e^{2\pi i /n}$.
\end{example}

Question:  Why is $A$ non-negatively graded for $\Proj$?

\begin{example}
  $A = \C[x,y]$, $\deg x = 1$, $\deg y = -1$.
  Then \["\Proj" A = \set{\text{homog. prime ideals in } \C[x,y]} = \set{(0)} \sqcup \setdef{(xy-a)}{a \in \C} \sqcup \set{(x)} \sqcup \set{(y)} \, .\]
  An ideal $\pid \subseteq \C[x,y]$ is a homogeneous prime ideal iff it $V(\pid)$ is an irreducible closed subset of $\C^2$ which is invariant under pointwise scalar multiplication.
  \begin{figure}[h!]
  	\centering
  	\begin{tikzpicture}
  	\draw[->] (0,0) -- (3.5,0) node[below] {$x$};
  	\draw[->] (0,-1.2) -- (0,3.5) node[above] {$y$};
  	
  	\draw[domain=-0.1:3,color=red] plot (\x,0) node[below left] {$(y)$};
  	\draw[domain=0.25:4,color=orange] plot (\x,{1/\x}) node[right] {$(xy-1)$};
  	\draw[domain=0.5:4,color=orange] plot (\x,{2/\x}) node[above right] {$(xy-a)$, $a \in \C$};
  	\draw[red] (0,-0.1) -- (0,3) node[left] {$(x)$};
  	\end{tikzpicture}
  	\caption{$"\Proj" \C[x,y]$}
  \end{figure}
  $D(x) = \Spec \C\left[x,y,\frac{1}{x}\right]_0 = \Spec \C[xy] \cong \A^1_\C$.
  $D(y) = \Spec \C[xy] \cong \A^1_\C$.
  $D(x) \cap D(y) \cong \A^1_\C \setminus \set{0}$, $"\Proj" A = \A^1_\C \cup_{\A^1_\C \setminus \set{0}} \A^1_\C$, the double-origin line.
  $"\Proj" A "=" (\C\setminus \set{0})^2/\C^\ast$ which is not seperated.
\end{example}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeo_master"
%%% End:
