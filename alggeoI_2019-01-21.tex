\chapter{Regularity and Smoothness}
\section{Regularity}
Suggested references for this chapter
\begin{itemize}
	\item Vakil
	\item Qing Liu -- Algebraic Geometry and Arithmetic Curves
\end{itemize}
Recall \Cref{Krull2}.

Let $A$ be a Notherian ring, $X=\Spec A$. Let $f_1, \dots , f_r \in A$ and let $Z$ be irreducible component of $V(f_1, \dots , f_r)$. Then $\codim(Z,X) \le r$.  
\begin{proposition} \label{ineqCTS}
	Let $A$ be a Noetherian local ring, $\mi \subset A$ be the maximal ideal and $k = A/m$. Then \[\dim A \le \dim_k(\mi/\mi^2)) \,. \]
\end{proposition}
\begin{proof}
	Let $\overline f_1, \dots , \overline f_r \in \mi/\mi^2$ be a basis. Let $f_1, \dots , f_r \in \mi$ be a lift of this basis. By Nakayama's Lemma, $\mi = (f_1, \dots , f_r)$. Therefore, $Z=V(f_1, \dots , f_r) = \Spec (A/(f_1, \dots , f_r)) = \Spec k = \{\bullet \}$. Hence, $\dim X = \codim(Z,X) \le r$ by Krull.  
\end{proof}
\begin{definition}[Zariski cotangent space]
	In the situation of \Cref{ineqCTS}, we say that $\mi/\mi^2$ is the \textbf{Zariski cotangent space of $A$}.
\end{definition}
\begin{definition}[regular]
	A Noetherian local ring $A$ is called \textbf{regular} if \[\dim A = \dim_k (\mi/\mi^2) \,. \]
\end{definition}
\begin{example}
	If $\dim A = 0$, then $A$ is regular $\iff$ $\mi/\mi^2=0$ $\iff$ $\mi=0$ $\iff$ $A$ is a field. 
	
	$k[x]/(x^2)$ is not regular. 
\end{example}
\begin{definition}[regular scheme]
	A locally Noetherian scheme $X$ is 
	\begin{itemize}
		\item \textbf{regular at $\pid \in X$} if $\MO_{x,\pid}$ is regular. 
		\item \textbf{regular} if it is regular for all $\pid \in X$.
		\item \textbf{singular at $\pid \in X$} if it is not regular at $\pid$.
	\end{itemize}
\end{definition}
\begin{note}
	Do not use ``smooth''. It's a different concept.
\end{note}
\begin{example}~ \vspace{-\topsep}
	\begin{itemize}
		\item $\Spec k$ is regular
		\item $X=\A_k^1=\{(0)\} \sqcup \setdef{(f)}{f \in k[x] \text{ irreducible}}$. We have $\MO_{X,(0)}=k(x)$ which is regular. Furthermore, $\MO_{X,(f)}=k[x]_{(f)}$ and $(f)/(f^2) \cong k((f))$. So $\dim \MO_{X,(f)} = 1 = \dim ((f)/(f^2)$, so $\MO_{X,(f)}$ is also regular. 
		
		All in all, $\A_k^1$ is regular. 
		\item $C$, a curve over $k$. Again $\MO_{C;\eta}=K$ is regular for the generic point $\eta$. 
		Let $x \in C$ be a closed point. Is $\MO_{X,x}$ regular? Using \Cref{regularCurve}, we see that $C$ regular $\iff$ $C$ normal. 
	\end{itemize}
\end{example}
\begin{lemma} \label{regularCurve}
	Let $A$ be a Notherian local ring of dimension $1$. Then $A$ is regular if and only if $A$ is a discrete valuation ring ($\iff$ $A$ is integrally closed).
\end{lemma}
\begin{proof}
	If $A$ is a discrete valuation ring, then every ideal in $A$ is principal and $A$ is a domain. So $\mi = (\pi)$ and $\mi=(\pi^2)$. Hence, \[\mi/\mi^2 = (\pi)/(\pi^2) \cong (A/\mi)\pi \] and hence $\dim(\mi/\mi^2))=1= \dim A$. Therefore, $A$ is regular. 
	
	Conversly, if $A$ is regular, then $\mi=(\pi)$ for some $\pi \in A$ by Nakayama. We show that every ideal in $A$ is principal. Let $I \subset A$ be a non-zero ideal. By commutative algebra $\bigcup_{l \ge 1} \mi^l = 0$. Therefore, there exists $n \ge 1$ such that $I \subset \mi^n$ but $I \not \subset m^{n+1}$. 
	There exists $y \in a\pi^n \in I$ such that $y \notin \mi^{n+1}$. Therefore, $a \notin \mi$ and $a$ is a unit. This implies $a^{-1}y = \pi^n \in I$ and finally $\mi^n = (\pi^n) \subset I$. All in all $I=(\pi^n)$. Moreover, for all $y_1, y_2 \in A$ we write $y_1=a_i\pi^{n_1}$ and $y_2=a_2\pi^{n_2}$ where $a_1,a_2$ are units. This implies that $y_1 \cdot y_2 \neq 0$. Hence, $A$ is a domain. 
\end{proof}
\begin{proposition} \label{HCA}
	Let $A$ be a regular Noetherian local ring. Then 
	\begin{itemize}
		\item $A$ is an integral domain.
		\item for any $\pid \in \Spec A$, also $A_\pid$ is regular. 
		\item $A$ is factorial, hence normal. 
	\end{itemize}
\end{proposition} 
\begin{proof}
	Harder commutative algebra.
\end{proof}
\begin{corollary} \label{regularclosedpoints}
	A Noetherian scheme $X$ is regular if and only if it is regular at all its closed points. 
\end{corollary}
\begin{proof}
	Let $\pid \in X$. Since $X$ is quasi-compact, there exists a closed point $\qi \in \overline{\{\pid\}}$. Let $U=\Spec A$ be an open affine. such that $\qi \in U$. Hence $\pid \in U$. So, $\pid \subset \qi$ (as ideals). 
	Therefore, $A_\pid = (A_\qi)_{\pid A_\qi}$. By our assumptions $A_\qi$ is regular and so is $A_\pid$ by \Cref{HCA}. 
\end{proof}
\begin{theorem}[Jacobi Criterion] \label{jacobi}
	Let $k$ be a field. Let \[X=\Spec(k[x_1, \dots , x_n]/(f_1, \dots , f_r)) \subset \A_k^n\,.\] Let $\pid \in X(k)$, i.e. $p\in X$ such that $\kappa(p)=k$ ($p$ corresponds to $(x_1-a_1, \dots , x_n-a_n)$). Let \[J_p = \begin{pmatrix}
	\frac{\partial f_1}{\partial x_1}(p) & \cdots & \frac{\partial f_r}{\partial x_1}(p) \\
	\vdots & \ddots & \vdots \\
	\frac{\partial f_1}{\partial x_n}(p) & \cdots & \frac{\partial f_r}{\partial x_n}(p)
	\end{pmatrix} \in M_{n \times r}(k)  \] where $\frac{\partial}{\partial x_1}(x_1^{l_1} \cdots x_n^{l_n}) = l_1x_1^{l_1-1}x_2^{l_2}\cdots x_n^{l_n}$, similar for the other derivatives. 
	
	Then $X$ is regular at $p$ if and only if $\operatorname{rank} J_p = n- \dim \MO_{X,p}$.
\end{theorem}
\begin{corollary}
	If $k$ is algebraically closed, then $X=V(f)$ is regular if and only if the system of equations $f=0$, $\frac{\partial f}{\partial x_i}=0$ for all $i$ has no solution in $k^n$. 
\end{corollary}
\begin{proof}
	\Cref{jacobi} and \Cref{HCA}.
\end{proof}
\begin{example}
	Let $X=V(y^2-x^2(x+1))$ (picture: \Cref{y^2-x^3-x^2}). Let $f=y^2-x^3-x^2$ [picture]. Then 
	\begin{align*}
		\frac{\partial f}{\partial x}&=-3x^2-2x \\
		\frac{\partial f}{\partial y}&=-2y
	\end{align*}
	So $(a,b) \in V(f,f_x,f_y)$ if and only if $b=0$ and $-3a^2-2a=0$ and $a^3=a^2$ which can only be true if $a=b=0$.
	
	All in all, $X$ is singular (at $(0,0)$).
	\begin{figure}[h!] 
		\centering
		\begin{tikzpicture}
		\begin{axis}[
		xmin=-1,
		xmax=3,
		ymin=-3,
		ymax=3,
		xlabel={$x$},
		ylabel={$y$},
		axis equal,
		yticklabels={,,},
		xticklabels={,,},
		axis lines=middle,
		samples=200,
		smooth,
		clip=false,
		]
		\addplot [thick, domain=-1:2] {sqrt(x^3+x^2)};
		\addplot [thick, domain=-1:2] {-sqrt(x^3+x^2))};
		\end{axis}
		\end{tikzpicture}
		\caption{$y^2=x^2(x+1)$, singular at $(0,0)$} \label{y^2-x^3-x^2}
	\end{figure}	
\end{example}
\begin{proof}[Proof of \Cref{jacobi}.]
	By linear change of coordinates we may assume that $p$ is the origin. Let $\mi=(x_1, \dots , x_n)$ the ideal of $p$ in $\A_k^n$. Let $\mathfrak n = \mi/I$ where $I=(f_1, \dots , f_r)$ be the ideal of $p$ in $X$. Let $\delta x_i$ be the image of $x_i$ in $\mi/\mi^2$.
	Then, $\delta x_1, \dots , \delta x_n$ for a basis of $\mi/\mi^2$. 
	Consider \[\begin{tikzcd}
		I \arrow[hook]{r} \arrow[bend right]{rr}{D} & \mi \arrow{r} & \mi/\mi^2 \cong k\delta x_1 \oplus \cdots \oplus k \delta x_n
	\end{tikzcd} \,. \]
	Then $D(g)=\frac{\partial g}{\partial x_1}(0) \delta x_1 + \dots + \frac{\partial g}{\partial x_n}(0) \delta x_n$. 
	Consider \[\begin{tikzcd}
	& & 0 \arrow{d} \\
	& & I \arrow[two heads]{r} \arrow{dr}{D} \arrow{d} & I/(I \cap \mi^2) \arrow{d} \\
	0 \arrow{r} & \mi^2 \arrow{d} \arrow{r} & \mi \arrow{d} \arrow{r} & \mi/\mi^2 \arrow{d} \arrow{r} & 0 \\
	& \mathfrak n^2 \arrow{d} \arrow{r} & \mathfrak n \arrow{d} \arrow{r} & \mathfrak n/\mathfrak n^2 \arrow{r} \arrow{d} & 0 \\
	& 0 & 0 & 0 
	\end{tikzcd} \]
	Hence, $\mathfrak n/\mathfrak n^2=\coker(D) \cong \coker(J_p)$. Hence \[\dim(\mathfrak n/\mathfrak n^2) = n-\operatorname{rank}(J_p)=\dim \MO_{X,p} \] iff $\operatorname{rank}(J_p)=n-\dim \MO_{X,p}$. 
\end{proof}
\begin{example}
	If $k$ is not algebraically closed, then the Jacobi criteria is not always useful. 
	
	Let $k=\mathbb F_p(u)$ and $X=(x^p-u) \subset \A_k^1$. Then, $\frac{\partial f}{\partial x} = p \cdot x^{p-1} - 0 =0$. But $k[x]/(x^p-u) = L$ a field since $x^p-u$ is irreducible. So $X=\Spec L$ is regular. 
\end{example}
\begin{example} 
	Regularity is not preserved by base changes or field extensions. Let $X$ be as in the previous example. Let $L=k[y]/(y^p-u)$. Then \[X \times_k L = L[x]/(x^p-u)=L[x]/(x^p-y^p)=L[x]/(x-y)^p \] which is not reduced. $X \times_k L = \{(x-y) \}$ and $L[x]/(x-y)^p$ is local ring at $(x-y)$. Hence, $X$ is not regular. 
\end{example}
\begin{note}
	There exists $X,Y$ varieties over $k$ such that $X,Y$ are regular but $X \times_k Y$ is nor regular. 
\end{note}
\section{Kähler Differentials} 
\begin{definition}[derivation]
	Let $A$ be a ring. Let $B$ an $A$-algebra and $M$ be an $B$-module.
	An \textbf{$A$-derivation of $B$ into $M$} is an $A$-linear map $\varphi \colon B \to M$ such that \[\varphi(ab)=a\varphi(b)+\varphi(a)b \] for all $a,b \in B$.
\end{definition}
\begin{remark}
	In particular. \[\varphi(1)=\varphi(1 \cdot 1) = 1 \cdot \varphi(1) + \varphi(1) \cdot 1 = 2 \varphi(1) \,. \] Therefore, $\varphi(1)=0$.
\end{remark}
\begin{example}
	Let $Y$ be a smooth manifold, $p \in Y$. Let $B=C^\infty(Y), M=\R, A=\R$ where $B \times M \to M$ is given by $(f,a) \mapsto (f(p) \cdot a)$. An example of a derivation is the direction derivative satisfying \[\frac{\partial}{\partial x}(fg)_p = \frac{\partial f}{\partial x}(p) \cdot g(p) + f(p) \cdot \frac{\partial g}{\partial x}(p) \,. \] In fact, we have \[\set{A\text{-derivations of $B$ into $M$}} \cong T_pY \,, \] the tangent space of $Y$ at $p$.
\end{example}
Consider the functor 
\begin{align*}
	\Der_A(B,-) \colon \qquad B-\operatorname{Mod} & \longrightarrow \Set \\
	M & \longmapsto \Der_A(B,M) \\
	(g \colon M \to M^\prime) & \longmapsto (g_\ast \colon \Der_A(B,M) \mapsto \Der_A(B,M^\prime))
\end{align*}
where $g_\ast \colon \iota \mapsto g \circ \iota$. 
\begin{proposition} \label{universalDer}
	The functor $\Der_A(B,-)$ is coreprensentable, so there exist a unique $B$-module $\Omega_{B/A}$ and an $A$-derivation $\delta \colon B \to \Omega_{B/A}$ such that for all $B$-modules $M$ the map 
	\begin{align*}
		\Hom_{B-\operatorname{Mod}}(\Omega_{B/A},M) & \longrightarrow \Der_A(B,M) \\
		g & \longmapsto g \circ \delta
	\end{align*}
	is an isomorphism. For all $A$-derivations $\varphi \colon B \to M$ there uniquely exists $g \colon \Omega_{B/A} \to M$ such that $g \circ \delta = \varphi$. \[\begin{tikzcd} B \arrow{rr}{\forall \varphi} \arrow{dr}{\delta} & & M \\
	& \Omega_{B/A} \arrow[dashed]{ur}{\exists ! g}  \end{tikzcd} \]
\end{proposition}
\begin{example}
	Let $M=\R$, $B=C^\infty(Y)$, $Y$ a manifold. Then \[\Hom_{B-\operatorname{Mod}}(\Omega_{B/k} \otimes k,k) \cong \Hom_{B-\operatorname{Mod}}(\Omega_{B/k},k) \cong \Der_k(B,k) = T_pY \,. \] Here, $\Omega_{B/k} \otimes k$ is the cotangent space to $p \in Y$.
\end{example}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeo_master"
%%% End:
