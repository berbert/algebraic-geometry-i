\section{Spectral Sequence of a Double Complex}
Consider \[\begin{tikzcd}
	K^{0,3} \arrow{r}{d} & \dots \\
	K^{0,2} \arrow{u}{d^\prime} \arrow{r}{d} & K_{1,2} \arrow{r}{d} & \dots \\
	K^{0,1} \arrow{u}{d^\prime} \arrow{r}{d} & K^{1,1} \arrow{u}{d^\prime} \arrow{r}{d} & K^{2,1} \arrow{r}{d} & \dots \\
	K^{0,0} \arrow{u}{d^\prime} \arrow{r}{d} & K^{1,0} \arrow{u}{d^\prime} \arrow{r}{d} & K^{2,0} \arrow{u}{d^\prime} \arrow{r}{d} & K^{3,0} \arrow{r} & \dots
\end{tikzcd} \]
where $d \circ d = 0$, $d^\prime \circ d^\prime = 0$ and $d \circ d^\prime = d^\prime \circ d$.

Define the total complex by \[\begin{tikzcd}
\operatorname{Tot}^0(K) = K^{0,0} \arrow{d}{\delta = d + (-1)^pd^\prime} & x \arrow[mapsto]{d} \\
\operatorname{Tot}^1(K) = K^{1,0} \oplus K^{0,1} \arrow{d}{\delta} & (dx,d^\prime x) & (y,z) \arrow[mapsto]{d}\\
\operatorname{Tot}^2(K) = K^{2,0} \oplus K^{1,1} \oplus K^{0,2} \arrow{d}{\delta} & & (dy,-d^\prime y+dz,d^\prime z) \\
\vdots \arrow{d}{\delta} \\
\operatorname{Tot}^p(K) = K^{p,0} \oplus \dots \oplus K^{0,p}
\end{tikzcd} \]
\begin{lemma}
	$\delta \circ \delta = 0$.
\end{lemma}
\begin{proof}
	Let $x \in K^{p,q}$. Then, \[\delta x = (\dots , 0, dx, (-1)^pd^\prime x, 0 , \dots) \] and therefore \[\delta^1 x = ( \dots , 0, d^2x, (-1)^{p+1}d^\prime d x + (-1)^pdd^\prime x , (-1)^{2p}d^\prime d^\prime x, 0, \dots )=0 \,.\]
\end{proof}
How to compute the cohomology $H^i$ of $\operatorname{Tot}^\bullet(K)$? 
\begin{example}
	Assume $K^{0,0}=0$. Then \[H^1(\Tot^\bullet(K)) = \ker(K^{1,0} \oplus K^{0,1} \to K^{2,0} \oplus K^{1,1} \oplus K^{0,2}) \] where $(y,z) \mapsto (dy,-d^\prime y + dz , d^\prime z)$. 
	
	For which $y \in K^{1,0}$ does there exist a $z \in K^{0,1}$ such that $(y,z) \in H^1(\Tot^\bullet(K))$. We need 
	\begin{itemize}
		\item $dy=0$. So $y \in \ker(K^{1,0} \xrightarrow{d} K^{2,0})=E^{1,0}$. Assume this.
		\item We need $d^\prime y \in \im(d \colon K^{0,1} \to K^{1,1})$ which means that $[d^\prime y]=0 \in \ker(d \colon K^{1,1} \to K^{2,1})/\im(d \colon K^{0,1} \to K^{1,1}) = E^{1,1}$. This means that $y \in \ker(d^\prime \colon E^{1,0} \to E^{1,1})=E^{1,0}_2$.
		\item We need $d^\prime z = 0$. Here, $z$ is well-defined up to a adding an element from $\ker(d \colon K^{0,1} \to K^{1,1})=E^{0,1}$. Furthermore, $d^\prime z \in \ker(d \colon K^{1,1} \to K^{1,2})$ since $dd^\prime z = d^\prime d z = ddy = 0$. All in all, $d^\prime$ is well-defined in $\ker(d \colon E^{0,2} \to E^{0,3})/\im(d^\prime \colon E^{0,1} \to E^{0,2})=E^{0,2}_2$. 
		
		We need $[d^\prime  z]=d_2([y])=0$ in $E^{0,2}_2$ where $d_2 \colon E_{2}^{1,0} \to E_2^{0,2}$. This means that $[y]\in \ker(d_2 \colon E_2^{1,0} \to E^{0,2}_2)=E_3^{1,0}$. 
	\end{itemize}
	All in all, we have an exact sequence \[\begin{tikzcd}
		0 \arrow{r} & E_2^{0,1} \arrow{r} & H^1(\Tot^\bullet(K)) \arrow{r} & E_3^{1,0} \arrow{r} & 0 
	\end{tikzcd} \]
	where the last map is given by $(y,z) \mapsto y$. Furthermore, note that \[ E_2^{0,1} = \setdef{(0,z)}{dz = 0 = d^\prime z} \,.\]
\end{example} 
\begin{exercise} Do the same for $H^2(\Tot^\bullet(K))$ assuming that $K^{0,0} = K^{1,0} = K^{0,1} = 0$. 
\end{exercise}
In general, define $E_0^{p,q}=K^{p,q}$ and \[E_{r+1}^{p,q} = \frac{\ker(d_r \colon E_r^{p,q} \to E_r^{p+1-r,q+r})}{\im(d_r \colon E_r^{p+r-1,q-r} \to E_r^{p,q})} \]  where $d_0=d \colon K^{p,q} \to K^{p+1,q}$ and $d_1 = d^\prime \colon E_1^{p,q} \to E_1^{p,q+1}$ and $d_2 \colon E_2^{p,q} \to E_2^{p-1,q+2}$ and \dots
For each $p,q$ there exists $r_0 > 0$ such that $E_{r+1}^{p,q} \cong E_r^{p,q}$ for all $r \ge r_0$. We define $E_\infty^{p,q} \coloneqq E_r^{p,q}$ for any $r>r_0$.
\begin{theorem} \label{filtration}
	There is a filtration $F^\bullet$ on $H^n(\Tot^\bullet(K))$ \[0 \subset F^0 \subset F^1 \subset F^2 \subset \dots  \subset F^n = H^n(\Tot^\bullet(k))  \] such that \begin{align*}
		F_n/F_{n-1} & \cong E_\infty^{n,0} \\
		F_{n-1}/F_{n-2} & \cong E_\infty^{n-1,1} \\
		& \vdots \\
		F_i/F_{i-1} & \cong E_\infty^{i,n-i} \\
		& \vdots \\
		F_0 & \cong E_\infty^{0,n}
	\end{align*}
\end{theorem}
\begin{remark}
	We can also reverse the orientation, i.e. start with $d_0=d^\prime$ and $d_1=d$ and so an to get a reversed filtration.
\end{remark}
\section{Application of Spectral Sequences in Sheaf and \v Cech Cohomology}
Let $X$ be a topological space. Let $\underline{\mathcal U} = \{U_i\}_{i \in I}$ be an open cover. 
\begin{theorem} \label{exSS}
	There exist a spectral sequence with $E_2$ page\[E_2^{p,q} = \check H^p(\underline{\mathcal U},H^q(\F)) \] converging to $H^{p+q}(X,\F)$ where $H^q(\F)$ is the sheaf associated to the presheaf $V \mapsto H^q(V,\F)$. 
\end{theorem}
\begin{proof}
	Choose an injective resolution \[\begin{tikzcd}
		\F \arrow{r} & \I^0 \arrow{r}{d} & \I^1 \arrow{r}{d} & \I^1 \arrow{r}{d} & \dots
	\end{tikzcd} \]
	Consider the double complex \[\begin{tikzcd}
		\dots & \dots \\
		C^2(\UC,\I^0) \arrow{r} \arrow{u}{d^\prime} & C^2(\UC,\I^1) \arrow{u}{d^\prime} \arrow{r}{d} & \dots \\
		C^1(\UC,\I^0) \arrow{r} \arrow{u}{d^\prime} & C^1(\UC,\I^1) \arrow{u}{d^\prime} \arrow{r}{d} & \dots \\
		C^0(\UC,\I^0) \arrow{r} \arrow{u}{d^\prime} & C^0(\UC,\I^1) \arrow{u}{d^\prime} \arrow{r}{d} & \dots
	\end{tikzcd} \]
	where $d^\prime$ is the \v Cech differential. Consider the spectral sequence with ``reverse orientation''. \begin{align*}E_1^{p,q} & = H^q(\dots \to C^{q,-1}(\UC,\I^p) \to C^q(\UC,\I^p)\to C^{q+1}(\UC,\I^p)) = \check H^q(\UC,\I^p) \\ &= \begin{cases}
		0, & \text{if } q>0 \\
		\check H^0(\UC,\I^p)=\I^p(X), & \text{if } q=0
	\end{cases} \end{align*} since $\I^p$ injective $\implies$ $\I^p$ flabby. 
	
	For $E_2^{p,q}$ consider \[ \begin{tikzcd}
	0 \arrow{drr}{d_2} & 0 & 0 \\
	0 \arrow{drr}{d_2} & 0 & 0 \\
	H^0(X,\F) & H^1(X,\F) & H^2(X,\F) 
	\end{tikzcd} \]
	Therefore, \[E_{r+1}^{p,q}=E_r^{p,q}= \dots = E_2^{p,q} = \begin{cases} H^p(X,\F), & \text{if } q = 0 \\
	0, & \text{else} \end{cases} \,. \] By \Cref{filtration}, $H^p(\Tot^\bullet(K))$ has a filtration with one non-zero graded piece which is $H^p(X,\F)$. 
	Therefore, $H^p(\Tot^\bullet(K)) \cong H^p(X,\F)$.
	
	Consider the spectral sequence with ``correct'' orientation. Then \begin{align*}
		& H^p\left(\dots \to \prod_{i_0 < \dots < i_q} \I^q(\UC_{i_0\dots i_q}) \to \prod_{i_0 < \dots < i_q} \I^{q+1}(\UC_{i_0\dots i_q}) \to \dots \right) \\
		\cong & \prod_{i_0 < \dots < i_q} H^p(\dots \to I^q(U_{i_0 \dots i_q}) \to I^{q+1}(U_{i_0 \dots i_p} \to \dots) \\ 
		= & \prod_{i_0 < \dots < i_q} H^p(U_{i_0 \dots i_q},\F) \\
		= & C^q(\UC,H^p(\F)) 
	\end{align*}
	Using this for $E_1^{p,q}$ we have \[\begin{tikzcd}
		C^2(\UC,H^0(\F)) & \dots \\
		C^1(\UC,H^0(\F)) \arrow{u} & C^1(\UC,H^1(\F)) \arrow{u} \\
		C^0(\UC,H^0(\F)) \arrow{u} & C^1(\UC,H^1(\F)) \arrow{u}
	\end{tikzcd} \]
	Taking cohomology gives $E_2^{p,q}$ as required. 
\end{proof}
\begin{proof}[Proof of \Cref{cechsheaf}]
	\textbf{Step 1.} Let $X$ affine. Let $\UC$ be finite affine open cover of $X$. Then $\check H^p(X,\F)=0$ for all $p>0$ and quasi-coherent $\F$. 
	
	We write $X= \Spec A$. Consider \[C^0(\UC,\F) \to C^1(\UC,\F)  \to C^2(\UC,\F) \to \dots \] which is a complex of $A$-modules. 
	We have $C^\bullet(\UC,\F)$ is exact if and only if $C^\bullet(\UC,\F)_p$ is exact for all $p \in \Spec A$. 
	Let $p \in \Spec A$. 
	Let $f \in A$ such that $p \in D(f)$ and $D(f) \subset U_{i_0}$ for some $i_0$. Then, $C^i(\UC,\F)_f \cong C^0(\UC \cap D(f),\F)$ and this isomorphism commutes with the differential maps. 
	
	Furthermore, $\UC \cap D(f)$ covers $D(f)$ with $U_{i_0} \cap D(f) = D(f)$, so the complex is exact by \Cref{lemma1}. 
	
	\textbf{Step 2.} With assumptions of Step 1, $H^p(X,\F)=0$ (We proved this for $X$ Noetherian)
	
	Assume this is false. Let $k$ be smallest positive integer for which there exist affine $X$ and quasi-coherent $\F$ such that $H^k(X,\F) \neq 0$. 
	
	 Here $H^k(X,F) = H^k(\Gamma(\I^\bullet))$ where $\I^\bullet$ is an injective resolution of $\F$. Let $0 \neq \alpha \in H^k(X,\F)$. Lift $\alpha$ to $\tilde \alpha \in \ker(d \colon \Gamma(\I^k) \to \Gamma(\I^{k+1}))$. Consider the exact sequence \[\begin{tikzcd}
	\I^0 \arrow{r} & \I^1 \arrow{r} & \dots \arrow{r} & \I^{k-1} \arrow{r} & \I^k \arrow{r} & \I^{k+1} \arrow{r} & \dots
	\end{tikzcd} \] which is equivalent to \[ \begin{tikzcd}\I^0_p \arrow{r} & \I^1_p \arrow{r} & \dots \arrow{r} & \I^{k-1}_p \arrow{r} & \I^k_p \arrow{r} & \I^{k+1}_p \arrow{r} & \dots \end{tikzcd}\] being exact for all $p \in X$. Here there exists $\beta_p \in \I_p^{k-1}$ which is send to $\tilde \alpha_p$. 
	
	There exist an affine open neighbourhood $p \in U_p$ and $\beta_p \in \Gamma(\I_p)$ such that $d\beta_p = \res{\tilde \alpha_p}{U_p}$. So, $\res{\alpha}{H^k(U_p,\F)}=0$. 
	
	Let $p_1, \dots , p_n \in X$ such that $X=U_{p_1} \cup \dots U_{p_n}$ and $\UC=\{U_i \}_{i=1}^n$. 
	Consider the spectral sequence \[\begin{tikzcd}
		0 = \check H^p(\UC,H^0(\F)) & \dots \\
		\vdots \\
		0 = \check H^1(\UC,H^0(\F))& \dots \\
		\check H^0(\UC,H^0(\F)) & \check H^0(\UC,H^1(\F)) & \dots & H^k(\UC,H^0(\F))
	\end{tikzcd} \]
	Here, the columns $1,2, \dots , k-1$ vanish because of the minimality of $k$. So, $\check H^0(\UC,H^k(\F))=E_2^{k,0}=E_3^{k,0} = \dots  =E_\infty^{k,0}$. 
	All in all, 
	\[\begin{tikzcd}
		\alpha \arrow[mapsto]{dd} & H^k(X,\F) = H^k(\Tot(K)) \arrow{dr}{\sim} \arrow{dd} \\
		& & E_\infty^{k,0} = \check H^0(\UC,H^k(\F)) \\
		(\res{\alpha}{U_{p_i}})=0 & \ker\left( \prod_i H^k(U_{p_i},\F) \to \prod H^k(U_{p_ip_j},\F) \right) \arrow[equal]{ur}
	\end{tikzcd}\]
	where the equivalence comes from the fact that all other graded pieces of $H^k(\Tot(K))$ vanish. 
	
	\textbf{Step 3.} Proof of the theorem.
	
	By \Cref{exSS}, \[E_2^{p,q}= \check H^p(\UC,H^q(\F)) = \begin{cases}
		0, & \text{if } q>0 \\
		\check H^p(\UC,\F), & \text{if } q=0 
	\end{cases} \xrightarrow{\text{converges to}} H^{p+q}(X,\F) \] Therefore, $H^{p+q(X,\F)}$ has a filtration with only one non-zero graded term which is $\check H^{p+q}(\UC,\F)$. 
\end{proof}
What is \c Cech cohomology really? Look at $P\MO_X-\operatorname{Mod}$, the category of presheaf $\MO_X$-modules. Consider the functor \begin{align*}\check{\mathcal H}^0(\UC,-) \colon P\MO_X-\operatorname{Mod} & \longrightarrow A-\operatorname{Mod} \\
\F & \longmapsto \ker\left(\prod_{i} \F(U_i) \to \prod_{i,j} \F(U_{i,j})\right) \end{align*} where $A=\Gamma(X,\MO_X)$
\begin{proposition}
	$\check{\mathcal H}^p(\UC,-)$ is the $p$-th-derived functor of $\mathcal H^0(\UC,-)$. 
\end{proposition}
Consider \[\begin{tikzcd}
	\MO_X-\operatorname{Mod} \arrow{rr}{\Gamma} \arrow{dr} & & A-\operatorname{Mod} \\
	& P\MO_X-\operatorname{Mod} \arrow{ur}[swap]{\check{\mathcal H}^0(\UC,-)}
\end{tikzcd} \]
The theorem about spectral sequences is a special case of spectral sequences with respect to composition of functor. 