\chapter{Elliptic Curves}
In this chapter, $k$ denotes an algebraically closed file with $\cha k \neq 2,3$. 
\begin{definition}[elliptic curve]
	An elliptic curve is a smooth proper curve $E$ (connected) of geometric genus $1$ together with a distinguished base point $P_0 \in E(k)$.  
\end{definition}
\begin{note} We will later see that there always exists a closed embedding $E \hookrightarrow \PR^2$ with image a cubic curve $C$ such that $C \cap \{z = 0 \} = \{ P_0 \}$.
	
Indeed, $p_g(C)=\frac{1}{2}(d-1)(d-2) = 1$. 
\end{note}
We are first going to study smooth plane cubic curves $C \hookrightarrow \PR^2$ without fixing $P_0$. In particular, we are interested in 
\begin{itemize}
	\item classification and
	\item structure of $\Pic C$, $\Div C$.
\end{itemize}
\section{Smooth Proper Curves}
For any point $P \in C$, one can construct a map $\varphi = \varphi_P \colon C \to \PR^1$ of degree $2$. 

Set-theoretically, we describe $\PR^1$ as $\{\text{lines in $\PR^2$ through $P$}\}$.
Let \[\varphi_P(Q)= \begin{cases}
	\overline{PQ}, & \text{ if } Q \neq P \\
	T_PC, & \text{ if } Q = P
\end{cases} \] (see \Cref{linePQ}).
\begin{figure}[h]
	\centering
	\begin{tikzpicture}
	\begin{axis}[
	xmin=-1.5,
	xmax=2.5,
	ymin=-3,
	ymax=3,
	xlabel={},
	ylabel={},
	axis equal,
	yticklabels={,,},
	xticklabels={,,},
	axis lines=middle,
	samples=200,
	smooth,
	clip=false,
	]
	\addplot [thick, domain=-1:0] {sqrt(x^3-x)};
	\addplot [thick, domain=-1:0] {-sqrt(x^3-x))};
	\addplot [thick, domain=1:2] {sqrt(x^3-x)};
	\addplot [thick, domain=1:2] {-sqrt(x^3-x))};
	\addplot [thick, domain=-1.5:2.5] {0.3*x};
	\end{axis}
	\draw (5.7,3.4)  node{$\overline{PQ}$};
	\draw (1.6,2.5) node[below right]{$P$};
	\draw (3.7,3.4) node{$Q$};
	\end{tikzpicture}
	\caption{The line $\overline{PQ}$ through $P, Q$} \label{linePQ}
\end{figure}
For $l \in \PR^1$ we have \[\deg \varphi^\ast(l) = \deg((l \cap C) \setminus \{P \}) = \deg(Q+R) = 2 \] Hence, $\deg(\varphi_p)=2$. 
So, $\varphi^\ast \MO_{\PR^1}(1)$ should be a line bundle of degree $2$. 

Scheme-theoretically, given a line bundle of degree $2$ with $2$ section that do not vanish simultaneously. Let $i \colon C \hookrightarrow \PR^2$, let $\mathcal L = i^\ast \MO_{\PR^2}(1) = \MO_C(1)$. Then, $H^0(C, \mathcal L) = k[x,y,z]$ by using \[\begin{tikzcd}
	 0 \arrow{r} & \MO_{\PR^2}(-2) \arrow{r} & \MO_{\PR^2}(1) \arrow{r} & \MO_{C}(1) \arrow{r} & 0
\end{tikzcd} \]
Consider $\mathcal L(-P) \coloneqq \mathcal L \otimes \MO_C(-P)$. We have \[\begin{tikzcd}
 0 \arrow{r} & \mathcal L(-p) \arrow{r} & \mathcal L \arrow{r} & \mathcal L \otimes \kappa(P) \arrow{r} & 0
\end{tikzcd} \] 
On stalks we get 
\[\begin{tikzcd}
0 \arrow{r} & \mathcal L(-P)_P \arrow{r} & \mathcal L_P \arrow{r} &  \kappa(P) \arrow{r} & 0 \\
& \MO_C(-P)_P \arrow[equal]{u} & \MO_{C,P} \arrow[equal]{u}
\end{tikzcd} \] 
Hence, $H^0(C, \mathcal L(-P)) ) =  \setdef{h \in k[x,y,z]}{h_P \in \mi_P}$ or equivalently, $h(P)=0$. This has dimension $2$. 

Let $s_0,s_1 \in H^0(C, \mathcal L(-P))$ be a basis. Then as sections of $\mathcal L(-P)$, $s_0$ and $s_1$ do not vanish simultaneously. Assume this, let \begin{align*}
	\varphi \colon C & \longrightarrow \PR^1 \\
	Q & \longmapsto [s_0(Q), s_1(Q)]
\end{align*}
be the associated map. 

To see that the assumption above is true, let $U$ be an open neighborhood of $P$ such that there exists $t \in H^0(U, \MO_C)$ such that $V(t) = \{p\}$. Then, $\res{\mathcal L}U (-p) = t \cdot \res{\mathcal L}U$. We may assume that $\res{\mathcal L}U \cong \MO_U$. Then, \[\begin{tikzcd}
\res{\mathcal L}U(-p) \arrow{r}{\cong} \arrow[hook]{d} & \MO_U(-p) = t \MO_U\arrow{r}{\cong} \arrow[hook]{d} &\MO_U\\
\res{\mathcal L}U \arrow{r}{\cong} & \MO_U
\end{tikzcd} \] 
Therefore, under $\res{\mathcal L(-p)}U \cong \MO_U$ a section $s$maps to $s/t$. 
We have $(s/t)_p \in \mi_p$ $\iff$ $s_p \in \mi_p^2$ $\iff$ $V(s)$ is tangent to $C$ at $p$ (since $T_pC = \mi_p/\mi_p^2$). 

Now, $V(s_0), V(s_1)$ are both tangent to $C$ at $p$. 
Therefore, $V(s_0) = V(s_1)$ and they are not linearly independent. 

All in all, $s_0,s_1$ actually do not vanish simultaneously. 

Why does this give the same $\varphi$ as before (set-theoretically)?

We look at a local model: $P=(0,0) \in C \cap \A_{z \neq 0}^2 \subset L \subset \PR^2$. Then, $s_0 = x, s_1 = y$. Hence, $\varphi(Q) = (x(Q),y(Q))$. Let $L^\prime$ be a line representing $Q$. Then $\overline{PQ} = \langle L, L^\prime \rangle$ and $\overline{PQ}/L =\text{line in $xy$-plane through $0$ and $(x(Q),y(Q)$}$. All in all, $\varphi$ is indeed the right set-theoretic map. 
\begin{lemma}
	Every map $t \colon C \to \PR^1$ is degree $2$ is of this form.
\end{lemma}
\begin{proof}
	Let \[\mathcal M \coloneqq f^\ast \MO_{\PR^1}(1) = f^\ast \MO_{\PR^1}([0:1]) = \MO_C(f^{-1}([0:1])) = \MO_C(P+Q) \,.\]
	Let $L \coloneqq \overline{PQ} \subset \PR^2$ (or $L=T_pC$ if $P=Q$). Then $L \cap V = P + Q + R$. Let \[\mathcal L = \MO_C(1) = i^\ast \MO_{\PR^2}(L)=\MO_C(P+Q+R)=\mathcal M(R) \,.\] 
	Hence, $\mathcal M = \mathcal L(-R)$. Also, $f^\ast x, f^\ast y$ form a basis of $H^0(C,\mathcal L(-R))$ as before.  
\end{proof}
Our idea is it to use $f \colon C \to \PR^1$ to classify cubic plane curves. 
\begin{proposition} \label{prop2305}
	For any two maps $f_1,f_2 \colon C \to \PR^1$ of degree $2$ there exist automorphisms $\sigma \colon C \to C$ and $\tilde \sigma \colon \PR^1 \to \PR^1$ such that \[\begin{tikzcd}
		C \arrow{r}{\sigma} \arrow{d}{f_1} & C \arrow{d}{f_2} \\
		\PR^1 \arrow{r}{\tilde \sigma} & \PR^1
	\end{tikzcd} \]
	commutes.
\end{proposition}
\begin{lemma} \label{lem12305}
	For any $2$ points $P,Q \in C$ there exists an automorphism $\sigma \colon C \to C$ such that $\sigma(P)=Q$.
\end{lemma}
\begin{proof}
	Let $R \in C$ such that $\overline{PQ} = P+Q+R$. Define $\sigma_R \colon C \to C$ by 
	\begin{itemize}
		\item set-theoretically: $\sigma_R(X)=Y$ where $Y \in C$ such that $\overline{XR} \cap C = X+R+Y$. 
		\item scheme-theoretically: Let $\varphi_R \colon C \to \PR^1$ be the degree $2$ map from before. Note $\varphi_R$ induces $K(\PR^1) \subset K(C)$ a degree $2$ extension (i.p normal). In fact, it is separable, as $g(C) \neq g(\PR^1)$ (as $\cha k \neq 2$). 
		Hence, it is a Galois extension. Hence, $\operatorname{Aut}(K(C)/K(\PR^1)\ni \sigma$ contains an involution. 
		
		Let $\sigma_r \colon C \to C$ be the associated map of curves (via \Cref{CatEqCurves}). 
	\end{itemize}
	All in all, $\sigma = \sigma_R$ is the desired map. 
\end{proof}
\begin{definition}[ramification index, branch point]
	Let $f \colon C_1 \to C_2$ be a non-constant map of curves. Let $P \in C_1$ and $Q=f(P)$. Let $t \in \MO_{C_2,Q}$ be a local parameter (uniforming, i.e. generator of $\mi_Q$). The \textbf{ramification index of $t$ at $P$} is \[e_P = v_P(f^{\#}t)  \,.\] If $e_P > 1$, $P$ is called a \text{branch point of $f$}. 
\end{definition}
\begin{lemma}
	We have $\dim_k(\Omega_{C_1/C_2,P}) = e_P-1$
\end{lemma}
\begin{proof}
	Exercise. 
\end{proof}
\begin{corollary}
	$P$ is a branch point if and only if $\Omega_{C_1/C_2,P} \neq 0$.
\end{corollary}
\begin{remark}
	We have $f^\ast Q = \sum_{P \in f^{-1}(Q)} e_P \cdot P$. 
	If $f$ has degree $2$, then $P$ is a branch point $\iff$ $e_P=2$. 
\end{remark}
\begin{lemma} \label{lem22305}
	Let $f \colon C \to \PR^1$ be a degree $2$ map and $C$ be a plane cubic. 
	Then $f$ has exactly $4$ (distinct) branch points. 
\end{lemma}
\begin{proof}
	Recall that there exists an exact sequence \begin{equation} \label{seqlem22305} \begin{tikzcd}
	f^\ast \Omega_{\PR^1} \arrow{r}{\dd f} & \Omega_C \arrow{r} & \Omega_{C/\PR^1} \arrow{r} & 0
	\end{tikzcd} \end{equation}
	where the first two sheaves are invertible. 
	We have, $(\Omega_{C/\PR^1})_\eta = \Omega_{K(C) / K(\PR^1)}=0$ as $K(\PR^1) \subset K(C)$ is separable. 
	Hence, $(\dd f)_\eta$ is surjective and hence injective. So, $(\ker \dd f)_\eta = 0$ and also $\ker \dd f = 0$.
	
	Hence, \eqref{seqlem22305} is also exact on the left. Therefore, 
	\begin{itemize}
		\item $\Omega_X = \omega_X = \res{\omega_{\PR^2}(3)}C$ 
		\item $f^\ast \Omega_{\PR^1} = f^\ast \Omega_{\PR^1}(-2)$ has degree $-4$
	\end{itemize}
	We get \[\begin{tikzcd}
		0 \arrow{r} & f^\ast\Omega_{\PR^1} \arrow[equal]{d} \arrow{r}{\dd f} & \Omega_C \arrow[equal]{d} \arrow{r} & \Omega_{C/\PR^1} \arrow{r} \arrow{d}{\cong} & 0 \\
		0 \arrow{r} & \MO_C(_D) \arrow{r} & \MO_C \arrow{r} & \MO_D\arrow{r} & 0
	\end{tikzcd} \]
	$D$ is an effective divisor of degree $4$. 
	All in all, $\sum_{p \in C} (e_p-1) = \Gamma(X, \Omega_{C/\PR^1}) = \dim \Gamma(\MO_D) = 4$. Therefore, there are $4$ points such that $e_p =2$. 
\end{proof}
\begin{proof}[Proof of \Cref{prop2305}.]
	Let $f_1,f_2$ be of degree $2$. Let $P_j$ be a branch point of $f_i$ for $i=1,2$. Let \[\mathcal L_i = f_i^\ast \MO_{\PR^1}(1 = f_i^\ast \MO(f_i(P_i))) = \MO_C(2 P_i) \,.\] Let $\sigma \colon C \to C$ such that $\sigma(P_1) = P_2$. Then $\sigma^\ast \mathcal L_2 = \mathcal L_1$. The rest is left to the reader as an exercise. 
\end{proof}
\section{Classification}
Let $f \colon C \to \PR^1$ be a degree $2$ map. let $P_1,P_2,P_3,P_4$ be the images of the branch points (so distinct). We will associate ``cross radio''.
\begin{lemma} \label{lem32305}
	Let $P,Q,R \in \PR^1$ be three distinct points. There is a unique $A \in \operatorname{PGL}(2) = \operatorname{Aut}(\PR^1)$ such that $P \mapsto 0, Q \mapsto 1, R \mapsto \infty$.   
\end{lemma}
\begin{proof}
	Write $\begin{pmatrix}
	a \\ b
	\end{pmatrix}$ for $[a,b] \in \R^1$. Let $P = \begin{pmatrix}
	P_0 \\ P_1
	\end{pmatrix}$ and $Q = \begin{pmatrix}
	Q_0 \\ Q_1
	\end{pmatrix}$ and $R = \begin{pmatrix}
	R_0 \\ R_1
	\end{pmatrix}$ and $A = \begin{pmatrix}
	 a& b \\ c & d
	\end{pmatrix}$. We need that 
	\begin{itemize}
		\item $A\begin{pmatrix}
		0 \\ 1
		\end{pmatrix} = \begin{pmatrix}
		b \\ d
		\end{pmatrix}$ should be $\begin{pmatrix}
			P_0 \\ P_1
		\end{pmatrix} \cdot \mu$
		\item $A\begin{pmatrix}
			1 \\ 1
		\end{pmatrix} = \begin{pmatrix}
		a+b\\ c+d
		\end{pmatrix}$ should be $\begin{pmatrix}
		Q_0 \\ Q_1
		\end{pmatrix} \cdot \nu$
		\item $A \begin{pmatrix}
		1 \\ 0
		\end{pmatrix}= \begin{pmatrix}
			a \\ c
		\end{pmatrix}$ should be $\begin{pmatrix}
		R_0 \\ R_1
		\end{pmatrix} \cdot \lambda$
	\end{itemize}
	The first and the third condition give that \[A = \begin{pmatrix}
		\lambda R_0 & \mu P_0 \\
		\lambda R_1 & \mu P_1
	\end{pmatrix} \]
	By the second condition, we get that \[\mu = \lambda \frac{Q_0R_1-Q_1R_0}{Q_1P_0-Q_0P_1}\] where $Q_1P_0-Q_0P_1 \neq 0$ since $Q \neq P$. 
	All in all, we found a unique $A$.
\end{proof}
\begin{definition}[crossratio]
	Let \[\operatorname{Crossratio}(P_1,P_2,P_3,P_4) = A \cdot P_4 \in k \setminus \{0,1\} \subset \PR^1 \] where $A \in \operatorname{Aut}(\PR^1)$ is the unique one sending $P_1 \mapsto 0, P_2 \mapsto 1, P_3 \mapsto \infty$.
\end{definition}
There is no preferred order of the branch points of $f \colon C \to \PR^1$. 
What happens if you permute the $P_i$'s? We get that crossratios form an $S_3$-orbit in $\PR^1$. Hence, the crossratio/invariant associated to $f$ should be an element in $\PR^1 \setminus \{0,1,\infty\}/S^3$
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeo_master"
%%% End:
