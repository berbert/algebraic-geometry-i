\section{Valuative Criterion for Separatedness}
Let $A$ be an integral domain and $K=\Frac A$.
\begin{definition}[valuation ring]
	The ring $A$ is called \textbf{valuation ring} if one of the following equivalent definitions are satisfied (where $K=\Frac A$)
	\begin{enumerate}
		\item $\forall 0 \neq x \in K \colon x \in A$ or $\frac{1}{x} \in A$
		\item The set of ideals is totally ordered.% [picture of $\Spec$]
		\item There exists a totally ordered group $\Gamma$ and a surjective group homomorphism $v \colon K^\ast \to \Gamma$ such that $A=\setdef{x \in K}{v(x) \ge 0} \cup \{0\}$
	\end{enumerate}
\end{definition}
\begin{proof} (sketch)
	$(a) \implies (b)$: Let $I \not \subset J, J \not \subset I$, let $a \in I \setminus J, b \in J \setminus I$. Consider $x=\frac{a}{b}$ and apply $(a)$.
	
	$(b) \implies (a)$: If $x=\frac{a}{b}$ with $a,b \in A$, then by $(b)$ we have $(a) \subset (b)$ or $(b) \subset (a)$ implying $a=bs$ for some $s \in A$ or $b=sa$ for some $s \in A$, respectively. 
	
	$(a),(b) \implies (c)$: Let $\Gamma = K^\ast / A^\ast$.
\end{proof}
\begin{example}~ \vspace{-\topsep}
	\begin{itemize}
		\item Let $A=\C[x]_{(x)}$. This is a valuation ring. Let $f = \frac{p(x)}{q(x)} \in \C[x]$ and define $v(f) = \text{order of vanishing at } 0 \coloneqq v(p)-v(q)$ where $v(p)=\max\setdef{l}{x^l | p}$.
		\item $A=\Z_{(3)}$ is a valuation ring (very similar).
		\item $A=\C[x,y]/(y^2-x^2(x+1))_\mi$ where $\mi = (x,y)$ is not a valuation ring (see \Cref{picexVR}).
		
		\begin{figure}[h]
			\centering
			\begin{tikzpicture}
			\begin{axis}[
			xmin=-1,
			xmax=3,
			ymin=-3,
			ymax=3,
			xlabel={$x$},
			ylabel={$y$},
			axis equal,
			yticklabels={,,},
			xticklabels={,,},
			axis lines=middle,
			samples=200,
			smooth,
			clip=false,
			]
			\addplot [thick, domain=-1:2] {sqrt(x^3+x^2)};
			\addplot [thick, domain=-1:2] {-sqrt(x^3+x^2))};
			\addplot [dashed, domain = -2:2] {x};
			\addplot [dashed, domain = -2:2] {-x};
			\end{axis}
			\end{tikzpicture}
			\caption{$\Spec \C[x,y]/(y^2-x^2(x+1))_{(x,y)}$}
			\label{picexVR}
		\end{figure}
		Intuitively, we can't read off the order of functions at $0$, e.g. $f=(x+y)^2+(x-y)$. 
	\end{itemize}
\end{example}
\begin{proposition}
	The valuation rings of $K$ are the maximal elements of the set of local subrings of $K$ ordered by domination (where $B$ dominates $A$ $\iff$ $A \subset B$ such that $\mi_B \cap A = \mi_A$)
\end{proposition}
\begin{example}
	Consider 
	\begin{align*}
		\C[t^2-1,t(t^2-1)]\cong\C[x,y]/(y^2-x^2(x+1)) & \hookrightarrow \C[t] \hookrightarrow K = \C(t) \\
		x & \mapsto t^2-1 \\
		y & \mapsto t(t^2-1)
	\end{align*}
	Now $\C[t^2-1,t(t^2-1)]_{(x,y)}$ is dominated by $\C[t]_{(t \pm 1)}$.
\end{example}
\begin{example}
	The ring $\C[x,y]_\mi$ is not a valuation ring, e.g. $x/y$ and $y/x$ are both not in the ring. It's dominated by e.g. $\C[x,y,x/y,x/y^2,x/y^3, \dots]_{(x,y)}$ wich is a valuation ring.
\end{example}
\begin{lemma} \label{helplemma}
	Let $T$ be reduced, $f \colon T \to X$ be a morphism. Then $f$ factors as \[\begin{tikzcd}
	T \arrow{r}{\tilde f} & Y \arrow[hook]{r}{i} & X
	\end{tikzcd}\]
	where $Y = \overline{f(T)}$ with the reduced induced subscheme structure. 
\end{lemma}
\begin{proof}
	We can assume that $X= \Spec B$. Let $I = \ker(B \to \MO_T(T))$ which is a radical ideal in $B$ (since $T$ is reduced).
	
	This leads to \[\begin{tikzcd}
		B \arrow{rr}{f^\#} \arrow{dr} & & \MO_T(T) \arrow{dl} \\
		& B/I
	\end{tikzcd}\]
	and finally to 
	$$\begin{tikzcd}
		\Spec B & & T \arrow{ll} \arrow[two heads]{dl} \\
		& \Spec(B/I) \arrow{ul}
	\end{tikzcd}$$
\end{proof}
\begin{lemma}\label{HomVR}
	Let $A$ be a valuation ring, $K = \Frac A$ and $X$ a scheme. There is a natural bijection between $\Hom_{\Sch}(\Spec A, X)$ and the set of pairs of 
	\begin{enumerate}
		\item points $x_0,x_1 \in X$ such that $x_0 \in \overline{\{x_1\}}$ ($x_0$ specialization of $x_1$)
		\item inclusions $\kappa(x_1) \hookrightarrow K$ such that $A \cap \kappa(x_1)$ dominates $\MO_{Z,x_0}$ where $Z=\overline{\{x_1\}}$ with reduced induced subscheme structure. %[picture]
	\end{enumerate}
\end{lemma}
\begin{proof}
	Given such a pair consider \[\begin{tikzcd}
	\MO_{Z,x_0} \arrow[hook]{r} \arrow[bend right]{rr} & A \cap \kappa(x_1) \arrow[hook]{r} & A
	\end{tikzcd}\]
	which induces \[\begin{tikzcd}
		\Spec A \arrow{r} \arrow[bend right]{rrr}{f} & \Spec \MO_{Z,x_0} \arrow{r}{\ast} & Z \arrow[hook]{r}{i} & X
	\end{tikzcd}\]
	where $\ast$ can be obtained as follows: Given $Z, z \in Z$. Consider $U = \Spec B$ open affine and $B = \MO_U(U) \xrightarrow{res} \MO_{Z,z}$ which induces \[\Spec \MO_{Z,z} \longrightarrow U \longrightarrow X \,.\]
	
	Conversly, given $f \colon \Spec A \to X$, let $x_1 = f((0))$ and $x_0=f(\mi)$. 
	Since $f$ is continuous, we have $x_0 \in \overline{\{x_1\}}$.
	Let $\kappa(x_1) \hookrightarrow K$ correspond to $$\Spec K \longrightarrow \Spec A \longrightarrow X$$ We need to check that $A \cap \kappa(x_1)$ dominates $\MO_{Z,x_0}$.
	Now we are going to use \Cref{helplemma}.
	Here $\overline{f(\Spec A)} = Z$ and $A$ reduced. 
	This leads to \[\begin{tikzcd}
		\Spec A \arrow{r}{\tilde f} & Z \arrow{r} & X \\
		\mi \arrow[mapsto]{r} & x_0
	\end{tikzcd}\]
	which implies that \[\begin{tikzcd}
		\MO_{Z,x_0} \arrow{r} \arrow[hook]{d}{res} & A_\mi = A \arrow{d} \\
		\kappa(x_1) \arrow{r} & K
	\end{tikzcd}\]
	does commute. So $A \cap \kappa(x_1)$ dominates $\MO_{Z,x_0}$
\end{proof}
Idea: Let $X$ be a scheme. 
Assumed we have $f,g \colon \A^1 \to X$ such that $\res{f}{\A^1 \setminus \{0\}} = \res{g}{\A^1 \setminus \{0\}}$. Do $f$ and $g$ agree? 
Is the extension of $\res{f}{\A^1 \setminus \{0\}}$ unique? 
Yes, if $X$ is seperated. The valuation criterion is converse. 

Idea 2: Here we really only care about $\res{f}{\Spec \C[x]_{(x)}}$ and $\res{g}{\Spec \C[x]_{(x)}}$. 
\begin{definition}[quasi-separated]
	Let $f \colon X \to Y$ be a morphism of schemes. It is called \textbf{quasi-separated} if $\Delta_X \colon X \to X \times_Y X$ is quasi-compact.
\end{definition}
\begin{lemma}
	A morphism $f \colon X \to Y$ is quasi-seperated iff for all open affine $\Spec A \subset Y$ and $U,V \subset X$ open affines such that $f(U) \subset \Spec A, f(V) \subset \Spec A$, we have that $U \cap V$ is a finite union of affine opens. 
\end{lemma}
\begin{remark}
	If $X$ is locally Noetherian, then any morphism $f \colon X \to Y$ is quasi-separated. 
\end{remark}
\begin{lemma} \label{helplemma2}
	Let $f \colon X \to Y$ be quasi-compact. Then $f(X)$ is closed in $Y$ $\iff$ $f(X)$ is stable under specialization, i.e. if $x_1 \in f(X)$ and $x_0 \in \overline{\{x_1\}}$, then $x_0 \in f(X)$. 
\end{lemma}
\begin{proof}
	``$\implies$'': $f(X)$ closed and $x_1 \in f(X)$. Then $\overline{\{x_1\}} \subset f(X)$ which especially implies $x_0 \in f(X)$.
	
	``$\impliedby$'': Here we need to use that $f$ is quasi-compact. 
	A counter-example for $f$ not being quasi-compact is the map $X=\bigsqcup_{a \in \C} \Spec \C[x]/(x-a) \xrightarrow{f} \Spec \C[x]$. Here $f(X)=\setdef{(x-a)}{a \in \C}$ is closed unter specialization but not closed. 
	
	Assume that $X$ is reduced. Replace $Y$ by $\overline{f(X)}$ with reduced induced subscheme structure we may assume that $\overline{f(X)}=Y$ (using \Cref{helplemma}).
	
	We need to prove that $f$ is surjective. 
	Assume that $Y$ is affine. 
	Since $f$ is quasi-compact we have $f^{-1}(Y)  = X_1 \cup \dots \cup X_m$ where $X_i$ are affine open. Let $Y_i = \overline{f(X_i)}$ with reduced induced subscheme structure. 
	Let $y \in Y$. 
	There exists $i$ such that $y \in Y_i$. 
	Let $\eta \in Y_i$ be a point such that $y \in \overline{\{\eta\}}$ and $\eta$ is not in the closure of any other point in $Y_i$. 
	Equivalently, let $Y_i = \Spec B$
	\begin{itemize}
		\item $y=\pid \subset B$ is a prime ideal
		\item $\eta = \pid^\prime \subset \pid \subset B$ be a prime ideal minimal among prime ideals contained in $\pid$. 
	\end{itemize}
	Let $X_i= \Spec A$. We claim that $\pid^\prime = \eta \in f(X_i)=f(\Spec A)$ which implies $y \in f(X)$ by specialization.
	
	Consider \[\begin{tikzcd}
	A \arrow{r}{\psi} & A \otimes_{B} B_{\pid^\prime} \\
	B \arrow{u}{f^\#} \arrow{r}{\rho} & B_{\pid^\prime} = \kappa(\pid^\prime) \arrow{u}{\varphi}
	\end{tikzcd}\]
	
	Let $\qi \subset A \otimes_B B_{\pid^\prime}$ be a prime ideal. Then $\varphi^{-1}(\qi) = (0)$. let $\qi^\prime = \psi^{-1}(\qi)$. Then $f(\qi^\prime)=(f^\#)^{-1}(\qi^\prime)=\rho^{-1}((0)) = \pid^\prime$
\end{proof}
\begin{theorem}[Valuative criterion for seperatedness]
	$f \colon X \to Y$ is seperated $\iff$ $f$ is quasi-seperated and for all valuation rings $A$ with $K=\Frac A$ and for all commutative diagrams \[\begin{tikzcd}
		\Spec K \arrow[equal]{r} & U \arrow{r} \arrow{d} & X \arrow{d}{f} \\
		\Spec A \arrow[equal]{r} & T \arrow{r} \arrow[dashed]{ur}{\le 1} & Y
	\end{tikzcd}\]
	there is at most one map $T \to X$ such that the whole diagram commutes. 
\end{theorem}
\begin{remark}
	In other words: Given $f,g \colon T \to X$ over $Y$ such that $\res{f}{U} = \res{g}{U} \implies f=g$. 
\end{remark}
\begin{remark}
	There is a Noetherian version of this theorem (can be found in Görtz-Wedhorn). Let $Y$ be locally Noetherian and $f \colon X \to Y$ be of finite type. 
	
	Then $f \colon X \to Y$ is seperated $\iff$ for all discrete valuation rings (Noetherian rings) the above condition holds. 
\end{remark}
\begin{proof}
	``$\implies$'': We know that $f$ seperated $\iff$ $\Delta_X \colon X \to X \times_Y X$ is a closed immersion. Therefore, $\Delta_X$ is quasi-compact and hence $f$ is quasi-seperated. 
	
	Let $T=\Spec A$ and $U = \Spec K$. Let $f,g \colon T \to X$ as in the diagram. 
	Let $Z=\setdef{t \in T}{f \circ i_t = g \circ i_t}$.
	Now $Z$ is closed since $f$ is seperated and $U \subset Z$ by assumption. Therefore, $Z=T$. Finally $T$ is reduced which implies $f=g$ (exercise).
	
	``$\impliedby$'': We check that $\Delta_X(X)$ is a closed subset. By \Cref{helplemma2} it is enough to show that $f$ is quasi-seperated and that $\Delta_X(X)$ is closed unter specialization. Let $x_0,x_1 \in X \times_Y X$ such that $x_0 \in \overline{\{ x_1 \}}$ and $x_1 \in \Delta_X(X)$. Let $K=\kappa(x_1)$ and let $A$ be a minimal local subring of $K$ that dominates $\MO_{Z,x_0}$ where.; $Z=\overline{\{x_1\}}$. Then $A$ is a valuation ring. 

	By \Cref{HomVR}, we have that \[\begin{tikzcd}
	 U = \Spec K \arrow{r}{\tilde t} \arrow{d}{j} & X \arrow{d}{\Delta_X} \\
	 T = \Spec A \arrow{r}{t} & X \times_Y X
	\end{tikzcd}\]
	where $\tilde t$ exists since $x_1 \in \Delta_X(X)$ and $\Delta_X$ is a closed immersion. 
	
	Let $f=p_1 \circ t \colon \Spec A \to X$ and $g = p_2 \circ t \colon \Spec A \to X$ (over $Y$) such that $\res{f}{U} = (p_1 \circ t \circ j)= p_1 \circ \Delta_X \circ \tilde t = p_2 \circ \Delta_X \circ \tilde t= \res{g}{U}$. By assumption $f=g$. Therefore, $t$ factors through $\Delta_X$ and $t(\mi) = x_0 \in \Delta_X(X)$.
\end{proof}
\begin{lemma}~ \vspace{-\topsep}
	\begin{enumerate}
		\item Open/closed immersion are separated
		\item The property of being seperated is preserved under composition, base change, local on target ($f \colon X \to Y$ seperated $\iff$ $\exists$ open cover $Y=\bigcup_i U_i$ such that $\res{f}{f^{-1}(U_i)}$ is seperated.)
		\item If $X \to Y \to Z$ is seperated then $X \to Y$ is seperated. 
	\end{enumerate}
\end{lemma}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeo_master"
%%% End:
