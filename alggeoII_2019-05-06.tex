\section{Hodge Numbers}
Let $X$ be a smooth projective variety over $k$, $\Omega_{X/k}$ locally free of rank $\dim X$. Consider
\begin{align*}
  h^{p,q}(X)&= \dim_k H^q(X,\Omega^p)  \\
  h^{1,0}(X) &= h^0(X,\Omega) = \text{dimension of space of global algebraic $1$-forms}  \\
  \vdots   &\phantom{=} \vdots  \\
  h^{n,0}(X) &= h^0(X,\Omega^n) = \text{geometric genus}
\end{align*}
\begin{note}
 The Hodge numbers $h^{p,0}(X)$ are birational invariants of $X$, i.e. if $X \xrightarrow[\text{birat.}]{\sim} X^\prime$, then $h^{p,0}(X) = h^{p,0}(X^\prime)$
\end{note}
\begin{remark}
  This is not true for all $h^{p,q}$, e.g. for $\PR^2$ CONT.
\end{remark}
Our goal is it to compute $h^{p,q}(Z)$ for nonsingular hypersurfaces $Z = V(f) \subseteq \PR^n$. 

We have $X$ smooth over $k$, $Z \subseteq X$ closed, smooth over $k$, $\I = \text{ideal sheaf of $Z$ in $X$}$.
Last semester we had
\begin{equation*}
  \I/\I^2 \xrightarrow{\, \, \delta \, \,} \res{\Omega_{X}}{Z} \longrightarrow \Omega_Z \longrightarrow 0
\end{equation*}
is exact ($\I/\I^2$ is an $\MO_Z$-module).
We don't need $Z$ or $X$ smooth for this.
\begin{theorem} \label{exseqKahler}
  If $Z$ and $X$ are smooth over $k$, then
  \begin{equation*}
    \begin{tikzcd} 0 \arrow{r} & \I/\I^2\arrow{r} & \res{\Omega_X}{Z} \arrow{r} & \Omega_Z\arrow{r} & 0 \end{tikzcd}
  \end{equation*}
  is exact.
\end{theorem}
\begin{example}
Let $C = V(f) \subseteq \PR^2$ be a nonsingular hypersurface of degree $d$.
What is $h^0(X,\omega_C) = p_g(C)$?
What is $h^1(C,\Omega_C)$? ($\Omega_C \cong \omega_C$)
We have $\I_{C\subseteq \PR^2} = \MO_{\PR^2}(-d)$, $\I^2 = \I(-d)$.
By the theorem we have an exact sequence
\begin{equation*}
  \begin{tikzcd} 0 \arrow{r} & \res{\MO_{\PR^2}(-d)}{C} \arrow{r} & \res{\Omega_{\PR^2}}{C} \arrow{r} & \Omega_C \arrow{r} & 0 \end{tikzcd}
\end{equation*}
Recall that if $0 \to V_1 \to V_2 \to V_3$ is an exact sequence of vector spaces of dimension $d_1$, $d_2$ and $d_3$, respectively, then $\bigwedge^{d_2}V_2 \cong \bigwedge^{d_1}V_2 \otimes \bigwedge^{d_3}V_3$.
Therefore we have $\res{\omega_{\PR^2}}{C} = \bigwedge^2\res{\Omega_{\PR^2}}{C} = \Omega_C \otimes \res{\MO_{\PR^2}(-d)}{C}$.
From this, we get $\Omega_C = (\omega_{\PR^2} \otimes \res{\MO_{\PR^2}(d))}{C}$ via the adjuction formula.
$\omega_{\PR^2} = \bigwedge^2\Omega_{\PR^2}$.
Euler Sequence:
\begin{equation*}
  \begin{tikzcd} 0 \arrow{r} & \Omega_{\PR^2} \arrow{r} & \MO(-1)^{\oplus 3} \arrow{r} & \MO_{\PR^2} \arrow{r} & 0 \end{tikzcd}
\end{equation*}
Therefore we have $\omega_{\PR^2} \cong \bigwedge^3(\MO(-1)^{\oplus 3}) = \MO(-1) \otimes \MO(-1) \otimes \MO(-1) = \MO_{\PR^2}(-3)$.
(In general: $\omega_{\PR^n} = \MO_{\PR^n}(-n-1)$).
Therefore $\Omega_C = \res{\MO_{\PR^2}(d-3)}{C}$.
Consider
\begin{equation*}
  \begin{tikzcd}
    0 \ar[r] & \MO_{\PR_2}(-d) \ar[r] & \MO_{\PR^2} \ar[r]  & \MO_C \ar[r] & 0  \\
    0 \ar[r] & \MO_{\PR_2}(-3) \ar[r] & \MO_{\PR^2}(d-3) \ar[r]  & \res{\MO_{\PR^2}(d-3)}{C} \ar[r] & 0  \\
    \mathrm{CONT}
  \end{tikzcd}
\end{equation*}
using that tensoring with $\MO(d-3)$ is exact.
$H^1(C,\MO_C) = k$,
\begin{align*}
  \dim H^0(C,\MO_C) &= \dim H^0(\MO_{\PR^2}(d-3))  \\
                    &= \dim A_{d-3}  \\
                    &= \binom{d-1}{2} = \frac{1}{2}(d-1)(d-2) \; ,
\end{align*}
$\dim_k H^0(C,\MO_C) = 1$, $\dim_k H^1(C,\MO_C) = p_a(C) = \frac{1}{2}(d-1)(d-2)$.
Hodge diamond: (PIK-CHA)
\end{example}
\begin{example}
Let $S \subseteq \PR^3$ be a smooth cubic (degree 3) hypersurface.

We claim that $S \not\cong \PR^1 \times \PR^1$.
\begin{proof}
  We have $\I_{S \subseteq \PR^3} = \MO_{\PR^3}(-3)$, $\I/I^2 \cong \I \otimes_{\MO_{\PR^3}} \MO_{\PR^3/\I} = \I \otimes \MO_S = \res{\I}{S} = \res{\MO_{\PR^3}(-3)}{S}$.
  By the theorem, we have an exact sequence
  \begin{equation*}
    \begin{tikzcd} 0 \arrow{r} & \I/\I^2 \arrow{r} & \res{\MO_{\PR^3}}{S} \arrow{r} & \MO_S \arrow{r} & 0 \end{tikzcd}
  \end{equation*}
  Therefore $\chi(\Omega_S) = \chi(\res{\MO_{\PR^3}}{S}) - \chi(\res{\MO_{\PR^3}(-3)}{S}) = 3 - 10 = -7$ (compute!), which is not equal to $\chi(\MO_{\PR^1\times \PR^1}) = -2$.
\end{proof}
\end{example}
\begin{remark}
  $\sum_p \chi(\Omega^p)y^p$ is called the $\chi_y$-genus.
\end{remark}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeo_master"
%%% End:
