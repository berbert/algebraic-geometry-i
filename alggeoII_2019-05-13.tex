\begin{definition}[very ample]
	A line bundle $\mathcal L \in \Pic(X)$ is called \textbf{very ample} if there exists an immersion $f \colon X \hookrightarrow \PR^n$ such that $f^\ast \MO(1) = \mathcal L$. 
\end{definition}
We want to understand whether a line bundle is very ample or not.
\chapter{Linear Systems}
\begin{definition}[linear system, family of subschemes associated to a linear system]
	Let $X$ be  as scheme over $k$ ($X$ projective, integral, $H^0(X, \MO_X)=k$) and let $\mathcal L$ be a line bundle. A linear subspace $W \subset H^0(X, \mathcal L)$ is called a \textbf{linear system}. A \textbf{family of subschemes associated to $W$} is defined (as a set) as \[L(W) = \setdef{V(s)}{0 \neq s \in W} = \setdef{V(\lambda_0 s_0 + \dots + \lambda_rs_r)}{[\lambda_0, \dots , \lambda_r] \in \PR^r(k)} \cong \PR^r(k)  \,. \] 
\end{definition}
\begin{example}[Prototypical example]
	Consider $f \colon X \to \PR^n$ and let $\mathcal L = \MO_X(1)$. Let \[W = \im(H^0(\PR^n,\MO(1)) \xrightarrow{f^\ast = \varphi} H^0(X,\mathcal L)) \,.\]
	Note that 
	\begin{itemize}
		\item $s \in \ker \varphi$ if and only if $f(X) \subset V(s)$.
		\item $\varphi$ is not always surjective. Consider $X=\{p_1, \dots , p_l \} \subset \PR^1$ with $l \ge 3$. Then $X = C \subset \PR^r$ has a high genus. 
	\end{itemize}
	Let $i \colon X \hookrightarrow \PR^2$ where $C=V(y^2-(x+1)x(x-1))$ (see \Cref{figlinsys}).
	\begin{figure}[h]
		\centering
		\begin{tikzpicture}
		\begin{axis}[
		xmin=-1,
		xmax=3,
		ymin=-3,
		ymax=3,
		xlabel={$x$},
		ylabel={$y$},
		axis equal,
		yticklabels={,,},
		xticklabels={,,},
		axis lines=middle,
		samples=200,
		smooth,
		clip=false,
		]
		\addplot [thick, domain=-1:0] {sqrt(x^3-x)};
		\addplot [thick, domain=-1:0] {-sqrt(x^3-x))};
		\addplot [thick, domain=1:2] {sqrt(x^3-x)};
		\addplot [thick, domain=1:2] {-sqrt(x^3-x))};
		\end{axis}
		\end{tikzpicture}
		\caption{$C=V(y^2-(x+1)x(x-1))$ in $\R^2$}
		\label{figlinsys}
	\end{figure}
	Here, \begin{align*}L(W) &= \setdef{C \cap L}{L \subset \PR^2 \text{ line}} \\&= \setdef{Z = P_1+P_2+P_3, \text{ length $3$ subschemes of } C }{Z \text{ contained in }L} \\&= \setdef{Z= P_1+P_2+P_3, \text{ length $3$ subschemes of } C}{P_1,P_2,P_3 \text{ collinear in } \PR^2}  \,. \end{align*}
	Distingushed family of length $3$ subschemes. All length $3$ subschemes: $\Sym^3(C) = C^3/S^3$ has dimension $3$.
	
	More generally, $W = \im(H^0(\PR^2,\MO(d)) \to H^0(X,\MO_X(d)))$. Here, \[L(W) = \setdef{C \cap V(g) }{g \text{ degree }d, \res{g}{C} \neq 0} \] where $C \cap V(g)$ has length $3$. 
	
	What is $\dim W$?. Consider 
	\[\begin{tikzcd}
	0 \arrow{r} & \MO_{\PR^2}(d-3) \arrow{r}{\cdot s_c} & \MO_{\PR^2}(d) \arrow{r} & \MO_C(d) \arrow{r} & 0 
 	\end{tikzcd} \]
 	%We get \[\begin{tikzcd}
 		%0 \arrow{r} & H^0(\MO_{\PR^2}(d)) \arrow{r} & H^0(\MO_{\PR^2}(d)) \arrow{r} & 0 
 	%\end{tikzcd}\]
 	So, \[\dim W = \binom{d+2}{2} - \binom{d-1}{2} = \frac{(d+2)(d+1)}2 - \frac{(d-1)(d-2)}2 = \frac{3d+2}2 - \frac{-3d+2}2 = 3d \,. \]
 	All length $3d$ subschemes have $\dim 3d$ and $L(W)$ has dimension $3d-1$. The codimension is $1 = p_a(C)$. 
 	
 	More generally, if $C \subset \PR^2$ has degree $m$, smooth. All length $md$ subschems have $\dim md$. Then, $L(W)$ has dimension $\binom{d+2}2 - \binom{d-m+2}2 - 1$ and the codimension is $p_a(C)$ if $d >> 0$ (then $H^2(\MO(d-m+2))=0$)
\end{example}
\begin{definition}[base locus]
	Consider $W \subset H^0(X, \mathcal L)$. Define \[\operatorname{Bl}(W) = \bigcap_{D \in L(W)} D \,,\] the \textbf{base locus}
\end{definition}
We will now construct $L(W)$ as a scheme where $W \subset H^0(X,\mathcal L)$ is a linear system. Let $s_0, \dots , s_r \in W$ be a basis. Let $\PR^r = \Proj k[\lambda_0, \dots , \lambda_r]$ with $\lambda_i \in H^0(\PR^r,\MO(1))$. Consider $\PR^r \times X$ and let $\mathcal L^\prime = p_1^\ast \MO_{\PR^r}(1) \otimes p_2^\ast(\mathcal L)$. Let $t = \sum_{i=0}^r p_1^\ast(\lambda_i) \otimes p_2^\ast(s_i)$. Define $Z_W = V(t) \subset \PR^r \times X$. By construction: For all $\lambda = [\lambda_0, \dots,  \lambda_r] \in \PR^r(k)$, we have $p_1^{-1}(\lambda) = Z_\lambda = V(\lambda_0s_0 + \dots + \lambda_rs_r) \subset X$.
\begin{example}
	Let $f,g \in H^0(\PR^2_k, \MO(3)) = k[x,y,z]_3$ such that $C_1=V(f)$ and $C_2=V(g)$ are distinct nonsingular cubics. Then $L(W) = \setdef{V(\lambda f + \mu g)}{[\lambda,\mu] \in\PR^1_k}$ and $Z_W = V(\lambda f+\mu g) \subset \Pr^1 \times \PR^2$ and there is the projection map $Z_W \to \PR^1$. The space $Z_W$ is called pencil of cubics
\end{example}
\chapter{Divisors}
\section{Cartier Divisors}
Let $X$ be a Noetherian scheme. 
\begin{proposition} \label{fracsheaf}
	There exists a unique sheaf of $\MO_X$-algebras $\mathcal K_X$ such that \[\mathcal K_X(U) = \Frac(A) \] for all affine open $U \subset X$ with $U =\Spec A$. With natural restriction maps we have 
	\begin{enumerate}
		\item $\MO_X \subset \mathcal K_X$
		\item $\mathcal K_{X,x} = \Frac \MO_{X,x}$ for all $x \in X$. 
	\end{enumerate}
\end{proposition}
\begin{definition}[total ring of fractions] 
	Let $A$ be a ring and let $R(A)$ be the multiplicative subset of regular elements in $A$. Define \[\Frac(A) = (R(A)^{-1})A  \,,\] the total ring of fractions. 
\end{definition}
\begin{remark}
	The map \begin{align*}
		l \colon A \longrightarrow \Frac(A) \\
		a \longmapsto \frac{a}1
	\end{align*} is injective. If $l(a)=0$, then $a \cdot s = 0$ for some $s \in R(A)$. Therefore, $a = 0 $. 
\end{remark}
\begin{example} Let $A=k[x,y]/(x,y)$. Here, $\Ass(A)= \{(x),(y)\}$. Therefore, $R(A) = A \setminus ((x) \cup (y)) = \setdef{f \in A}{\text{Image of $f$ in $k[x]$ and $k[y]$ is both non-zero} }$.
\end{example}
\begin{remark}
	If $X$ is integral and $\eta \in X$ the generic point. Then $\MO_{x,\eta}=K(X)$ is called the function field of $X$, then $\mathcal K_X(U) = K(X)$ for all nonempty open $U \subset X$.
\end{remark}
\begin{proof}[Proof of \Cref{fracsheaf}]Let $\mathcal B = \{\text{affine open subsets of } X \}$ which is a basis of a topology of $X$. Define $\mathcal K_X$ as a sheaf on $\mathcal B$, then use $\Sch(\mathcal B) \cong \Sch(X)$. Define a presheaf on $\mathcal K_X$ on $\mathcal B$ by $\mathcal K_X(U) = \Frac A$ (where $U = \Spec A$). Let $\Spec A \subset \Spec B$ and $b \in B$ regular, then $\res{b}{\Spec A}$ is regular in $A$ since $b \in B \iff b_p \in B_p$ is regular for all $p \in B$. 
	So, the restriction maps are well-defined. 
	
	We check that $\mathcal K_X$ is actually a sheaf on $\mathcal B$. Let $U = \Spec A \in \mathcal B$ and $U = \bigcup U_i$ with $U_i = \Spec A_i$ an open affine cover. 
	\begin{itemize}
		\item Uniqueness: Let $\frac a b \in \mathcal K_X(U) = \Frac (A)$, $a \in A$, $b \in A$ regular such that $\frac{a_i}{b_i} = \res{\frac{a_i}{b_i}}{U_i} = 0$ in $\Frac(A_i)$. 
		Let $a_i = \res{a}{U_i}$ and $b_i= \res{b}{U_i}$ which is again regular. 
		Restrict to $D(b_i)$. Then $\frac{a_i}{b_i} = 0$ in $(A_i)_{b_i}$ implies that $a_ib_i^n = 0 $ in $A_i$ and hence $a_i = 0 $ in $A_i$ (since $b_i$) is regular. All in all, $a=0$. 
		\item Gluing: Let $\frac{a_i}{b_i} \in \mathcal K_X(U_i)$ such that $\res{\frac{a_i}{b_i}}{U_{ij}} = \res{\frac{a_i}{b_i}}{U_{ij}}$. By replacing $U_i$ by a refinement we can assume $U_i = \Spec(A_{f_i})$ for distinguished $f_i \in A$. Therefore, $\frac{a_i}{b_i} = \frac{a_j}{b_j}$ in $A_{f_if_j}$ with $a_i \in A_{f_i}$ and a regular $b_i \in A_{f_i}$. 
		
		\textbf{Step 1.} Replace $a_i$ by $f_i^N a_i$ such that $a_i \in A$ and $b_i$ by $f_i^N b_i$ such that $b_i \in A$ for $N>>0$. We get \begin{equation} \label{eq1305}a_ib_j = a_jb_i \end{equation} in $A$. (Can assume that the covering $U_i$ is finite.)
		
		\textbf{Step 2.} We want to find $\alpha, \beta \in A$, $\beta$ regular such that $\frac\alpha\beta = \frac{a_i}{b_i}$ in $A_{f_i}$ which is equivalent to $\alpha = \frac{\beta a_i}{b_i}$. We want $\beta a_i$ to be ``divisible'' by $b_i$ in $A_{f_i}$. 
		Let $I = \setdef{\beta \in A}{\beta a_i \in (b_i) \text{ in } A_{f_i}}$. 
		
		We claim that $I \not\subset \pid$ for any associated prime $\pid \in \Ass(A)$. Let $\pid = \Ann(m)$. Note that $b_i, \dots , b_n \in I$ by \eqref{eq1305}. If $I \subset \pid$, then \begin{align*}
			& & b_im & = 0 \text{ in } A \\
			\implies &&  b_im & = 0 \text{ in } A_{f_i} \\
			\implies & & m & = 0 \text{ in } A_{f_i} & (b_i \text{ regular in } A_{f_i}) \\
			\implies & & m & = 0 
		\end{align*}
		which is a contradiction. 
		
		Now $I \not\subset \pid$ for all $\pid \in \Ass(A)$. We know that $\Ass(A)$ is finite. Hence, by prime avoidance $I \not\subset \bigcup_{\pid \in \Ass(A)} \pid$. There exists a $\beta \in I \setminus \bigcup_{\pid \in \Ass(A)}\pid$ which is equivalent to finding $\beta \in I$ being regular. 
		
		\textbf{Step 3.} Let $\beta \in I$ be regular. Write $\beta a_i = b_i c_i$ with $c_i \in A_{f_i}$. Multiply \eqref{eq1305} by $\beta$. We get $(b_ic_i)b_j = (b_jc_j)b_i$ in $A_{f_if_j}$. This implies $c_i=c_j$ in $A_{f_if_j}$ (since $b_i,b_j$ regular). There exists $c \in A$ such that $c = c_{f_i}$ in $A_{f_i}$. This gives $\frac{c}{\beta} \in \Frac(A)$ which is the desired element. \qedhere
	\end{itemize}
\end{proof}
\begin{definition}[Cartier divisor]
	A \textbf{Cartier divisor $D$ on $X$} is a section of $\mathcal K_X^\ast / \MO_X^\ast$ where $\mathcal K_X^\ast(U)$ and $\MO_X(U)^\ast$ are the units of $\mathcal K_X(U)$ and $\MO_X(U)$, respectively. We write \begin{align*}\Div(X) = H^0(X, \mathcal K_X^\ast/\MO_X^\ast) = \setdef{(U_i,f_i)}{\begin{matrix}U_i \text{ open covering of } X, f_i \in \mathcal K_X^\ast(U_i) \\ f_i/f_j \in \MO_X^\ast(U_i \cap U_j)\end{matrix}}/\sim \end{align*} where $(U_i,f_i) \sim (V_i,g_i)$ if and only if $f_i/g_j \in \MO_X^\ast(U_i \cap V_j)$ for all $i,j$. 
\end{definition}
\begin{lemma} \label{DivPic}
	There is a group homomorphism 
	\begin{align*}
		\Div(X) & \longrightarrow \Pic(X) \\
		D & \longmapsto \MO_X(D)
	\end{align*}
	defined in the following three equivalent ways 
	\begin{enumerate}
		\item By transition functions $\varphi_{ji} = f_j / f_i \in \MO_X^\ast(U_{ij})$
		\item Consider \[\begin{tikzcd}
			0 \arrow{r} & \MO_X^\ast \arrow{r} & \mathcal K_X^\ast \arrow{r} & \mathcal K_X^\ast/\MO_X^\ast \arrow{r} & 0
		\end{tikzcd} \]
		We get $\delta \colon \Div(X) \eqqcolon H^0(X,\mathcal K_X^\ast/\MO_X^\ast) \to H^1(X,\MO_X^\ast) = \Pic(X)$.
		\item Define $\MO_X(D)$ as a subsheaf of $\mathcal K_X$. On $U_i$ we define $\res{\MO_X(D)}{U_i} \coloneqq \frac{1}{f_i} \cdot \MO_{U_i}$. The trivialization is given by \[\begin{tikzcd}
			\frac{1}{f_i} \MO_{U_i} = \res{\MO_X(D)}{U_i} \arrow{r}{\cong}[swap]{\cdot f_i} & \MO_{U_i} \subset \mathcal K_X 
		\end{tikzcd} \]
	\end{enumerate}
	with transition functions \[\begin{tikzcd}
		\MO_{U_{ij}} & \res{\MO_X(D)}{U_{ij}} \arrow{l}{\cdot f_j}[swap]{\varphi_{U_j}} \arrow{r}{\cdot f_i}[swap]{\varphi_{U_i}} & \MO_{U_{ij}} \arrow[bend left]{ll}{\varphi_{ji} = \frac{f_j}{f_i}}
	\end{tikzcd} \]
\end{lemma}
\begin{proof}
	``$(a) \iff (b)$'': If $D$ is given by $\{(U_i,f_i)\}$ look at \v Cech cover with respect to $U_i$. Consider \[\begin{tikzcd}
		& \prod_i \mathcal K_X^\ast(U_i) \arrow{r} \arrow{d} & \prod_i (\mathcal K_X^\ast / \MO_X^\ast)(U_i) \\
		\prod_i \MO_X(U_{ij}) \arrow{r} & \prod_{i,j} \mathcal K_X^\ast (U_{ij}) \arrow{r} & \prod_{i,j} (\mathcal K_X^\ast / \MO_X^\ast)(U_{ij})
	\end{tikzcd}\] 
	An element $(U_i,f_i) \in \prod_i \mathcal K_X^\ast(U_i) $ is mapped to $(U_{ij},f_j/f_i)$ in $\prod_i \MO_X(U_{ij})$.
\end{proof}