\textbf{Goal}: $X$ smooth projective over $k$, $\F$ coherent sheaf
\begin{equation*}
\Ext^i(\F,\omega_{X}) \cong H^{n-i}(X,\F)^{\vee}
\end{equation*}
\begin{definition}
Let $(X,\MO_{X})$ be a scheme, $\F$ be an $\MO_{X}$-module.  
\begin{align*}
  \Ext^i(\C,\strich) \colon \MO_{X}\strich\Mod& \to \MO_{X}(X)\strich\Mod  \\
  \Extc^{i}(\C,\strich) \colon \MO_{X}\strich\Mod&\to \MO_{X}\strich\Mod
\end{align*}
are the $i$th derived functor of $\Hom(\F,\bullet)$ and $\Homc(\F,\bullet)$ respectively.
\end{definition}
\begin{remark}\label{property}
  We have the following properties
  \begin{enumerate}
  \item For $U\subseteq X$ open, we have $\res{\Extc^i(\F,\G)}{U}=\Extc^{i}(\res{F}{U},\res{\G}{U})$
  \item Let \[\begin{tikzcd} 0 \arrow{r} & \F \arrow{r} & \F^{\prime}\arrow{r} & \F^{\prime\prime}\arrow{r} & 0 \end{tikzcd} \] be a short exact sequence.
    Then there is a long exact sequence 
    \[ \begin{tikzcd}
      0 \arrow{r} & \Hom(\F^{\prime\prime},\G) \arrow{r} & \Hom(\F^{\prime},\G) \arrow{r} \arrow[d, phantom, ""{coordinate, name=Z}] & \Hom(\F,\G) \arrow[dll,
      rounded corners,
      to path={ -- ([xshift=2ex]\tikztostart.east)
      	|- (Z) [near end]\tikztonodes
      	-| ([xshift=-2ex]\tikztotarget.west)
      	-- (\tikztotarget)}]  \\
        & \Ext^{1}(\F^{\prime\prime},\G) \arrow{r} & \Ext^{1}(\F^{\prime},\G) \arrow{r} & \dotsb 
       \end{tikzcd}
    \]
    and the same holds for $\Extc^{i}$.
  \end{enumerate}
\end{remark}
\begin{proof}~ \vspace{-\topsep}
	\begin{enumerate}
	\item Let $j \colon U \hookrightarrow X$ be the inclusion.
	$F_{i} = j^\ast \circ \Extc_{\MO_X}^i(\F,-)$, $G_i = \Extc_{\MO_U}^i(\res{\F}{U},-) \circ j^\ast$.
	$j^\ast$ is exact, therefore $F_i$ and $G_i$ are $\delta$-functors.
	For $i=0$, $F_0(\G) = \res{\Homc_{\MO_X}(\F,\G)}{U} = \Homc_{\MO_U}(\res{\F}{U},\res{\G}{U}) = G_0(\G)$, so $\F_0 = \G_0$.
	
	$F_i$ and $G_i$ are minimal, that is $F_i(\mathcal I) = 0$ and $G_i(\mathcal I)=0$ for all injectives $I$ since
	$F_i(\mathcal I) = j^\ast \Extc^i(\F,\mathcal I)$ and $\Extc^i(\F,\mathcal I) = 0$ by the definition of right derived functors.
	Furthermore, $G_i(\mathcal I) = \Extc^i(\res{\F}{U},\res{\mathcal I}{U}) = 0$ since as $I$ is injective, so is $\res{\mathcal I}{U}$.

\item   Let \[ \begin{tikzcd}  0\arrow{r} & \G \arrow{r} & \I^{0} \arrow{r}{d} & \I^{1}\arrow{r}{d}& \I^{2} \arrow{r} & \dots \end{tikzcd} \] be an injective resolution.
  Apply $\Hom(-,\I^{j})$ to this sequence.
  \begin{equation*}
    \begin{tikzcd}
      0 \ar[r] & \Hom(\F^{\prime\prime},\I^{0}) \ar[d,"d"] \ar[r]&  \Hom(\F^{\prime},\I^{0}) \ar[d,"d"] \ar[r] & \Hom(\F,\I^{0}) \ar[d,"d"] \ar[r]& 0  \\
      0 \ar[r] & \Hom(\F^{\prime\prime},\I^1) \ar[d,"d"] \ar[r]&  \Hom(\F^{\prime},\I^1) \ar[d,"d"] \ar[r] & \Hom(\F,\I^1) \ar[d,"d"] \ar[r]& 0  \\
      &\vdots&\vdots&\vdots& \; .
    \end{tikzcd}
  \end{equation*}
  So we have a long exact sequence in cohmology, which shows what we want.
  
  For the sheafified $\Extc$ we do the same argument and the following claim:
  $\I$ injective implies that $\Homc(-,\I)$ is an exact functor.

  To see this claim, consider the following for any $U \subseteq X$: 
  \begin{equation*}
    \begin{tikzcd}
      0 \ar[r] & \Homc(\F^{\prime\prime},I)(U) \ar[d,equals] \ar[r] & \Homc(\F^{\prime},I)(U) \ar[d,equals] \ar[r] & \Homc(\F,I)(U) \ar[d,equals] \ar[r,dashrightarrow,"\text{?}"]  & 0\\
      0 \ar[r] & \Hom(\res{\F^{\prime\prime}}{U},\res{I}{U}) \ar[r] & \Hom(\res{\F^{\prime}}{U},\res{I}{U}) \ar[r] & \Hom(\res{\F}{U},\res{I}{U}) \ar[r] & 0
    \end{tikzcd}
  \end{equation*}
  Since $I$ is injective, so is $\res{I}{U}$, so the sequence is exact on the right, so the top sequence is exact.
  This shows the claim. \qedhere
  \end{enumerate}
\end{proof}
\begin{remark}[Property 3]\label{property3}
  We have the following property $(\ast)$:
  Let $\F$ be a locally free sheaf of finite rank.
  Then $\Extc^i(\F,\G)=0$ for all $\G$ and all $i >0$.
\end{remark}
\begin{proof}
  Let $U\subseteq X$ such that $\res{\F}{U}= \MO_{U}^{\oplus r}$.
  Then $\res{\Extc^{i}(\F,\G)}{U}= \Extc^{i}(\res{\F}{U},\res{G}{U}) = \Extc^{i}(\MO_{U},\res{\G}{U})^{\oplus r}$ (since $\Extc^{i}(\MO_{U},-)$ is the right derived function of $\Homc(\MO_{U},-) = \id$).
\end{proof}
\begin{remark}
  More generally, let $\F$ be a sheaf with a resolution \[\begin{tikzcd} \dotsb \arrow{r} & \mathcal L_{-2} \arrow{r} & \mathcal L_{-1} \arrow{r} & \mathcal L_{0} \arrow{r} & \F \arrow{r} & 0 \end{tikzcd}\] such that
  $\mathcal L_{i}$ locally free of finite rank.
  Then \[ \Extc^{i}(\F,\G)\cong H^i(\Homc(\mathcal L_{\bullet},\G)) \] (apply $\Hom(-,\G)$) 
\end{remark}
\begin{proof}
  \begin{itemize}
  \item For $i=0$, $0 \to \Homc(\F,\G) \to \Homc(\L_0,\G) \to \Homc(\L_\bullet,\G)$, so $H^0(\Homc(\L,\G)) \cong \Homc(\F,\G) = \text{LHS}$.
  \item Both sides define $\delta$-functors (for the right hand side use $(\ast)$ and the argument of the proposition (?)).
  \end{itemize}
\end{proof}
\begin{remark}\label{property4}
  Let $\mathcal L$ be locally free of finite rank, $\F,\G$ arbitrary.
  Then
  \begin{enumerate}
  \item $\Ext^{i}(\F\otimes\mathcal L,\G) \cong \Ext^{i}(\F,\mathcal L^{\vee}\otimes\G)$
  \item $\Extc^{i}(\F\otimes\mathcal L,\G) \cong \Extc^{i}(\F,\mathcal L^{\vee}\otimes\G) \cong \Extc^{i}(\F,\G)\otimes \mathcal L^{\vee}$
  \end{enumerate}
\end{remark}
\begin{proof}~ \vspace{-\topsep}
  \begin{enumerate}
  \item ($i=0$) We have $\Hom(\F\otimes\mathcal L,\G) = \Hom(\F,\Homc(\mathcal L,\G)) = \Hom(\F,\mathcal L^{\vee}\otimes\G)$.
    Both sides are $\delta$-functors in $\G$.
    Minimality: Let $\I$ be injective.
    If $\I$ is injective, then $\I\otimes\mathcal L^{\vee}$ is injective.
    This is because $\Hom(-,\I\otimes\mathcal L^{\vee}) = \Hom(-\otimes\mathcal L,\I) = \Hom(-,\I) \circ \mathcal L \otimes(-)$ (exactness where $-$). \qedhere
  \end{enumerate}
\end{proof}
\begin{remark}\label{property5}
  Let $\F$ be a coherent sheaf.
  \begin{enumerate}
  \item $\Extc^{i}(\F,\G)_{x} = \Ext^{i}_{\MO_{X,x}}(\F_{x},\G_{x})$ ($\G$ arbitrary)
  \item If $\F$, $\G$ are coherent, then $\Extc^{i}(\F,\G)$ is coherent ($\F$, $\G$ quasi-coherent, then $\Homc(\F,\G)$ is quasi-coherent and $\Homc(\F,\G)_{x} = \Homc(\F_{x},\G_{x})$).
  \end{enumerate}
\end{remark}
\begin{proof}~ \vspace{-\topsep}
	\begin{enumerate}
  \item Assume $X = \Spec A$ is affine, $\F = \widetilde M$.
  Then there exists a resolution \[\begin{tikzcd} \dotsb \arrow{r} & \mathcal L_{-2} \arrow{r} & \mathcal L_{-1} \arrow{r} & \mathcal L_{0} \arrow{r} & \F \arrow{r} & 0 \end{tikzcd} \,. \]
  Apply $\Homc(\bullet,\G)$:
  INSERT PHOTOGRAPHED DIAGRAM
\item $\mathcal L_{i}$, $\G$ coherent, then $\Homc(\mathcal L_{i},\G)$  is coherent.
  The category of coherent sheaves is abelian, so $H^{i}(\Homc(\mathcal L_{\bullet},\G))$ is coherent. \qedhere
  \end{enumerate}
\end{proof}
\section{Serre duality for $\PR^{n}_{k}$}
Recall that we proved that
\begin{equation*}
  H^{0}(\PR^{n},\MO(d)) \otimes H^{n}(\PR^{n},\MO(-d-n-1)) \to H^{n}(\PR^{n},\MO(-n-1)) \cong k
\end{equation*}
is a perfect pairing. By replacing $d$ with $-d-n-1$ we receive
\begin{align*}
  \Hom(\MO(d),\omega_{\PR^{n}}) &= \Hom(\MO_{\PR^{n}},\MO(-d)\otimes\omega_{\PR^{n}}) \\& = H^{0}(\PR^{n},\MO(-d)\otimes\omega_{\PR^{n}}) \otimes H^n(\PR^{n},\MO(d)) \to H^{n}(\PR^{n},\omega_{\PR^{n}})
\end{align*}
is a perfect pairing.
\begin{theorem}[Serre duality for $\PR^n$]
  Let $\F$ be a coherent sheaf over a field $k$.
  \begin{enumerate}
  \item We have a perfect pairing
    \begin{align*}
      \Hom(\F,\omega_{\PR^{n}}) \times H^n(X,\F) &\longrightarrow H^{n}(\PR^{n},\omega_{\PR^{n}}) \cong k  \\
      (g,\alpha) &\longmapsto g_{\ast}(\alpha)
    \end{align*}
  \item There exists natural functional isomorphisms
    \begin{equation*}
      \Ext^{i}(\F,\omega_{\PR^{n}}) \to H^{n-i}(\PR^{n},\F)^{\vee} \; ,
    \end{equation*}
    where for a $k$-vector space $V$ over $k$ we write $V^{\vee} = \Hom_{k}(V,k)$.
  \end{enumerate}
\end{theorem}
\begin{proof}
  Let $\F$ be a coherent sheaf on $\PR^{n}$.
  There exists a resolution \[ \begin{tikzcd} \bigoplus_{i=1}^{b} \MO(m_{i}) \arrow{r} & \bigoplus_{i=1}^{a}\MO(n_{i}) \arrow{r} & \F \arrow{r} & 0 \end{tikzcd} \,.\]
  After applying $\Hom(-,\omega_{\PR^{n}})$ we get
    \begin{equation*}
      \begin{tikzcd}
        0 \ar[r] &\Hom(\F,\omega_{\PR^{n}}) \ar[r] \ar[d,"\text{$\cong$ by $5$-lemma}"] & \bigoplus_i \Hom(\MO(n_{i}), \omega_{\PR^{n}}) \ar[r] \ar[d,"\cong"] & \bigoplus_{i}\Hom(\MO(m_i),\omega_{\PR^{n}}) \ar[d,"\cong"]  \\
        0  \ar[r] & H^{n}(\PR^{n},\F) \ar[r]& \bigoplus_{i} H^{n}(\PR^{n},\MO(n_{i}))^{\vee} \ar[r] &\bigoplus_{i} H^{n}(\PR^{n},\MO(m_{i}))^{\vee} \; .
      \end{tikzcd}
    \end{equation*}
    We check that both sides define a universal $\delta$-functors.
    The inital term is the same by (a).
    $\delta$-functor in $\F$: LHS: \Cref{property}.
    RHS: Proposition of sheaf cohomology. \qedhere
\end{proof}
We need a weaker/stronger minimality/uniqueness statement.
\begin{definition}[effaceable]
  A covariant functor $T\colon \mathcal{C} \to \mathcal \mathcal {D}$ is \textbf{effaceable} iff for all $A \in \mathcal{C}$ there exists a monomorphism $u \colon A \hookrightarrow B$ such that $T(u)=0$.
\end{definition}
\begin{theorem}[Grothendieck]
  A $\delta$-functor $(T^{i})_{i\geq0}$ such that every $T^{i}$ is effaceable is universal.
  If $\mathcal{C}$ has enough injectives, then $T^{i}=R^{i}(T^{0})$
\end{theorem}
\begin{remark}
  Parallel for contravariant $\delta$-functors, $T$ is \textbf{coeffaceable} if for all $A \in \mathcal{C}$ there exists an epimorphism $u \colon B \twoheadrightarrow$ such that $T(u) = 0$.
\end{remark}
We need to check that $\Ext^{i}(\F,\omega_{\PR^{n}}) \cong H^{n-i}(X,\F)^{\vee}$.
We check that these are coeffaceable in $\F$:
For every $d\gg 0$ there exists an $r$ and a surjection $\bigoplus \MO(-d)^{r} \twoheadrightarrow\F \to 0$

Why?
$\bigoplus_{i=1}^{a} \MO(n_{i}) \to \F \to 0$.
Then replace $\MO(-n_{i})$ by $\MO(-(n_{i}-1))^{\oplus (n+1)}$ using the map $(\MO_{\PR^{n}}(-1)^{\oplus n+1} \twoheadrightarrow \MO_{\PR^{n}})\otimes \MO(-n_{i})$.

For $d\gg 0$, $i>0$,
\begin{align*}
\Ext^i(\bigoplus\MO(-d),\omega_{PR^{n}}) &= 
\end{align*}
CONT CONT
\section{Serre Duality for Projective Varieties}
Motivation: Let $i=0$.
We want $\Hom_X(\F,\omega_X^0) \xrightarrow{\sim} H^{n}(X,\F)^{\vee}$.
By Yoneda's lemma this relation determines $\omega_{X}^{0}$ (if it exists).

Consider $j \colon X \hookrightarrow \PR^{n}$ a closed immersion
(where $r=N-n$ with $n = \dim X$).

We have \[H^{n}(X,\F) \cong H^{n}(\PR^{n},j_{\ast}\F) \cong \Ext^{r}(j_{\ast}\F,\omega_{\PR^{n}})^{\vee} \,,\] which gives us \[\Hom_{X}(\F,\omega_{X}^{0}) \cong \Ext^{r}_{\PR^{N}}(j_{\ast}\F,\omega_{\PR^N}) \,.\]
Assume we have that relation on the sheafified $\Extc$/$\Homc$, so \[\Homc_{X}(\F,\omega_{X}^{0}) \cong \Extc_{\MO_{\PR}}^r(j_{\ast}\F,\omega_{\PR}) \,.\]
Take $\F = \MO_{X}$, then we have
\[\omega^{0}_{X} = \Homc_{\MO_{X}}(\MO_{X},\omega_{X}^{0}) = j^{\ast}\Extc^{r}_{\MO_{\PR}}(j_{\ast}\MO_{X},\omega_{\PR}) \,.\]
We need to prove that \[\Hom_{X}(\F,\Extc_{\MO_{\PR}}^{r}(j_{\ast}\MO_{X},\omega_{\PR})) \cong \Ext^{r}_{\MO_{\PR}}(j^{\ast}\F,\omega_{\PR}) \,.\]
\begin{definition}[dualizing sheaf]
  A coherent sheaf $\omega_{X}^{0}$ together with a morphism $\tr \colon H^{n}(X,\omega_{X}^{0}) \to k$ is called a \textbf{dualizing sheaf} if the natural map \[ \Hom(\F,\omega_{X}^{0}) \otimes H^{n}(X,\F) \to H^{n}(X,\omega_{X}^{0}) \xrightarrow{tr} k \] is perfect for all coherent sheaves $\F$.
\end{definition}
\begin{remark}
  $\omega_{X}^{0}$ is unique if it exists.
\end{remark}
\begin{theorem} \label{mainforserre}
  $\Extc^{r}(j_{\ast}\MO_{X},\omega_{\PR^{N}})$ is a dualizing sheaf on $X$ (with trace to be defined later).
\end{theorem}
Motivation continued.
Back to $i\geq 0$.
We need to proove that
\begin{align*}
  \Ext^{i}(\F,\omega_{X}^{0}) & =  H^{n-i}(X,\F)^{\vee}  \\
  &= H^{n-i}(\PR^{N},j_{\ast}\F)^{\vee}  \\
  &= \Ext^{r+i}(j_{\ast}\F,\omega_{\PR^{N}})
\end{align*}
or equivalently \[\Hom(\F,\Extc^{r}(j_{\ast}\MO_{X},\omega_{\PR})) \cong \Ext^{r}(j_{\ast}\F,\omega_{\PR}) \,,\]
or the sheafified version \[\Homc_{\MO_{X}}(\F,\Extc^{r}(j_{\ast}\MO_{X},\omega_{\PR})) \cong \Extc^{r}_{\MO_\PR}(j_{\ast}\F,\omega_{\PR}) \,.\]

Key input:
There is an adjuction.
For $\G \in \cat{Coh}(\PR^{N})$
\begin{equation*}
  \Homc_{\MO_X}(\F,\Homc_{\MO_X}(j_\ast \MO_X,\mathcal G)) \cong \Homc_{\MO_\PR}(j_\ast \F,\mathcal G)
\end{equation*}
Derive this adjunction via composition of functor spectral sequence will yield what we need to show.

For an $A$-module $M$ and a $B$-module $N$ and $A$ a $B$-algebra $B\to A$, there exists an adjunction $\Hom_A(M,\Hom_B(A,N)) \cong \Hom_B(_BM,N)$.
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeo_master"
%%% End:
