%\begin{theorem}[Hilbert, Serre]
%  Let $\F$ be a coherent sheaf on $\PR_k^n$.
%  There is a polynomial $P_\F$ such that $\chi(\PR^n,\F(m)) = %P_\F(m)$ for all $ m \in \Z$.
%  \begin{enumerate}
%  \item $O_\F(m) = h^0(\PR^n,\F(m))$ for all $m \gg 0$
%  \item $\deg P_\F = \dim \operatorname{supp} \F$
%  \item $P_\F(m) = \frac{a_r}{r!}m^r + \frac{a_{r-1}}{(r-1)!} %m^{r-1} + \dotsb$, $a_r \neq 0$.
%    Then $a_r > 0$, $a_r \in \Z$.
%  \end{enumerate}
%\end{theorem}
%\begin{remark}
%  $\dim \F := \dim \operatorname{supp} \F$
%\end{remark}
The strategy of the proof of \Cref{hilbert} is the following: induction on $\dim \supp \F$.
	
	Let $f \in H^0(\PR^n,\MO(1))$, $H = V(f)$ be a hyperplane.
	\begin{equation*}
	\begin{tikzcd}
	0 \ar[r] & \MO_{\PR^n}(-1) \ar[r,"\cdot f"] & \MO_{\PR^n} \ar[r] & \MO_H \ar[r] & 0
	\end{tikzcd}
	\end{equation*}
	Apply $\otimes_{\MO_{\PR^n}} \F$ (right-exact):
	\begin{equation*}
	\begin{tikzcd}
	\F(-1) \ar[r,"\cdot f"] & \F \ar[r] & \res{\F}{H} \ar[r] & 0
	\end{tikzcd}
	\end{equation*}
	\textbf{Problem:} We need to find an $f$ such that
	\begin{itemize}
		\item $\F(-1) \xrightarrow{\cdot f} \F$ injective
		\item $\dim \res{\F}{H} < \dim \F$.
	\end{itemize}
\section{Associated Points in Commutative Algebra}
Let $A$ be a ring, $M$ an $A$-Module.
Does there exist an $f \in A$ such that $M \xrightarrow{\cdot f} M$ is injective?
Or equivalently: Is there an $f \in A$ such that $f \not\in \operatorname{Ann}(m)$ for all $0 \neq m \in M$?

\begin{definition}[annihilator]
  Let $m \in M$.
  Then the \textbf{annihilator ideal of $m$} is defined as \[ \Ann(m) = \setdef{a \in A}{a\cdot m = 0} \, .\]
\end{definition}
\begin{remark}
  We have \[V(\Ann(m)) = \supp(m) = \setdef{\pid \in \Spec A}{m \neq 0 \in M_\pid}  \,.\]
\end{remark}
\begin{definition}[associated prime]
	If $\Ann(m)$ is a prime ideal, we call it an \textbf{associated prime of $M$}. We write
	\[\operatorname{Ass}(M) = \set{\text{associated primes of $M$}} \,.\]
\end{definition}
\begin{example}
  Let $A = k[x,y]$.
  \begin{itemize}
  	\item Let $M = k[x,y]/(xy) = \MO_Z$.
  Then $\Ann(x) = (y)$, $\Ann(y) = (x)$, $\Ann(1) = (xy) = (x) \cap (y)$, $\Ann(x+y) = (xy)$.
  Sp, $\supp(M) = V(xy) = V(x) \cup V(y) = \overline{\set{x}} \cup \overline{\set{y}}$.
  \item Let $M = k[x,y]/(xy,y^2)$.
  Then $\supp(M) = \overline{\set{(y)}}$
  $\Ann(x) = (y)$, $\Ann(y) = (x,y)$, $\Ann(x+y) = (y)$
  We have $\operatorname{Ass}(M) = \set{(y),(x,y)}$, and $(x,y)$ is the ``embedded prime''.
  \end{itemize}
\end{example}
\begin{lemma}
  If $I$ is maximal (with respect to inclusion) among $\setdef{\Ann(m)}{0 \neq m \in M}$, then $I \in \operatorname{Ass}(M)$.
\end{lemma}
\begin{proof}
  Let $x,y \in A$.
  If $xy \in I$, but $x \not\in I$, and $y \not\in I = \Ann(m)$, then $\Ann(xm) \supseteq \Ann(m)$ and $y \in \Ann(xm)$.
  If $I$ is maximal, this is a contradiction.
  So $I$ must be prime.
\end{proof}
\begin{corollary} \label{cor0502}
	We have $\operatorname{Ass}(M) \neq \emptyset$, unless $M = 0$.
\end{corollary}
Now we see that asking if there exists an $f \in A$ such that $M \xrightarrow{\cdot f} M$ is injective is equivalent to asking if there exists an $f \in A$ such that $f \not\in \pid$ for all $\pid \in \operatorname{Ass}(M)$, or $\pid \not\in V(f)$.
\begin{example}
  Let $A$ be a ring, $M= A/\pid$ for a prime ideal $\pid$.
  Let $0 \neq m \in M$, then $\Ann(m) = \setdef{a \in A}{a\cdot m = 0}$.
  Therefore $\operatorname{Ass}(A/\pid) = \set{\pid}$
\end{example}
\begin{lemma}\label{lem20502}
  Let $0 \to M_1 \to M_2 \to M_3 \to 0$ be a short exact sequence.
  Then $\operatorname{Ass}(M_2) \subseteq \Ass(M_1) \cup \operatorname{Ass}(M_3)$.
\end{lemma}
\begin{proof}
  Let $m \in M_2$ such that $\Ann(m) = \pid$ is prime.
  We have an inclusion $\psi \colon A/\pid \hookrightarrow M$, $a \mapsto a\cdot m$. Note $A\cdot m = \im(\psi)$.

  \textbf{Case 1.} 
  If $A \cdot m \cap M_1 \neq 0$, let $n \in A\cdot m \cap M_1 \cong A/p \cap M_1$.
  Then by our example we have $\Ann(n) = \Ann(m) = \pid \in \operatorname{Ass}(M_1)$.
  
  \textbf{Case 2.} $\A/p \hookrightarrow M_2 \xrightarrow{\phi} M_3$ is injective, so $\Ann(\phi(m)) = \Ann(m) = \pid \in \operatorname{Ass}(M_3)$.
\end{proof}
\begin{lemma}\label{lem30502}
  Let $A$ be Noetherian, $M$ a finitely generated $A$-module.
  Then there exists a filtration $0 = M_0 \subseteq M_1 \subseteq M_2 \subseteq \dotsb \subseteq M_n = M$ such that $M_i / M_{i-1} \cong A/\pid_i$ for some $\pid_i \in \Spec A$.
\end{lemma}
\begin{proof}
  By the \Cref{cor0502}, $\operatorname{Ass}(M) \neq 0$, so there exists an inclusion $0 \to A/\pid \xrightarrow{i_1} M$.
  Let $M_1 = \coker(i_1)$.
  Then repeat.
\end{proof}
\begin{lemma} \label{lem40502}
  Let $A$ Noetherian and let $S \subseteq A$ be a multiplicative subset.
  Then $\operatorname{Ass}_{S^{-1}A}(S^{-1}M) = \operatorname{Ass}(M) \cap \Spec(S^{-1}A)$.
\end{lemma}

\begin{theorem}
  Let $A$ be Noetherian, and $M$ finitely generated.
  Then
  \begin{enumerate}
  \item $\operatorname{Ass}(M)$ is a finite set
  \item $\operatorname{Ass}(M) \subseteq \supp(M)$
  \item The minimal elements of $\operatorname{Ass}(M)$ agree with the minimal elements of $\supp(M)$, that is the generic points of $\supp(M)$.
  \end{enumerate}
\end{theorem}
\begin{proof}~ \vspace{-\topsep}
  \begin{enumerate}
  \item Combine \Cref{lem30502} and \Cref{lem20502}.
  \item Let $\pid \in \operatorname{Ass}(M)$, $\pid = Ann(m)$.
    Then we have an inclusion $A/\pid \hookrightarrow M$, $a \mapsto a\cdot m$, and $(A/\pid)_\pid \hookrightarrow M_\pid$.
    Therefore $M_\pid \neq 0$, and $\pid \in \supp M$.
  \item The minimal elements of $\supp (M)$ lie in $\operatorname{Ass}(M)$.
    To see this, let $\pid \in \supp (M)$ be minimal, and consider $0 = (M_0)_\pid \subseteq \dotsb \subseteq (M_n)_\pid = M_p \neq 0$.
    Let $i$ be smallest such that $(M_i)_\pid = (A/\pid_i)_\pid \neq 0$.
    Then $(A/\pid_i)_\pid \hookrightarrow M_\pid$, and we have $\pid_i \in \operatorname{Ass}_{A_\pid}(M_\pid) = \operatorname{Ass}(M) \cap \Spec A_\pid$ by \Cref{lem40502}, and $\pid_i \in \operatorname{Ass}(M)$ implying $\pid_i \in \supp(M)$ but $\pid_i \subseteq \pid$, so $\pid_i = \pid$.
    This shows the claim.

    Now we have $\set{\text{min. elements of $\supp(M)$}} \subseteq \operatorname{Ass}(M) \subseteq \supp(M)$. \qedhere
  \end{enumerate}
\end{proof}

\section{Associated Points and the Proof of Hilberts Theorem}
Let $X$ be a Noetherian scheme, $\F$ coherent.

\begin{definition}[associated prime]
  A point $p \in X$ is an \textbf{associated prime} (or point) of $\F$ if one of the following equivalent conditions hold.
  \begin{enumerate}
  \item There exists an open affine $U = \Spec A$ and $p \in U$ such that $p \in \operatorname{Ass}(M)$ where $\res{\F}{U} = \widetilde M$.
  \item $ m_p \in \operatorname{Ass}(M_p)$ where $m_p \subseteq \MO_{X,p}$ is the maximal ideal.
  \item There exists an open $p \in U \subseteq X$ and an $s \in \F(U)$ such that $\supp(s) = \overline{\set{p}} \cap U$.
  \end{enumerate}
\end{definition}

\begin{corollary}~ \vspace{-\topsep}
  \begin{enumerate}
  \item $\operatorname{Ass}(\F)$ is finite.
  \item $\operatorname{Ass}(\F) \subseteq \supp (\F)$
  \item minimal elements of $\operatorname{Ass}(\F)$ and $\supp(\F)$ agree.
\end{enumerate}
\end{corollary}

\begin{definition}[$\F$-regular]
  Let $\F$ be a coherent sheaf, $\mathcal L$ a line bundle.
  \begin{enumerate}
  \item A section $s \in \Gamma(X,\mathcal L)$ is \textbf{$\F$-regular} if $\F \otimes L^\vee \xrightarrow{\cdot s} \F$ is injective
  \item A sequence $s_i \in \Gamma(X,\mathcal L_i)$, $1 \leq i \leq n$, is \textbf{$\F$-regular} if $\F_i \otimes \mathcal L_i^\vee \xrightarrow{\cdot s_1} \F_i$ is injective, where $\F_1 = \F$ and $\F_{i+1} = \F_{i}/s_{i-1}(\F_{o}\otimes\mathcal L^\vee)$.
  \end{enumerate}
\end{definition}

\begin{lemma}
  Let $\F$ be a coherent sheaf on $\PR^n_k$ of dimension $r$.
  Assume $k$ is an infinite field.
  Then there exist $\F$-regular sequence $s_1,\dotsc,s_r \in H^0(\PR^n,\MO(1))$.
\end{lemma}

\begin{proof}
  Let $p_1,\dotsc,p_l$ be the associated primes of $\F$.
  Consider $p_i \cap A_1 \subsetneq A_1 = k^{n+1}$, $A = \pol{k}{x}{0}{n}$ (proper subspace since $p_i \neq A_+$).
  As $k$ is infinite, there exists $f \in A_1 \setminus \bigcup_{i=1}^l (p_i \cap A_i)$.
  Let $s_1 = f$.
  Then $p_i \not\in V(f_i)$ by construction.
  So $\F(-1) \xrightarrow{\cdot s_1} \F $ is injective (our question in the beginning of this chapter).
  Also $ 0 \to \F(-1) \to \F \to \res{\F}{H} \to 0$ is exact, and $\dim(\res{\F}{H}) < \dim \F$
  (If $\supp \F = Z_1 \cup \dotsb \cup Z_m$, $Z_i$ irred. comp, $Z_i = \overline{\set{p_i}}$.
  Then $p_i \in \operatorname{Ass}(\F)$, so $p_i \not\in V(f)$, so $Z_i \cap V(f) \subsetneq Z_i$ for all $i$, in fact $\dim(\res{\F}{H}) = \dim(\F) - 1$.)
\end{proof}

\begin{lemma}\label{hilem}
  Let $f \colon \Z \to \Z$ be a function such that for all $m \in \Z$ we have $f(m) - f(m-1) = Q(m)$ for some polynomial $Q$.
  Then $f(m) = p(m)$ for some polynomial $p$.
\end{lemma}

\begin{example}  
  We have $f(m) = f(m-1) + Q(m) = f(m-2) + Q(m-1) + Q(m) = \dotsb = Q(0) + \dotsb + Q(m) + f(-1)$

  For $Q=m^i$, we get $f(m) = 1^i + 2^i + \dotsb + m^i$.
\end{example}

\begin{proof}
  We have $\binom{n}{k} + \binom{n}{k+1} = \binom{n+1}{k+1}$, and $\binom{n+1}{k+1} - \binom{n}{k+1} = \binom{n}{k}$, $\binom{m}{l} + \binom{m-1}{l} = \binom{m-1}{l-1}$.
  If $Q(m) = \binom{m-1}{l-1}$, then $f(m) = \binom{m}{l} + \mathrm{const}$ is polynomial. Use that these binomial coefficients form a basis of all polynomials. 
\end{proof}

\begin{proof}[Proof of \Cref{hilbert}.]
  Let $\F$ be a coherent sheaf on $\PR^n$.
  Pick $s_1 \in H^0(\PR^n,\MO(1))$ $\F$-regular, $H = V(s)$.
  Then $0 \to \F(-1) \to \F \to \res{\F}{H} \to 0$ is exact (since $s_1$ is $\F$-regular).
  Apply $\otimes\MO(m)$, and we get an exact sequence $0 \to \F(m-1) \to \F(m) \to \res{\F}{H}(m) \to 0$, and we have $\chi(\F(m)) - \chi(\F(m-1)) = \chi(\res{\F}{H}(m))$, which is polynomial in $m$.

  As a consequence of \Cref{hilem}, we $\chi(\F(m))$ is polynomial in $m$ by induction on $\dim \supp \F$.

  \textbf{Claim:} Let $r = \dim \supp \F$, and let $s_1,\dotsc,s_r \in H^0(\PR^n(\MO(1)))$ be an $\F$-regular sequence.
  Then $P_\F(m) = \sum_{i=0}^r \chi(\res{\F}{H_1 \cap \dotsb \cap H_i}) \cdot \binom{m+i-1}{i}$, where  $H_i = V(s_i)$ hyperplane.
  $\binom{m+i-1}{i}$ has degree $i$.

  If we assume the claim, we have:

  \begin{itemize}
  \item $P_\F(0) = \chi(\F)$, as $\binom{i-1}{i} = \begin{cases}0 & \text{if $i\neq0$}\\1 &\text{else}\end{cases}$.
  \item (b),(c):
    The leading coefficient is
    $\chi(\res{\F}{H_1 \cap \dotsb \cap H_r}) \cdot \frac{m^r}{r!}$, and we have $\chi(\res{\F}{H_1 \cap \dotsb \cap H_r}) >0$ % $(\ast)$.
    since $\res{\F}{H_1 \cap \dotsb \cap H_r}$ is coherent, nonzero, $0$-dimensional.
    So $\chi(\res{\F}{H_1 \cap \dotsb \cap H_r}) = h^0(\res{\F}{H_1 \cap \dotsb \cap H_r}) > 0$.
  \end{itemize}
	Now to prove the claim.
	By induction we know $P_{\res{\F}{H}}(m) = \sum_{i=1}^r \chi(\res{\F}{H_1 \cap \dotsb \cap H_i}) \cdot \binom{m+i-2}{i-1}$.
	So from $P_\F(m) - P_\F(m-1) = P_{\res{\F}{H}}(m)$ and \Cref{hilem}, we have $p_\F(m) = \sum_{i=1}^r \chi(\res{\F}{H_1 \cap \dotsb \cap H_r}) \cdot \binom{m+i-1}{i} + \mathrm{cst}$ and $\mathrm{cst} = P_\F(0) = \chi(\F)$. 
\end{proof}

\begin{remark}
  If $\F = \MO_Z$, $Z \subseteq \PR^n$ is a closed subscheme of dimension $r$, then $\deg Z = \text{(leading coeff of $P_\F$)} \cdot r^\prime = \chi(\res{Q_Z}{H_1 \cap \dotsb \cap H_r}) = \text{``\# of pts with mult $h^0$ or less'' of $Z\cap V$}$, $V \subseteq \PR^n$ ``general'' codim $r$ linear subspace in $\PR^n$ \dots.
  ($H_1 \cap \dotsb \cap H_r$ codim $r$ linear subspace.)
\end{remark}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeo_master"
%%% End:
