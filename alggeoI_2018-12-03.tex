\section{Immersions}
\begin{definition}[open immersion]
	A morphism $j \colon Y \to X$ is an \textbf{open immersion} if 
	\begin{enumerate}
		\item the underlying continuous map of $j$ is a homeomorphism onto an open subset $U \subset X$. 
		\item $j^\# \colon \MO_X \to j_\ast \MO_Y$ induces an isomorphism $\res{\MO_X}{U} \to \tilde j_\ast \MO_Y$ where $\tilde j \colon Y \to U$.
	\end{enumerate}
	All in all, $\tilde j \colon (Y,\MO_Y) \to (U,\res{\MO_X}{U})$ is an isomorphism.
\end{definition}
\begin{definition}[closed subscheme] \label{defClSu}
	A \textbf{closed subscheme} of $X$ is a triple of
	\begin{itemize}
		\item closed subset $Y \subset X, i \colon Y \hookrightarrow X$
		\item sheaf of rings $\MO_Y$ on $Y$ such that $(Y,\MO_Y)$ is a scheme
		\item surjection $i^\# \colon \MO_X \twoheadrightarrow i_\ast \MO_Y$. ``locally every function on $Y$ is restriction of function on $X$''.
	\end{itemize}
\end{definition}
\begin{definition}[closed immersion]
	A morphism $i \colon Z \to X$ is a \textbf{closed immersion} if 
	\begin{enumerate}
		\item the underlying continuous map of $i$ is a homeomorphism onto a closed subset
		\item $i^\# \colon \MO_X \to i_\ast \MO_Z$ is surjective.
	\end{enumerate}
\end{definition}
\begin{remark}
	Let $i \colon Z \to X$ be a closed immersion. Let $Y=i(Z)$ and $\tilde i \colon Z \to Y$. Define $\MO_Y \coloneqq \tilde i_\ast \MO_Z$. Then, $\tilde i \colon Z \to Y$ is an isomorphism and $Y \subset X$ is a closed subscheme.
\end{remark}
\begin{proposition} \label{ClSuKey}
	Let $X = \Spec A$ and $\ai \subset A$ ideal. Consider $\varphi \colon A \to A/\ai$. Then $f_\varphi \colon \Spec(A/\ai) \to \Spec A$ is a closed immersion.
\end{proposition}
\begin{proof}~ \vspace{-\topsep}
	\begin{enumerate}
		\item The map 
		\begin{align*}
		\Spec(A/\ai) & \longrightarrow V(\ai) \\
		\qi & \longmapsto \varphi^{-1}(\qi)\supset \ai
		\end{align*}
		is an isomorphism and a homeomorphism (\Cref{clIm}).
		\item Check surjectivity of map of schemes on stalks. Let $\pid \in \Spec A$. Then, 
		\[A_p = \MO_{\Spec A,\pid} \twoheadrightarrow i_\ast \MO_{\Spec(A/\ai),\pid} = \begin{cases} 0 & \pid \notin V(\ai) \\ (A/\ai)_{(\pid/\ai)} & \pid \in V(\ai)
		\end{cases}\]
		is surjective. \qedhere 
	\end{enumerate}
\end{proof}
\begin{theorem} \label{ClSU}
	Let $X=\Spec R$, let $Z \overset{f}{\subset} X$ be a closed subscheme. Let \[I = \ker\left(R \xrightarrow{f^\#(\Spec R)} \MO_Z(Z)\right)\] Then there exists a commutative diagram \[\begin{tikzcd}
	Z \arrow{rr}{f} \arrow{dr}{\cong} & & \Spec R \\
	& \Spec(R/I) \arrow[hook]{ur}{j}
	\end{tikzcd}\]
	In particular, every closed subscheme of an affine schemes is affine. 
\end{theorem}
\begin{proof}
	\textbf{Step 1:} Consider \[\begin{tikzcd}
		R \arrow{rr}{f^\#(\Spec R)=\varphi} \arrow{dr} & & \MO_Z(Z) \arrow{dl}{\psi} \\
		& R/I 
	\end{tikzcd}\]
	We can see that $f$ factors as $Z \xrightarrow{\tilde f} \Spec(R/I) \xrightarrow{j} \Spec R$. 
	
	We claim that $\tilde f$ is a closed immersion
	\begin{enumerate}
		\item We know that $f(Z) \subset R$ is closed. Therefore, $f(Z) \subset \Spec(R/I)$ is closed which implies that $|\tilde f|$ is a homeomorphism onto its image.
		\item Consider \[\begin{tikzcd}
			\MO_{\Spec R} \arrow[two heads]{rr} \arrow{dr} & & f_\ast \MO_Z=j_\ast \tilde f_\ast \MO_Z \\
			& j_\ast \MO_{\Spec(R/I)} \arrow{ur}
		\end{tikzcd}\]
		which implies that $\MO_{\Spec(R/I)} \to \tilde f_\ast \MO_Z$ is also surjective.
	\end{enumerate}
	All in all, we can assume that $\varphi \colon R \to \MO_Z(Z)$ is injective.
	
	\textbf{Step 2:} We have $f(Z) \subset X$ be closed and $X$ quasi-compact. Then, $Z$ is quasicompact and therefore $Z=\bigcup_{i = 1}^m \Spec S_i$ for some rings $S_i$.
	
	\textbf{Step 3:} Prove that $f \colon Z \to X$ is surjective.
	
	Since $f(Z)$ is closed it is enough to show that $f(Z)$ is dense.
	If $f(Z)$ is not dense, there exists $g \in R$ such that $D(g) \cap f(Z) = \emptyset$. This implies $f(Z) \subset V(g)$.
	
	This leads to $f^\#(g)(\pid) = 0$ for all $\pid \in Z$. 
	Therefore, $\res{f^\#(g)}{\Spec S_i}$ is nilpotent for all $i$, i.e. $(\res{f^\#(g)}{\Spec S_i})^{n_i}=0$ for some $n_1 \ge 1$.
	Let $n = \max(n_1, \dots , n_m)$. 
	Then, $f^\#(g)^n = f^\#(g^n) = 0$ in $\MO_Z(Z)$ by sheaf property. %Therefore, $f^\#=\varphi$. 
	Since $\varphi = f^\#(\Spec R)$ is injective $\implies g^n = 0 \implies D(g) = \emptyset$ which is a contradiction. 
	
	\textbf{Step 4:} We need to check that $\MO_X \to f_\ast \MO_Z$ is injective. Since then $\MO_X \to f_\ast \MO_Z$ is an isomorphism and $Z \to X$ is an isomorphism. 
	
	Check this on stalks. Let $\pid \in Z, f(\pid) \in X$. Consider \[\begin{tikzcd}\MO_{X,f(\pid)} = R_{f(\pid)} \arrow{r}{\varphi_\pid} & \MO_{Z,\pid} \\
	R\arrow{u}{\operatorname{res}_X} \arrow[hook]{r}{\varphi} & \MO_Z(Z) \arrow{u}{\operatorname{res}_Z} \end{tikzcd}\]
	Let $a \in R$ such that $\varphi_\pid(\operatorname{res}_X(a)) = 0$. We show that then $\operatorname{res}_X(a)=0$ in $R_p$.
	This suffices for injectivity of $\varphi_\pid$ since $\varphi_\pid(1/g)$ is a unit for all $g \in R/f(\pid)$.
	
	We have \[\varphi_\pid(\operatorname{res}_X(a)) = \operatorname{res}_Z(\varphi(a))=0 \,.\]
	Therefore, there exists an open neighbourhood $\pid \in V \subset Z$ such that $\res{\varphi(a)}{V}=0$. Since $f$ is a homeomorphism we can choose $V=f^{-1}(D(t))=D(\varphi(t))$ for some $t \in R$ since $f$ is morphism of locally ringed spaces.
	
	We have \[\res{\varphi(a)}{D(\varphi(t))} = 0\] in $\MO_Z(D(\varphi(t)))$ which implies \[\res{\varphi(a)}{D(\varphi(t)) \cap \Spec S_i} = 0 \in (S_i)_{\sigma_i}\] where $\Spec(S_i)_{\sigma_i} = D(\varphi(t)) \cap \Spec S_i$ with $\sigma_i = \res{\varphi(t)}{\Spec S_i} \in S_i$.
	
	There exists $n_i \ge 1$ such that \[\res{\varphi(a)\varphi(t)^{n_i}}{\Spec S_i} = \res{\varphi(a)}{\Spec S_i}\sigma_i^{n_i} =0 \, .\]
	Let $n = \max(n_1, \dots , n_m)$. Then, $\res{\varphi(a)\varphi(t)^n}{\Spec S_i} = 0$ for all $i$.
	Therefore, $\varphi(at^n)=\varphi(a)\varphi(t)^n=0$ in $\MO_Z(Z)$. 
	This implies $at^n=0$. But $\pid \in V = f^{-1}(D(t))$ so $f(\pid) \in D(t)$ so $t \in R \setminus f(\pid)$ so $a=0$ in $R_{f(\pid)}$.
\end{proof}
\begin{remark}
	The first condition of \Cref{defClSu} cannot be dropped.
	Consider $Y = \A^2_\C \setminus \{ 0 \} \to \A^2_\C$. The second and third conditions are fulfilled but not the first one.
	
	If $Y$ if affine, then the first condition can be dropped. 
\end{remark}
\begin{proposition}
	$f \colon Y \to X$ is  a closed immersion $\iff$ there exists an affine open cover $X=\bigcup U_i$ such that $f^{-1}(U_i)$ is affine and $\MO_X(U_i) \to \MO_Y(f^{-1}(U_i))$ is surjective.
\end{proposition}
\begin{proof}
	``$\implies$'' follows from \Cref{ClSU}. ``$\impliedby$'' follows from \Cref{ClSuKey}.
\end{proof}
\begin{remark}~ \vspace{-\topsep}
	\begin{enumerate}
		\item Composition of closed immersions are closed immersions
		\item Closed immersions are preserved under basechanges (using $B \otimes_A A/I = B/IB$).
	\end{enumerate}
\end{remark}
Let us recall the following result from commutative algebra.
\begin{theorem} 
	$R$ Noetherian and $\dim R = 0$ $\iff$ $R$ Artin $\implies$ there exist finitely many maximal ideals $\mi_1, \dots \mi_k$ and \[R \xrightarrow{\cong} R/\mi_i^{n_i} \times \dots \times R/\mi_k^{n_k}\] for some $n_1, \dots , n_k \ge 1$.
\end{theorem}
\begin{example}
	Closed subschemes of $\A_\C^1 = \Spec \C[x]$. Every closed subscheme is of the form $Z= \Spec \C[x]/I$ (\Cref{ClSU}). 
	If $I=(f)$ for $0 \neq f \in \C[x]$. 
	Let $f=(x-a_1)^{n-1} \dots (x-a_k)^{n_k}$ where $a_i$ are distinct.
	Then, \[\C[x]/I = \C[x]/(x-a_1)^{n_1} \times \dots \times \C[x]/(x-a_k)^{n_k}\,.\]  Therefore, \[Z = \Spec \C[x]/I = \bigsqcup_i \Spec \C[x]/(x-a_i)^{n_i}\] %[picture]
	
	There is a bijection 
	\[\{ \text{closed subscheme of }\A_\C^1 \} \longleftrightarrow \{[\A_C^1]  \} \sqcup \bigsqcup_{n=0}^\infty \operatorname{Sym}^n(\C)\] where $\operatorname{Sym}^n(\C)=\C^n/S_n$.
	
	Let $I \subset \C[x,y]$ such that $R=\C[x,y]/I$ is zero-dimensional. Here we have $R/\mi_i = \C$, so $\varphi^{-1}(\mi_i) = (x-a_i,y-b_i)$ for some $a_i,b_i \in \C$. We can assume $R$ local Artin ring, $\overline \mi$ maximal ideal with $\varphi^{-1}(\overline \mi) = (x,y)$. We get $I \subset \mi = (x,y)$. Also $\overline \mi^l = 0$ for some $l \ge 1$. 
	so $\mi^l \subset I$. Choosing $I$ cooresponds to choosing $\overline I = I/\mi^l \subset \mi/\mi^l$.
	
	Let $d=\dim_\C R$
	\begin{itemize}
		\item $d=1$: Then, $R=\C$ and $\Spec \C = \{ \bullet \}$, reduced point.
		\item $d=2$: Then, $R \supset \overline \mi \supset \overline \mi^2 \supset \dots$. If $\mi^n = \mi^{n+1}$, then $\mi^n =0 $ by Nakayama.
		
		Therefore, $\mi^2 \subset I \subset \mi$. 
		The ideal $I$ corresponds to a subspace $\overline I \subset \mi/\mi^2 = \C\langle x \rangle \oplus \C\langle y \rangle$.
		
		This finally implies $I=(x^2+xy+y^2,ax+by),(a,b) \in \PR^1(\C)$.
		We have a bijection \[\{ \text{length $2$ $0$-dimensional subschemes of } \A_C^2 \} \longleftrightarrow \PR^1(\C) \, .\]
		\item $d=3$ We have $R \supset \overline \mi \supset \overline \mi^2 \supset \overline \mi^3 =0$. 
		
		\textbf{Case 1:} $\overline \mi^2 =0$. Then $I=\overline \mi^2 = (x^2,xy,y^2)$
		
		\textbf{Case 2:} $I=(y+\alpha x + \beta x^2, x^3)$ or $I=(x+\alpha y + \beta y^2, y^3)$
	\end{itemize}
\end{example}
\begin{example}
	$f \in \C[x,y]$ and $X=V(f)$. What is $\{ \text{0-dim subschemes of $X$ supported at $0$} \}$? 
	
	Let's consider $f=xy$. For $d=1$ we have $I = \mi$. For $d=2$ we have $I=(ax+by,x^2+xy+y^2)$. For $d=3$ we have \[I=\begin{cases}
		(x^2,xy,y^2)=\mi^2 \\
		(y+\beta x^2,x^3) \\
		(x + \tilde \beta y^2,y^3)
	\end{cases}\,.\]
\end{example}
