\section{Construction using Injective Resolutions}
We will discuss an equivalent construction for sheaf cohomology theory.

Choose short exact sequences with $\mathcal I^i$ injective
\begin{equation*}
  \begin{tikzcd}
    0 \arrow[r] & \F \ar[r] & \mathcal I^0 \ar[r] & \mathcal I^0/\F \ar[r] & 0  \\
    0 \arrow[r] & \mathcal I^0 / F \ar[r] & \mathcal I^1 \ar[r] & \mathcal I^1/\mathcal I^0 \ar[r] & 0  \\
    0 \arrow[r] & \mathcal I^1 / \mathcal I^0 \ar[r] & \mathcal I^2 \ar[r] & \mathcal I^2/\mathcal I^1 \ar[r] & 0  \\
    & & \dots
  \end{tikzcd}
\end{equation*}
We get an injective resolution $\mathcal I^\bullet$ of $\F$.
\begin{equation*}
  \begin{tikzcd}
    0 \ar[r] & \mathcal F \ar[r] & \mathcal I^0 \ar[r] & \mathcal I^1 \ar[r] & \mathcal I^2 \ar[r] & \dots \; ,
  \end{tikzcd}
\end{equation*}
which is exact.
Define \[\tilde H^i (X,\F) \coloneqq H^i (\Gamma(X,\mathcal I^\bullet)) = H^i(\Gamma(X,\mathcal I^0) \rightarrow \Gamma(X,\mathcal I^1) \rightarrow \dots) \,.\]
Note that $0 \to \Gamma(X,\F) \to \Gamma(X,\mathcal I^0) \to \Gamma(X,\mathcal I^1)$ is exact, so $\tilde H^0(X,\F) = \ker(\Gamma(\mathcal I^0) \to \Gamma (\mathcal I^1)) = \im (\Gamma(\F) \to \Gamma(\mathcal I^0))$, so $\tilde H^0 = \Gamma$ (A1).

Check (A2), (A3).

Let's compare the two constructions.
We have a commutative diagram. 
\begin{equation*}
  \begin{tikzcd}
    &&& 0 \ar[d] & &  \\
    0 \ar[r] & \F \ar[r] & \mathcal I^0 \ar[dr,"d^0"] \ar[r] & \mathcal I^0 / \F \ar[d] \ar[r] & 0 &  \\
    &&& \mathcal I^1 \ar[d] \ar[dr,"d^1"] &&  \\
    && 0 \ar[r] &\mathcal I^1 / \mathcal I^0 \ar[d] \ar[r] &\mathcal I^2 \ar[r] & \dots  \\
    &&& 0 &&
  \end{tikzcd}
\end{equation*}
So
\begin{align*}
  H^1(X,\F) &= \coker (\Gamma(\mathcal I^0) \xrightarrow{\Gamma(d^0)} \Gamma(\mathcal I^0 / \F))  \\
            &=\Gamma(\mathcal I^0 /\F) / \im \Gamma(d^0)  \\
            &= \ker(\Gamma(d^1)) / \im \Gamma(d^0)  \\
            &= H^1(\Gamma(\mathcal I^0) \to \Gamma(\mathcal I^1) \to \Gamma (\mathcal I^2) \to \dots)  \\
            &= \tilde H^1 (X,\F)\,.
\end{align*}
For $i \geq 2$, we can use induction to show that
\begin{align*}
  H^i(X,\F) &= H^{i-1}(X,\I^0/\F)  \\
            &= H^{i-1}(X,\Gamma(\text{inj. res. of }\mathcal I^0 /\F))  \\
            &= H^i(X,\Gamma(\I^\bullet)) \\
            &= \tilde H^1(X,\F)
\end{align*}
We will define a generalization. Let $\mathcal A$, $\mathcal B$ abelian categories.
\begin{definition}[$\delta$-functor]
  An covariant \textbf{$\delta$-functor from $\mathcal A$ to $\mathcal B$} is a collection of functors $T = (T^i)_{i \geq 0} \colon \mathcal A \to \mathcal B$ together with morphisms $\delta \colon T^i (C) \to T^{i+1}(A)$ for all $i$, such that % for all short exact sequences $0 \to A \to B \to C \to 0$ such that
  
\begin{enumerate}
\item For all morphisms of short exact sequences
  \begin{equation*}
    \begin{tikzcd}
      0 \ar[r] & A \ar[d] \ar[r] & B \ar[d] \ar[r] & C \ar[d] \ar[r] &0 \\
      0 \ar[r] & A^\prime  \ar[r] & B^\prime  \ar[r] & C^\prime  \ar[r] &0 \; 
    \end{tikzcd}
  \end{equation*}
  the diagram
  \begin{equation*}
    \begin{tikzcd}
      T^i(C) \ar[r,"\delta"] \ar[d] & T^{i+1} (A) \ar[d]  \\
      T^i(C^\prime) \ar[r,"\delta"] & T^{i+1} (A^\prime)
    \end{tikzcd}
  \end{equation*}
  commutes.
\item For all short exact sequences $0 \to A \to B \to C \to 0$, the sequence $0 \to T^0(A) \to T^0(B) \to T^0(C)  \xrightarrow{\delta} T^1(A) \to T^1(B) \to T^1(C) \xrightarrow{\delta} T^2(A) \to \dots$ is exact.
  Note that $T^0$ is left exact.
\end{enumerate}
\end{definition}
\begin{definition}[universal]
  $T = (T^i)$ is \textbf{universal} iff for any other $\delta$-functor $\tilde T = (\tilde T^i) \colon \mathcal A \to \mathcal B$ and every morhpism $f \colon T^0 \to \tilde T^0$ there is a unique sequence of morphisms $f^i \colon T^i \to \tilde T^i$ such that $f^0 = f$ and which commutes with $\delta$, that is the following diagram commutes:
  \begin{equation*}
    \begin{tikzcd}
      T^i(C) \ar[r,"\delta"] \ar[d,"f^i"] & T^{i+1}(A) \ar[d,"f^{i+1}"]  \\
      \tilde T^i(C) \ar[r,"\delta"] & \tilde T^{i+1}(A)
    \end{tikzcd}
  \end{equation*}
\end{definition}
\begin{remark}~ \vspace{-\topsep}
  \begin{itemize}
  \item Let $F \colon \mathcal A \to \mathcal B$ be left exact.
    If there exists a universal $\delta$-functor $T$ with $T^0 = F$, then such $T$ is unique (up to isomorphism).
  \item For these definitions, $\mathcal A$ does not need to have enough injectives.
  \end{itemize}
\end{remark}
\begin{theorem} \label{undelta}
  Assume that $\mathcal A$ has enough injectives.
  Assume $T = (T^i)$ is a $\delta$-functor such that $T^i(\mathcal I) = 0$ for all $i >0$, $\mathcal I$ injective.
  Then $T$ is universal.
\end{theorem}
\begin{corollary}
  If we take $\mathcal A := \operatorname{Sh}(X)$, $T^0 = \Gamma(X,-)$, then we see that a cohomology theory on a topological space is unique up to unique isomorphism (also using \Cref{enoughinjectives}).
\end{corollary}
\begin{proof}[Proof of \Cref{undelta} (sketch)]
  Let $\tilde T$ be a $\delta$-functor and let $f \colon T^0 \to \tilde T^0$.
  We need to construct $f^i \colon T^i \to \tilde T^i$, $i >0$.
  For $A \in \mathcal A$, choose $0 \to A \to \mathcal I \to \mathcal I /A$ with $\mathcal I$ injective.
  Then we have
  \begin{equation*}
    \begin{tikzcd}
      T^0(\mathcal I) \ar[r] \ar[d,"f"] & T^0(\mathcal I/A) \ar[r, "\delta"] \ar[d,"f"] & T^1(A) \ar[d,dashrightarrow,"\exists ! f^1"] \ar[r] & T^1(\mathcal I) = 0  \\
    \tilde T^0 (\mathcal I) \ar[r] & \tilde T^0(\mathcal I/A) \ar[r,"\delta"] & \tilde T^1(A) \ar[r] & \tilde T^1(\mathcal I) \; .
    \end{tikzcd}
  \end{equation*}
  This defines $f^1$. Show: independent of choice of $\mathcal I$.
  Then use induction
  \begin{equation*}
    \begin{tikzcd}
      0 = T^1(\mathcal I) \ar[d,"f"] \ar[r] & T^1(\mathcal I /A) \ar[d,"f^1"] \ar[r,"\delta","\cong"'] & T^2(A) \ar[d,dashrightarrow,"\exists ! f^2"] \ar[r] & T^2(\mathcal I) = 0 \\
      \tilde T^1(\mathcal I) \ar[r] & \tilde T^1 (\mathcal I/A) \ar[r,"\delta"] & \Tilde T^2(A) \ar[r] & \tilde T^2 (\mathcal I)
    \end{tikzcd}
  \end{equation*}
  etc.
\end{proof}
\begin{theorem}[right derived functors]
  Assume that $\mathcal A$ has enough injectives.
  Then for every left exact functor $F \colon \mathcal A \to \mathcal B$ there exists a universal $\delta$-functor $(R^iF \colon \mathcal A \to \mathcal B)_{i \geq 0}$ with $R^0F = F$.
  The $R^iF$ are called the \textbf{right derived functors} of $F$.
\end{theorem}
\begin{proof}[Proof (Idea).]
  As for sheaf cohomology:
  For $A \in \mathcal A$, choose an injective resolution $0 \to A \to \mathcal I^0 \to \mathcal I^1 \to \dots$.
  Define $R^iF(A) := H^i(F(\mathcal I^\bullet))$.
\end{proof}
Problem: Injective objects are difficult to handle.
\begin{definition}[$F$-acyclic]
  Let $F \colon \mathcal A \to \mathcal B$ be left exact, and let $\mathcal A$ have enough injectives.
  An object $J \in \mathcal A$ is called \textbf{$F$-acyclic} iff $R^iF(J) = 0$ for $i >0$.
\end{definition}
\begin{proposition}
  Assume $\mathcal A$ has enough injectives.
  Let $A \in \mathcal A$ and let \[0 \to A \to J^0 \to J^1 \to \dots \] be an $F$-acyclic resolution of $A$.
  There exists a natural isomorphism \[R^iF(A) \cong H^i(F(J^\bullet)) \,.\]
\end{proposition}
\begin{proof} (sketch)
  Consider $0 \to A \to J^0 \to A/J^0 \to 0$.
  Then $0 \to F(A) \to F(J^0) \to F(J^0/A) \to R^1F(A) \to R^1F(J^0) = 0 \to \dots$.
  Therefore $R^1F(A) = \coker(F(J^0) \to F(J^0/A)) = H^1(F(J^\bullet))$ (Exercise, like comparison of $H^i$ and $\tilde H^i$, uses $F$ left exact).

  For $i \geq 2$, by induction:
  \begin{align*}
    R^iF(A) &= R^{i-1}(F(J^0/A)) & \text{l.e.s.}  \\
            &= H^{i-1}(F(\text{acycl. res. of } J^0/A))  \\
            &=H^i(F(\text{acycl. res. of } A))
  \end{align*}
  which finishes the proof
\end{proof}
Back to sheaf cohomology.

$X$ is a topological space, and $\Gamma(X,-) \colon \operatorname{Sh}(X) \to \mathrm{AbGps}$.

Our goal is to find good acyclic objects in $\operatorname{Sh}(X)$.
\begin{definition}[flabby]
  A sheaf $\F \in \operatorname{Sh}(X)$ is called \textbf{flabby} (or \textbf{flasque}) iff for all $U \supseteq V$ of open subsetes, the restriction map $\F(U) \to \F(V)$ is surjective.
\end{definition}
\begin{lemma}
  Injective sheaves are flabby.
\end{lemma}
\begin{proof}
  Let $V \subseteq U$ open, $j \colon V \hookrightarrow X$, $i \colon U \hookrightarrow X$.
  For $\mathcal G \in \operatorname{Sh}(V)$, let $j_!\mathcal G$ be the sheaf associated to $W \mapsto \mathcal G(W)$ if $W \subseteq V$, and $W \mapsto 0$ else.
  Consider $0 \to j_!\MO_V \to i_!\MO_U \to Q \to 0$ ($Q$ is the quotient).
  Let $\mathcal I$ be an injective sheaf on $X$.
  Then $\Hom(-,\mathcal I)$ is exact.
  So $0 \to \Hom(Q,\mathcal I) \to \Hom(i_!\MO_U,\mathcal I) \to \Hom(j_!\MO_U,\mathcal I) \to 0$, but $\Hom(i_!\MO_U,\mathcal I) \cong \Hom_U(\MO_U,\mathcal I_U) = \Gamma(U,\mathcal I)$, and $\Hom(j_!\MO_V,\mathcal I) \cong \Gamma(V,\mathcal I)$ (Exercise).
\end{proof}
\begin{proposition} \label{flabby->acyclic}
  Flabby sheaves are acyclic for $\Gamma(X,-)$.
\end{proposition}
\begin{lemma} \label{flabbyhelp}
  Let $0 \to \mathcal F \to \mathcal G \to \mathcal H \to 0$ be a short exact sequence in $\operatorname{Sh}(X)$ with $\mathcal F$ flabby.
  \begin{enumerate}[label=(\alph*)]
  \item The sequence $0 \to \Gamma(X,\F) \to \Gamma(X,\mathcal G) \to \Gamma(X, \mathcal H) \to 0$ is exact
  \item If $\mathcal G$ is also flabby, then $\mathcal H$ is flabby as well.
  \end{enumerate}
\end{lemma}
\begin{proof}
	Exercise.
\end{proof}
\begin{proof}[Proof of \Cref{flabby->acyclic}]
  Let $\mathcal F \in \operatorname{Sh}(X)$ be flabby.
  choose $0 \to \F \to \mathcal I \to \mathcal I/\F \to 0$ with $\mathcal I$ injective.
  Then \[0 \longrightarrow \Gamma(\F) \longrightarrow \Gamma(\mathcal I) \longrightarrow \Gamma(\mathcal I/\F) \longrightarrow H^1(X,\F) \to H^1(X,\mathcal I) = 0 \to \dotsb \,.\] Therefore $H^1(X,\F) = 0$.

  For $i \geq 2$:  Note that by \Cref{flabbyhelp}, $\mathcal I/\F$ is flabby.
  Therefore $H^i(X,\F) = H^{i-1}(X,\mathcal I/\F) = 0$ by induction.
\end{proof}
\begin{remark}
  For all $\mathcal \F \in \operatorname{Sh}(X)$, there is a natural embedding $\F \to \mathcal G$ of $\F$ into a flabby sheaf $\mathcal G$ (Homework exc II 1.16e), the \textbf{sheaf of discontinuous sections} of $\mathcal F$, $\mathcal G(U) := \setdef{s \colon U \to \bigcup_{p \in U} \mathcal F_p}{\forall p \in U\colon s(p) \in \F_p}$.
\end{remark}
Some properties of sheaf cohomology are
\begin{itemize}
\item Let $Z \subseteq X$ be a closed subset, $j \colon Z \hookrightarrow X$, $\F \in \operatorname{Sh}(X)$.
  Then $H^i(Z,\F) = H^i(X,j_\ast\F)$ for all $i$.
\item $H^i(X,\F \oplus \mathcal G) = H^i(X,\F) \oplus H^i(X,\mathcal G)$.
\item Let $X$ be noetherian and $\F_\alpha$ be a directed system of sheaves on $X$.
  Then $\varprojlim\limits_{\alpha} H^i(X,\F_\alpha) \cong H^i(X,\varprojlim\limits_{\alpha} \F_\alpha)$.
  In particular, $H^i$ commutes with infinite direct sums.
\item Theorem (Grothendieck): Let $X$ be noetherian of dimension $n$.
  Then for all $\F \in \operatorname{Sh}(X)$, we have $H^i(X,\F) = 0$ for $i >n$.
\end{itemize}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeo_master"
%%% End:
