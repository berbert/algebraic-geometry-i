\begin{lemma}
  $\Spec R$ is quasicompact.
\end{lemma}
\begin{proof}
  Let $\Spec R = \bigcup_{i \in I} U_i$ be an open cover.
  By passing to a subcover we can assume $U_i = D(f_i)$, $f_i \in R$.
  We have $\emptyset = \bigcap_{i \in I} V(f_i) = V\left(\sum_{i \in I} (f_i)\right)$.
  Applying $I$ yields $R = \sqrt{\sum_{i \in I} (f_i)} \ni 1$, therefore $R = \sum_{i \in I} (f_i) \ni 1$.
  Therefore there exist $g_1, \dotsc,g_m \in R$ such that $1= f_{i_1}g_1 + \dotsb + f_{i_m}g_m$, $i_1,\dotsc,i_m \in I$.
  Therefore $\Spec R = D(f_{i_1}) \cup \dotsb \cup D(f_{i_m})$.
\end{proof}
\begin{lemma}
  The assignment
  \begin{align*}
    R \longmapsto & \Spec R \\
    (\varphi \colon R \to S) \longmapsto & (\varphi^\ast \colon \Spec S \to \Spec R )
  \end{align*}
  is a contravariant functor from the category of commutative rings with $1$ and topological spaces.
\end{lemma}
\begin{proof}
  We need to prove that $\varphi^\ast$ is continuous:
  Let $V(\ai) \subseteq \Spec R$ be a closed subset.
  \begin{align*}
    (\varphi^\ast)^{-1}(V(\ai)) &= \setdef{ \pid \in \Spec S}{\varphi^\ast (\pid) \in V(\ai)}  \\
                              &= \setdef{\pid \in \Spec S}{ \varphi^{-1}(\pid) \supseteq \ai}  \\
                              &= \setdef{ \pid \in \Spec S}{\varphi(\ai) \subseteq \pid} = V(\varphi(\ai))
  \end{align*}
  is closed.
\end{proof}
\begin{lemma}
  Let $f \in R$ and consider $\varphi \colon R \to R_f$, the localization.
  Then \[\varphi^\ast \colon \Spec (R_f) \to \Spec (R)\] is an open embedding with image $D(f)$ (meaning $\varphi^\ast$ is a homeomorphism onto an open subset).
\end{lemma}
\begin{proof} Recall the correspondence
  \begin{align*}
    \Spec R_f = \set{\text{prime ideals in } R_f} & \xrightarrow{\varphi^\ast, {\cong^{\mathrm{bij}}}} \set{\text{prime ideals in } \Spec R \text{ which } \not \ni f} = D(f) \\
    \pid & \mapsto \varphi^{-1} (\pid)
  \end{align*}
  By the lemma $\varphi^\ast$ is continuous.
  We need to check that $\varphi^\ast$ is open.
  Let $V(\ai) \subseteq \Spec R_f$ for some $\ai \in \Spec R_f$.
  
  Claim: $\varphi^\ast(V(\ai)) = V(\varphi^{-1}(\ai)) \cap D(f)$, $V(\varphi^{-1}(\ai)) \cap D(f)$ is closed in $D(f)$, as $\varphi^\ast$ is bijective this means that $\varphi^\ast$ is open.

  ``$\supseteq$'': Let $ \pid \in D(f) \cap V(\varphi^{-1} (\ai))$.
  Since $\pid \in D(f)$, there exists a $\qi \in \Spec R_f$ such that $\varphi^{-1}(\qi) = \pid$.
  We have $\varphi^{-1} (\ai) \subseteq \pid = \varphi^{-1}(\qi)$.
  We then get $\varphi(\varphi^{-1}(\ai)) \subseteq \varphi(\pid) \subseteq \qi$, therefore $\qi \in V(\ai)$.%, therefore $\ai = R_f \varphi( \varphi^{-1} (\ai)) \subseteq q$.
  
  ``$\subseteq$'': Exercise.
\end{proof}
\begin{remark} \label{clIm}
  Let $\ai \subseteq R$ be an ideal.
  Consider $\varphi \colon R \to R/\ai$.
  The map $\varphi^\ast \colon \Spec R/\ai \to \Spec R$ is a closed embedding with image $V(\ai)$.
\end{remark}
\section{Structure Sheaf}
Let $X = \Spec R$. We want to define a sheaf on $X$.

What should be a regular function on $X$? It should be an element in $R$.

What should be a regular function on $D(f)$ ($\cong \Spec R_f$)? It should be an element in $R_f$.

Let $\mathfrak B := \setdef{D(f)}{f \in R}$ be a basis of the Zariski topology.
We define a presheaf with respect to $\mathfrak B$ by $\MO_X^{\mathfrak B} (D(f)) \coloneqq R_f$.

Note that
\begin{align*}
  D(f) \subseteq D(g) & \iff V(g) \subseteq V(f)  \\
                      & \iff \sqrt{(f)} \subseteq \sqrt{(g)}  \\
                      & \iff f^m \in \sqrt{(g)} \text{ for some } m\geq 1  \\
                      & \iff f^m = gh \text{ for some } h\in R, \, m \geq 1
\end{align*}
Therefore, the restriction map $\MO_X^{\mathfrak{B}} (D(g)) = R_g \longrightarrow \MO_X^{\mathfrak B} (D(f)) = R_f$ can be defined by
\begin{align*}
  r_{fg} \colon R_g &\longrightarrow R_f  \\
  \frac{a}{g^n}&\longmapsto \frac{ah^n}{(gh)^n} = \frac{ah^n}{f^{mn}} \, .
\end{align*}
The stalk of $\MO_X^{\mathfrak B} $ at $\pid$ is
\begin{equation*}
  \MO_{X,\pid}^{\mathfrak B} = \varinjlim_{\substack{D(f) \in \mathfrak B \\ \pid \in D(f)}} \MO_X^{\mathfrak B} (D(f)) = \varinjlim\limits_{f \notin \pid} R_f = (R/\pid)^{-1}R = R_\pid
\end{equation*}
\begin{proposition}
  $\MO_X^{\mathfrak B}$ is a sheaf of rings with respect to $\mathfrak B$.
\end{proposition}
\begin{proof}
  Let $D(f) = \bigcup_{i \in I} D(f_i)$.
  We need to show:

  \textbf{Uniqueness.}  If $t \in \MO_X^{\mathfrak B} (D(f)) $ such that $\res{t}{D(f_i)} = 0$ for all $i$, then $t=0$

  \textbf{Gluing.}  Let $t_i \in \MO_X^{\mathfrak B} (D(f_i)) $ such that $\res{t_i}{D(f_i) \cap D(f_j)} = \res{t_j}{D(f_i) \cap D(f_j)}$ for all $i,j$.
  Then there exists a $t \in \MO_X^{\mathfrak{B}} (D(f))$ such that $\res{t_i}{D(f_i)} = t_i$.

  \textit{Proof Uniqueness.}
  Let $t = \frac{a}{f^n} \in R_f$. Let
  $\operatorname{Ann} (a) = \setdef{b \in R}{ab = 0}$.
  We claim that $V(\operatorname{Ann} a) = \setdef{\pid \in \Spec R}{a_\pid \neq 0 \in R_\pid}$ (``Support of $a$'').
  \begin{align*}
    a_\pid = 0 & \iff \exists g \in R \setminus \pid, m \geq 1 \colon g^ma = 0 \text{ for some } m\geq 1 \\
            & \iff \exists g \in R\setminus \pid, m \geq 1 \colon g^m \in \operatorname{Ann}(a)  \\
            & \iff R\setminus \pid \cap \sqrt{\operatorname{Ann}(a)} \neq \emptyset \\ %(\iff g \in \sqrt{\operatorname{Ann}(a)})\\
            & \iff \sqrt{\operatorname{Ann}(a)} \not\subseteq \pid \\
            & \iff \pid \notin V(\operatorname{Ann}(a))
  \end{align*}
  which shows the claim.

  Let $t = \frac{a}{f^n}$ such that $\res{t}{D(f_i)} = 0$ for all $i \in I$.
  For all $\pid \in D(f)$ let $i \in I$ such that $\pid \in D(f_i)$.
  Then \[\frac{a_\pid}{ f_\pid^n} = t_\pid = \left(\res{t}{D(f_i)}\right)_\pid = 0_\pid = 0 \in A_\pid\,.\]
  But $f_\pid \in A_\pid$ is a unit in $A_\pid$. This implies $a_\pid = 0$.
  Therefore $V(\operatorname{Ann}(a)) \subseteq V(f)$.
  Therefore $f \in \sqrt{(f)} \subseteq \sqrt{\operatorname{Ann}(a)}$.
  Therefore there exists an $m \geq 1$ such that $f^m \in \operatorname{Ann}(a)$, which is equivalent to $f^m a = 0$.
  Therefore $\frac{a}{f^n} = 0 $ in $R_f$.

  \textit{Proof of Gluing.}

  Step 1:
  We can assume that $f = 1$, hence $D(f) = X$ (otherwise replace $R$ by $R_f$ and $D(f)$ by $\Spec R_f$).
  
  Step 2:
  We can assume that $I$ is finite. (Pf: $X$ is quasicompact, therefore there exist $i_1,\dotsc,i_m \in I$ such that $X = \bigcup_{j=1}^m D(f_{i_j})$.  If $t \in R$ such that $\res{t}{D(f_{i_j})} = t_{i_j}$, then for all $i_0 \in I$ we have \[\res{\left( \res{t}{D(f_{i_0})}\right)}{D(f_{i_0}) \cap D(f_{i_j})} = \res{\left( \res{t}{D(f_{i_j})}\right)}{D(f_{i_0}) \cap D(f_{i_j})} = \res{\left( t_{i_j} \right)}{D(f_{i_0}) \cap D(f_{i_j})} = \res{\left( t_{i_0} \right)}{D(f_{i_0}) \cap D(f_{i_j})}\] by assumption.  By uniqueness we have $\res{t}{D(f_{i_0})} = t_{i_0}$).

  Step 3: Now $X = \bigcup_{i =1}^m D(f_i)$.
  Write $t_i = \frac{a_i}{f^{n_i}} \in R_{f_i}$.
  By extending with powers of $f$ we can assume that there exists $n \geq 1$ such that $t_i = \frac{a_i}{f^n}$.
  By assumption $\frac{a_i}{f_i^m} = \frac{a_j}{f_j^n} \in R_{f_i f_j} = \MO_X^{\mathfrak B} (D(f_i) \cap D(f_j))$.
  Therefore there exists a uniform $M \geq 1$ such that $(f_i f_j)^M (a_i f_j^n - a_j f_i^n) = 0$ in $R$.
  Therefore $a_if_i^M \cdot f_j^{M +n} - a_jf_j^Mf_i^{M+n} = 0$ in $R$.
  Let $\tilde a_i = a_i f_i^M$ and $\tilde n =M+n$.
  Then $\frac{\tilde a_i}{f^{\tilde n}} = \frac{a_i}{f^n} = t_i$, but $\tilde a_j f_i^{\tilde n} - \tilde a_i f_j^{\tilde n} = 0$ in $R$.
  We can assume $a_j f_i^n - a_i f_j^n = 0$ in $R$.

  Step 4:
  $X = \bigcup_{j = 1}^m D(f_j^n) \implies \emptyset = \bigcap_{j=1}^m V(f_j^n)$.
  Via $I$ we then have $\sqrt{(f_1^n,\dotsc,f_m^n)} = R$ therefore there exist $g_1,\dotsc,g_m \in R$ such that $1 = f_1^ng_1 + \dotsb + f_m^n g_m$.
  Let $t = \sum_{j=1}^m a_jg_j \in R$.
  Check that \[\left(\res{t}{D(f_i)} \right) f_i^n = \sum_{j =1}^n f_i ^n a_j g_j = \sum_{j=1}^m f_j^n a_i g_j = a_i\] in $R_{f_i}$ satisfies the required properties.
  %\underline{Step 1:} We need to prove:  Given $t_i \in R_{f_i}$ such that $t_i = t_j$ in $R_{f_if_j}$, there then exists a $t \in R_f$ such that $\res{t}{R_{f_i}} = t_i$.
  %Let $S = R_f$.
  %Equivalent to above:
  %Given $t_i \in S_{f_i} \dots$ there exists $t \in S$ such that \dots (SIC).
\end{proof}
\begin{definition}[structure sheaf]
  The sheaf of rings on $X$ induced by $\MO_X^{\mathfrak B}$ is denoted by $\MO_X$ and called the \textbf{structure sheaf}.

  Concretely, let $U \subseteq X$ be open.
  If $U = D(f)$, then $\MO_X (D(f)) = R_f$.
  For a general $U$:
  \begin{equation*}
    \MO_X(U) = \setdef{s\colon U \to \bigcup_{\pid \in U} R_\pid}{\begin{matrix}\forall \pid \in U \colon s(\pid) \in A_\pid \wedge \exists \text{ cover } U = \cup D(f_i) \text{ and } t_i \in R_{f_i} \\ \text{ s.t. } s(\pid) = (t_i)_\pid \text{ for all } \pid \in D(f_i)\end{matrix}}
  \end{equation*}
\end{definition}
\begin{note} A ``regular function'' is \textbf{not} determined by its values!
\end{note}
\begin{example}
	Let $k[x,y]/(y -x^2) \otimes_{k[x,y]} k[x] = R$. In other words
	$y - x^2 = 0$, $y = 0$, $k[x,y]/(y-x^2,y) = R$.
	
  Then $\Spec R = \{(x,y)\}$, $f = x \in R$.
  Therefore $f(\pid) = 0$ for the unique point $\pid = (x,y) \in \Spec R$, but $f \neq 0$.

  More generally, let $f \in R$ such that $f(\pid)=0$ for all $\pid \in \Spec R$ $\iff f \in \pid$ for all $\pid \in \Spec R$.
  Then $f \in \bigcap_{\pid \subseteq \Spec R} \pid = \operatorname{Nil} R$.
  So $f(\pid) = 0$ for all $\pid \in \Spec R$ if and only if $f \in \operatorname{Nil} R$.
\end{example}
\begin{example}
  Let $R = \C[x]_{(x)}$.
  $\Spec R = \set{(0),(x)} = X$.
  $D(x) = \setdef{\pid}{x \notin \pid}$.
  $\MO_X (D(x)) = (\C[x]_{(x)})_x = \C(x)$.
  $\MO_X (D(1)) = \C[x]_{(x)}$.
\end{example}
\begin{example}
  $R = k[x]$, $k$ algebraically closed.
  $\Spec R = \setdef{(x-a)}{a \in k} \cup \set{(0)}$.
  Let $0 \neq f \in R$.
  Then $\MO_X (D(f)) = k[x]_f$.
  
  The stalks are given by
  \begin{equation*}
    \MO_{X,\pid} =
    \begin{cases}
      k[x]_{(x-a)}, & \text{if }\pid = (x-a) \\
      k[x]_{(0)} = k(x), & \text{if } \pid = (0)
    \end{cases}
  \end{equation*}
\end{example}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeo_master"
%%% End:
