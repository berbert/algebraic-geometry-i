\begin{comment}
Goal: Find invariants of smooth cubic plane curves $C \subseteq \PR_k^2$, $k$ algebraically closed, $\chara k \neq 2,3$.
Idea:
\begin{equation*}
  \begin{tikzcd}
    C \ar[d,"2:1"]  & \text{[Image of $C$]}\\
    \PR^1 & \text{[Image of $\PR^1$]}
  \end{tikzcd}
\end{equation*}
We have
\begin{align*}
  \text{$Q \in C$ branch pt.} & \iff e_Q (f) = v_Q (F^\#(t_p)) >1, \, p = f(Q), \, \text{$t_p$ local param at $p$}  \\
  &\overset{\deg f = 2}{\iff} f^\ast (P) = 2Q \; .
\end{align*}
Let $P_1,\dotsc,P_4 \in \PR^1$ be distinct images of branch points of $f$.
We define
\begin{equation*}
  \cro(P_1,P_2,P_3,P_4) = A\cdot P_4 = \lambda \in k\setminus\set{0,1} \,
\end{equation*}
where $A \in \PGL_2$ is the unique matrix such that $A\cdot P_1 = 0$, $A\cdot P_2 = 1$ and $A \cdot P_3 = \infty$.
How does $\lambda$ depend on choices?
Let $f^\prime \colon C \xrightarrow{2:1} \PR^1$ be another map.
Last time:
\begin{equation*}
  \begin{tikzcd}
    C \ar[r,"\exists \sigma","\sim"'] \ar[d, "f^\prime"'] & C \ar[d,"f"]  \\
    \PR^1 \ar[r,"\exists \tilde\sigma"] & \PR^1 \; .
  \end{tikzcd}
\end{equation*}
\end{comment}
Let $P_1^\prime,\dotsc, P_4^\prime$ be ordered such that
\begin{equation*}
  \begin{tikzcd}
    (P_1^\prime,\dotsc,P_4^\prime) \ar[r,mapsto,"\tilde \sigma"] \ar[dr,"A^\prime = A \circ \sigma"',mapsto] & (P_1,\dotsc,P_4) \ar[d,mapsto,"A"]  \\
    & (0,1,\infty,\lambda)
  \end{tikzcd}
\end{equation*}
We have a dependence on the ordering of $P_1,\dots,P_4$.
Let $0,1,\infty,\lambda$ be fixed.

\begin{lemma} Reorder the points $0,1,\infty$. We claim that \[S_3 \cdot \lambda = \set{\lambda,1/\lambda,1-\lambda,1/(1-\lambda), (\lambda-1)/\lambda,\lambda/(\lambda-1)} \,.\] 
\end{lemma}
\begin{proof}
\begin{align*}
  S_3 &\to \PGL_2  \\
  \sigma &\mapsto A_\sigma \; ,
\end{align*}
where $A_\sigma$ is the matrix corresponding to the mapping that maps $0,1,\infty$ to its permutation according to $\sigma$.
So $S_3$ ats on $\PR^1$.
We need to determine the orbit of $S_3 \cdot \lambda$.
\begin{enumerate}
\item Let $A \in \PGL_2$ such that $A\cdot 0 = \infty$, $A\cdot \infty = 0$ and $A\cdot 1 = 1$.
  Then we must have $A = \begin{pmatrix}0 & 1 \\ 1 & 0\end{pmatrix}$.
  Therefore $A \cdot \lambda = 1/\lambda$.
\item Let $A \in \PGL_2$ such that $A\cdot \infty = \infty$, $A\cdot 0 = 1$ and $A\cdot 1 = 0$.
  Then we must have $A =\begin{pmatrix}-1 & 1 \\ 0 & 1\end{pmatrix}$, so $A \cdot \lambda = 1-\lambda$.
\end{enumerate}
We therefore have the orbit \[S_3 \cdot \lambda = \set{\lambda,1/\lambda,1-\lambda,1/(1-\lambda),1-1/\lambda = (\lambda-1)/\lambda,\lambda/(\lambda-1)}\]
which proves the claim.
\end{proof}
Now, reorder all, $P_1 = 0$, $P_2 = 1$, $P_3 = \infty$ and $P_4 = \lambda$.
\begin{lemma}
  We have $\cro(P_{\sigma(1)},\cdots,P_{\sigma(4)}) \in S_3 \cdot \lambda$ for all $\sigma \in S_4$.
\end{lemma}
\begin{proof}
  Let $A \in \PGL_2$ such that $A \cdot 0 = 0$, $A\cdot \infty = \infty$ and $A\cdot \lambda = 1$.
  Then we must have $A =\begin{pmatrix}1/\lambda & 0 \\ 0 & 1\end{pmatrix}$.
  We need to check that $A\cdot 1 \in S_3 \cdot \lambda$.
  But this is true as $A\cdot 1 = 1/\lambda$.
\end{proof}
What are the invariants of $S_3 \cdot \lambda$, that is, what is $k[\lambda,1/\lambda,1/(\lambda-1)]^{S_3}$?
Recall that $S_3 \curvearrowright \PR^1$.
Let $\Sigma = \PR^1 / S_3$ be the quotient, defined as follows:
If $G$ is a finite group acting on $\Spec A = X$, then $X/G := \Spec(A^G)$.
If $X$ is non-affine, do this affine locally (if possible) and glue.
This is possible if $X$ is quasi-projective.
We know that if $\PR^1$ is normal, then $\PR^1 / S_3$ is normal.
Therefore $\Sigma$ is a normal proper curve.

Facts (proof later):
\begin{enumerate}
\item Let $f \colon C \to C^\prime$ be a finite morphism of normal proper curves.
  Then $p_a(C^\prime) \leq p_a (C)$.
\item $p_a(C) = 0 \iff C \cong \PR^1$.
\end{enumerate}

Consider $\PR^1 \xrightarrow{f} \Sigma$, $f^{-1}(\pt) = S_3 \cdot \lambda$ of degree 6.
Via the first fact $\Sigma$ is of genus $0$, and by the second fact $\Sigma \cong \PR^1$.
So $(\PR^1\setminus\set{0,1,\infty}) / S_3 = \Sigma \setminus \set{\infty} = \PR^1 \setminus \set{\infty} = \A^1 = \Spec k[j]$.
Therefore $k[\lambda,1/\lambda,1/(\lambda-1)]^{S_3} = k[j(\lambda)]$.
What do we know about $j(\lambda)$?
\begin{enumerate}
\item It is unique up to adding a constant and rescaling
\item $f^\ast(\infty) = 2(0) + 2(1) + 2(\infty)$.
  Therefore $j(\lambda)$ has poles at $0$, $1$ and $\infty$ of order $2$, and therefore $j(\lambda) = \frac{p(\lambda)}{\lambda^2(\lambda-1)^2}$, $\deg p(\lambda) = 6$, since $1/\lambda^2$, $1/(\lambda-1)^2$ vanishes at $\infty$ to order $2$.
\item $V(j) = f^\ast(0)$ is of the form $S_3\cdot a$, $a \in k\setminus\set{0,1}$.
  Therefore $p(\lambda) = (\lambda-a)(\lambda-1/a)(\lambda-(1-a))\cdot \dotsb \cdot (\lambda-a/(a-1))$
\end{enumerate}
By adding a constant to $j(\lambda)$, we can choose $a \in k\setminus\set{0,1}$ freely.
Idea: Choose $a$ such that $S_3 \cdot a$ is as samll as possible.

\textbf{Degenerate orbits $S_3 \cdot \lambda$, i.e. $\amount{(S_3 \cdot \lambda)} <6$.}

\textbf{Case 1}: $\lambda = 1/\lambda$, therefore $\lambda^2 = 1$, meaning $\lambda = \pm 1$.
If $\lambda = 1$, then $S_3 \cdot \lambda = \set{0,1,\infty}$.
If $\lambda = -1$, then $S_3 \cdot \lambda = \set{-1,2,1/2}$.

\textbf{Case 2}:
$\lambda = 1-\lambda$, then $\lambda = 1/2$.

\textbf{Case 3}:
$\lambda = 1/(1-\lambda)$, then $-\lambda^2 + \lambda -1 = 0$, or $\lambda^2 - \lambda +1 = 0$.
We have $(\lambda^2-\lambda + 1)(1 + \lambda) = \lambda^3 +1$, therefore $\lambda = -\omega$, where $\omega$ is a cube root of $1$ ($\omega^3 = 1$).
Therefore $S_3 = \set{-\omega,1/(-\omega) = -\omega^2,1+\omega = -\omega^2,\dotsc} = \set{-\omega,-\omega^2}$.
Therefore if $a = -\omega$ then $p(\lambda) = (\lambda^2 - \lambda _1)^3$.
\begin{definition}[$j$-invariant]
  Let $j(\lambda) \coloneqq 2^8 \frac{(\lambda^2-\lambda +1)^3}{\lambda^2(\lambda-1)^2}$ be the invariant of $S_3 \cdot \lambda$, or the \textbf{$j$-invariant of $C$}.
\end{definition}
\begin{theorem}[classification of cubic plane curves]
\begin{enumerate}
\item Let $C$ be a smooth plane cubic, $C \subseteq \PR^2$.
  Then $j(\lambda)$ only depends on the isomorphism class of $C$.
\item
  \begin{align*}
    J \colon \set{\text{cubic plane  curves $C \subseteq \PR^2$}}/\mathrm{Iso} & \to k  \\
    C & \mapsto j(C)
  \end{align*}
\end{enumerate}
\end{theorem}
\begin{proof}
  $J$ is surjective.
  Let $a \in k$, and let $\lambda \in k \setminus \set{0,1}$ such that $j(\lambda) = a$.
  Let $C = V(y^2 = x(x-1)(x-\lambda))$.
  [Maybe image here]
  Ramification at $0$, $1$, $\infty$ and $\lambda$.
  We have $J(C) = j(\lambda) = a$.

  $J$ is injective.
  Let $C$ and $C^\prime$ be such that $j(C) = j(C^\prime)$.
  We have
  \begin{equation*}
    \begin{tikzcd}
      C \ar[d,"f"] & C^\prime \ar[d,"f^\prime"]  \\
      \PR^1 & \PR^1 \; ,
    \end{tikzcd}
  \end{equation*}
  each ramified at $0$, $1$, $\infty$ and $\lambda$.
  Then $j(\lambda) = J(C) = J(C^\prime) = j(\lambda^\prime)$.
  Therefore $\lambda^\prime \in S_3 \cdot \lambda$, so there exists an $A$ such that $A \cdot \lambda = \lambda^\prime$, so by composing $f$ by $A$, we can assume $\lambda = \lambda^\prime$.
  Then \Cref{lem0527} implies the injectivity.
\end{proof}
\begin{lemma}\label{lem0527}
  Let $C$ be a smooth projective curve over $k$, $k$ algebraically closed with $\chara k \neq 0$.
  Let $f \colon C \to \PR^1$ be of degree $2$.
  Then $C$ is determined by (the images of) its branch points
\end{lemma}
\begin{remark}
  $\chara k \neq 2$ means that $f$ is separable ($K(C)/K(\PR^1)$ is separable extension), therefore the branch points make sense.
\end{remark}
\begin{proof}
  $K(C) \overset{f^\#}{\hookleftarrow} k(x) = K(\PR^1)$.
  Let $1,y$ be a basis of $K(C)/k(x)$.
  Then $y^2 = ay+b$, $a,b \in k(x)$.
  By replacing $y$ by $y+ a/2$, we can assume $a=0$, therefore $y^2 = b = p(x)/q(x)$, $p,q \in k[x]$.
  By replacing $y$ by $q(x)y$, we can assume $q=1$, $y^2=p(x)$.
  If $p(x) = r(x)\cdot s(x)^2$, by replacing $y$ by $y/s(x)$, we can assume that $p(x)$ is square-free.
  Therefore $K(C) = k(x)[y]/(y^2=p(x))$, $p\in k[x]$ square-free.
  \begin{claim}
    $X = \Spec k[x,y]/(y^2 = p(x))$ is smooth, hence normal.
  \end{claim}
  \begin{proof}
    $h = y^2 - p(x)$.
    Jacobi-criterion: $h=0$ therefore $p(x)=0$.
    $h_x = -p^\prime(x) = 0$, $h_y = 2y=0$, so $y=0$.
    But $p(x)$ is square-free, so $p(x) = p^\prime(x) = 0$ has no solutions.
  \end{proof}
  Therefore $k[x,y]/(y^2=p(x))$ is the integral closure of $k[x]$ in $K(C)$.
  Therefore $f^{-1}(\A^1_x) = X$.
  Therefore via our calculation, $f(\set{\text{branch pts of $f$}}) \cap \A^1_x = V(p(x))$.
  The branch points of $f$ determine $p(x)$ up to scalar, and the branch points of $X$ determine $X$, hence $C$ ($K(X) = K(C)$).
\end{proof}
\begin{remark}
  \begin{equation*}
    \set{\text{spectral orbits of $j$}} \leftrightarrow \set{\text{degenerate orbits of $S_3 \cdot \lambda$}} \; .
  \end{equation*}

  Case $S_3 \cdot \lambda = \set{0,1,\infty}$:  $\leftrightarrow j(C) = \infty$, which is equivalent to  $C$ is singular.

  Case $S_3 \cdot \lambda = \set{-1,2,1/2}$: $\leftrightarrow j(C) = j(-1) = 2^8 \frac{3^3}{(-1)^2(2)^2} = 2^6 \cdot 3^3 = 1728$, equivalently (skip) $C$ has an extra involution.

  Case $S_3 \cdot \lambda = \set{-\omega,-\omega^3}$: $\leftrightarrow j(C) = j(-\omega) = 0$, equivalently $C$ has an extra automorphism of order $3$ (example $x^3 + y^3 + z^3 = 0$, fermat cubic).

  \begin{equation*}
    \Aut(C,p) = \begin{cases} \Z/2 & j(C) \neq 0,1728  \\ \Z/2 \times \Z /2 & j = 1728  \\ \Z/2 \times \Z/3 & j=0\end{cases}
  \end{equation*}
\end{remark}
(Interchange of fibers of projection to $x$-axis) $C = V(y^2 = x(x-1)(x-\lambda)) \to \PR^1$, $(x,y) \mapsto z$, $p = \infty$.

\textbf{Picard group}:
Let $(C,P_\infty)$ where
\begin{itemize}
\item $X \subseteq \PR^2$ a plane cubic, smooth
\item $P_\infty = [0:1:0] \in C$ such that $C \cap \set{z=0} = 3 P_\infty$.
\end{itemize}
\begin{remark}
  Given any normal proper curve $C$ of geometric genus $1$ and $P\in C(k)$, there exists $i \colon C \hookrightarrow \PR^2$ as above.
\end{remark}
Goal: Determine $\Pic(C)$.
\begin{equation*}
  \begin{tikzcd}
    0 \ar[r] & \Pic^0(C) \ar[r] & \Pic(C) \ar[r,"\deg"] & \Z \ar[r] & 0  \\
    &&\MO_C(\sum_i m_i P_i)  \ar[r,mapsto] & \sum_i m_i
  \end{tikzcd}
\end{equation*}
\begin{theorem} \label{picss} The map
  \begin{align*}
    \varphi \colon C(k) &\to \Pic^0(C)  \\
    P &\mapsto \MO(P-P_\infty)
  \end{align*}
is bijective.
\end{theorem}
\begin{remark}
  $\Pic^{\mathrm{top}}(S^1 \times S^1) = \Z^2$, $C(\mathbb C) \cong S^1 \times S^1$.
\end{remark}
\begin{proof}
  Injectivity,
  Let $P,Q \in C$ such that $\MO(P-P_\infty) \cong \MO(Q-P_\infty)$, which is equivalent to $\MO(P) \cong \MO(Q)$.
\end{proof}
\begin{lemma} \label{picssinj}
  Let $C$ be a smooth projective curve over $k$.
  Let $P,Q \in C$, $P\neq 0$ such that $\MO(P) \cong \MO(Q)$.
  Then $C \cong \PR^1_k$.
\end{lemma}
\begin{proof}
  $\mathcal L \coloneqq \MO(P)$.
  The effective divisors $P \in C$ correspond to $s_P \in H^0(C,\mathcal L)$ such that $V(s_P)=p$.
  The effective divisors $Q \in C$ correspond to $s_Q \in H^0(C,\mathcal L)$ such that $V(s_Q)=Q$.
  Therefore $H^0(C,\mathcal L)$ is of dimension $\geq 2$, hence base point free.
  Therefore there exists $f \colon C \to \PR^1$, $R \mapsto [s_P(R),s_Q(R)]$, the morphism induced by the linear system $\spann (s_P,s_Q)$.
  Then $f^\ast (0) = V(s_P) = P$, and $1 = \deg(P) = \deg(f^\ast0) = \deg(f) \cdot 1$, so $\deg(f) = [K(C) : K(\PR^1)] = 1$, so $C \cong \PR^1$.
\end{proof}
\begin{remark}
  Let $C$ be a smooth projective curve over $k$, $P_\infty \in C$ a fix point.
  \begin{align*}
    \Phi \colon C(k) & \hookrightarrow \Pic^0(C) & \text{Abel Jacobi map}  \\
    p &\mapsto \MO(P-P_\infty) \; .
  \end{align*}
  One can upgrade this to a morphism of schemes $C \hookrightarrow J(C)$, the Jacobian of $C$ ($J(C)(k) = \Pic^0(C) $), abelian variety of dimension $g = g(C)$.
\end{remark}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeo_master"
%%% End:
