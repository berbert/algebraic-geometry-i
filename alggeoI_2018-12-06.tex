\section{Dimensions}
\begin{definition}[dimension]
	Let $X$ be a scheme. We define the \textbf{dimension of $X$} as
	 \[\dim X \coloneqq \topdim X = \sup \setdef{r}{\exists \, \text{chain } Z_0 \subsetneq Z_1 \subsetneq \dots \subsetneq Z_r \subsetneq X, Z_i \subseteq X \text{ irred. closed}} \,.\] 
\end{definition}
\begin{example}
	Let $X = \Spec k[x,y,z]/I$ where $I=(x) \cap (y,z)$. Then $\dim X = 2$. In general, $\dim X = \sup \{ \dim X_i \} $ where $X_i \subseteq X$ are the irreducible components (assume they exist, e.g. $|X|$ Noetherian)
\end{example}
\begin{definition}[codimension] Let $Z \subseteq X$ be an irreducible closed subset. Define the \textbf{codimension of $Z \subseteq X$} as \[\codim(Z,X) \coloneqq \sup \setdef{r}{\exists \, \text{chain } Z = Z_0 \subsetneq Z_1 \subsetneq \dots \subsetneq Z_r \subsetneq X, Z_i \subseteq X \text{ irred. closed}} \, .\]
\end{definition}
\begin{proposition}
	We have $\codim(Z,X) = \dim \MO_{X, \eta}$ where $\eta$ is the generic point of $Z$.
\end{proposition}
\begin{proof}
	We can assume that $X= \Spec A$ is affine. Then, $Z=Z_0 \subset \dots \subset Z_r$ corresponds to a chain \[\pid=\pid_0 \supset \pid_1 \supset \dots \supset \pid_r  \supset \Nil(A)\] which corresponds to \[\pid A_\pid = \overline \pid_0 \supset \overline \pid_1 \supset \dots \supset \overline \pid_r\] in $A_\pid = \MO_{X,\eta}$ with $\eta = \pid$.
\end{proof}
\begin{theorem}[Krull principal ideal theorem] \label{Krull2}
	Let $X$ be a locally Noetherian scheme and $f \in \MO_X(X)$. Then the irreducible components of $V(f)$ are of codimension $0$ or $1$.
\end{theorem}
\begin{note}
	For $Z \subset X$ being an irreducible closed subset we have $\dim (Z) + \codim(Z,X) \le \dim(X)$ but this is not a strict inequality for all $Z \subset X$.
\end{note}
\begin{example}
	Let $A=k[x]_{(x)}[y]$ with $k=\overline k$ which is an integral domain. Then $(0) \subset (x) \subset (x,y)$ is a chain of primes. Therefore, $\dim A \ge 2$.
	Furthermore, $A$ is localization of $k[x,y]$. 
	Therefore, $\dim A \le 2$.
	All in all, $\dim A =2$.
	
	Let $f=(xy-1)$. 
	Then $V(f)=\Spec (A/f)=\Spec k(x)$ which has dimension $0$
	(since $A/(f) = k[x]_{(x)} [x^{-1}] = k(x)$).
	
	By \Cref{Krull2} we have $\codim(V(f),X)=1$. Therefore, \[\dim V(f)+\codim(V(f),X)=1 \neq 2 = \dim X \, .\]
\end{example}
\begin{proposition}
	Let $X$ be irreducible of finite type over a field $k$. Then \[\dim Z + \codim(Z,X)=\dim X\] for all closed irreducible $Z \subset X$. 
\end{proposition}
\section{Reduced Induced Subscheme Structure}
Let $X$ be a scheme and $Z^{\Set} \subset X$ be a closed subset. We want to give $Z^{\Set}$ the structure of a closed subscheme with underlying support $Z^{\Set}$ \[Z=(Z^{\Set},\MO_Z,\MO_X \twoheadrightarrow i_\ast \MO_Z) \,.\]

The easy case is where $X=\Spec A$. Then $Z^{\Set} \subset X \implies Z^{\Set} = V(\ai)$ for some unique radical $\ai \subset A$.

Define $Z \subset X$ to be the closed subscheme associated to the closed immersion $\Spec(A/\ai) \hookrightarrow \Spec A$.

Let us now consider the general case.
\begin{lemma} \label{rissL}
	In the above situation let $U=D(g) \subset \Spec A$. Then $Z \cap D(g) \subset \Spec A_g$ is the reduced induced subscheme structure associated to $Z^{\Set} \cap D(g)$.
\end{lemma}
\begin{proof}
	We know that $Z \cap D(g) \cong \Spec(A / \ai)_{\overline g}$. Therefore the reduced induced subscheme structure of $Z^{\Set} \cap D(g)$ is isomorphic to $\Spec (A_g/\ai A_g)$.
\end{proof}

Let us now consider the general case where $Z^{\Set}=X= \cup_i U_i$ where $U_i = \Spec A_i$. 
Define $Z_i \subset U_i$ to be the reduced induced subscheme structure associated to $Z^{\Set} \cap U_i \subset U_i$.

The intersection $U_i \cap U_j$ is covered by afine opens $V$ which are basic opens for both $U_i$ and $U_j$.
By \Cref{rissL} we have $\res{Z_i}V=\res{Z_j}V$ which is both the reduced induced subscheme structure of $Z^{\Set} \cap V$. 
Therefore, $\res{Z_i}{U_i \cap U_j} \cong \res{Z_j}{U_i \cap U_j}$.

Define $Z$ to be the closed subscheme defined by gluing $Z_i$.
\section{Seperated Morphisms}
Recall that a variety $X$ is seperated $\iff \Delta_X \subset X \times X$ is closed (\Cref{defVar2}).
\begin{definition}[seperated] 
A morphism $f \colon X \to Y$ is called \textbf{seperated} if the induced morphism $\Delta_X \colon X \to X \times_Y X$ in the diagram below is a closed immersion. 
\[\begin{tikzcd}
X \arrow[bend left]{drr}{\id} \arrow[dash]{dr}{\Delta_X} \arrow[bend right]{ddr}{\id} \\
& X \times_Y X \arrow{r}{p_2} \arrow{d}{p_1} & X \arrow{d}{f} \\
& X \arrow{r}{f} & Y
\end{tikzcd}\]
We also say that \textbf{$X$ is seperated over $Y$}. 

If $Y=\Spec \Z$, we say that $X$ is \textbf{seperated}.
\end{definition}
\begin{example}
	If $f \colon X = \Spec A \to Y = \Spec B$. Then $\Delta_X$ is induced by 
	\begin{align*}
		A \otimes_BA \twoheadrightarrow & A \\
		(a_1 \otimes a_2) \longmapsto & a_1 \cdot a_2
	\end{align*}
\end{example}
\begin{example}
	Let $L= \A_k^1 \sqcup \A_k^1/(D(x) \simeq D(y))$ be the line with two origins. 
	The map $L \to \Spec k$ is not seperated.
\end{example}
\begin{proposition}
	$f \colon X \to Y$ seperated $\iff \Delta_X(X) \subset X \times _Y X$ is closed.
\end{proposition}
\begin{proof}
	``$\implies$'': If $\Delta_X$ is a closed immersion. Then $\Delta_X$ is a homeomorphism onto a closed subset.
	
	``$\impliedby$'': Assume that $\Delta_X(X)$ is closed. Then $\res{p_1}{\Delta_X(X)}$ is inverse to $\Delta_X$. Therefore, $\Delta_X$ is a homeomorphism.
	
	Let $\pid \in X, \Delta_X(\pid) \in X \times_Y X$. 
	Consider \[\MO_{X \times_Y X,\Delta_X(\pid)} \to \MO_{X, \pid} \, .\]
	We need to check surjectivity.
	Let $V=\Spec B \subset Y$ be open affine such that $f(\pid) \in V$. 
	Let $U = \Spec A \subset f^{-1}(\Spec B)$ be an open affine such that $\pid \in U$. 
	Then $U \times_V U$ is an open neighbourhood of $\Delta_X(\pid)$. 
	Now $\res{\Delta_X}U = \Delta_U\colon U \to U \times_V U$ is a closed immersion by the previous example. 
	Therefore, the map is surjective.
\end{proof}
\begin{remark}
	The proof shows that $\Delta_X \colon X \to X \times_Y X$ is always a locally closed immersion (it factors as a composition of an open immersion and a closed immersion $X \to \bigcup_{U,V} U \times_V U \to X \times_Y X$).
\end{remark}
\begin{proposition} \label{ChaSep}
 	$X$ seperated over $S$ $\iff$ for all schemes over $S$ and all morphisms $f,g \colon Y \to X$ over $S$, the set $Z=\setdef{y \in Y}{f \circ i_y = g \circ i_y}$ is a closed subset of $Y$ where $i_y \colon \Spec \kappa(y) \to Y$.
\end{proposition}
\begin{lemma} \label{helpSep}
	Let $f,g \colon W \to X$ be two morphisms over $S$. Let $t=f \times_S g \colon W \to X \times_S X$ be the induced map. Then $t$ factors through $\Delta_X$ if and only if $f=g$. 
	\[\begin{tikzcd}
	W \arrow[bend left]{drr}{g} \arrow[dashed]{dr}{t} \arrow[bend right]{ddr}{f} \\
	& X \times_Y X \arrow{r}{p_2} \arrow{d}{p_1} & X \arrow{d}{a} \\
	& X \arrow{r}{a} & Y
	\end{tikzcd}\]
\end{lemma}
\begin{proof}
	``$\implies$'': If $t= \Delta_X \circ \tilde t$ with $\tilde t \colon W \to X$ then \[f=p_1 \circ t = p_1 \circ \Delta_X \circ \tilde t = p_2 \circ \Delta_X \circ \tilde t = p_2 \circ t = g \,.\]
	
	``$\impliedby$'': Define $t^\prime = \Delta_X \circ f \colon W \to X \to X \times_Y X$. Then \[p_1 \circ t^\prime = p_1 \circ \Delta_X \circ f = f = p_1 \circ t\] and similarly $p_2 \circ t^\prime = f = g = p_2 \circ t$ which implies $t=t^\prime$ by the universal property. 
\end{proof}
\begin{proof}[Proof of \Cref{ChaSep}]
	``$\impliedby$'' follows by \[\Delta_X(X) = \setdef{z \in X \times_Y X}{p_1 \circ i_z = p_2 \circ i_z}\] where ``$\supset$'' is true by \Cref{helpSep}.
	For ``$\subset$'' consider $z \in \Delta_X(X)$.
	Since $\Delta_X$ is locally closed immersion it factors as $\Spec \kappa(z) \to X \xrightarrow{\Delta_X} X \times_Y X$ where statement follows by \Cref{helpSep}.
	
	The factorization follows by \[\begin{tikzcd}
		Z \arrow[hook]{r}{\text{closed}} & U \arrow[hook]{r}{\text{open}} & X \times_Y X \\
		& \Spec \kappa(z) \arrow[dashed]{ul} \arrow{u} \arrow{ur}
	\end{tikzcd}\]
	where the dashed arrow comes from \[\begin{tikzcd}
	A \arrow{r} \arrow{dr} & A/I \arrow[dash]{d} \\
	& \kappa(\pid)
	\end{tikzcd}\]
	where $\pid \subset I$ is corresponding to $Z$.
	
	``$\implies$'': We claim that $\setdef{y \in Y}{f \circ i_y = g \circ i_y} = (f \times g)^{-1}(\Delta_X(X))$ (generalization of previous claim).
	``$\subset$'' follows by \Cref{helpSep} (since $(f \times g) \circ i_y=(f \circ i_y) \times (g \circ i_y)$ factors through diagonal).
	For ``$\supset$'' consider $y \in (f \times g)^{-1}(\Delta_X(X))$ and \[\begin{tikzcd}
		X^\prime \arrow{r} \arrow{d}[swap]{\text{loc.cl.im.}} & X \arrow{d}{\Delta_X}[swap]{\text{loc.cl.im.}} \\
		Y \arrow{r}{f \times g} & X \times_S X
	\end{tikzcd}\]
	Using\[\begin{tikzcd}
		\Spec \kappa(y) \arrow{rr} \arrow{dr} & & Y\\ & X^\prime \arrow{ur}
	\end{tikzcd}\]
	This leads to \[\begin{tikzcd}
		\Spec \kappa(y) \arrow{r}{(f \times g) \circ i_y} \arrow{d} & X \times_S X \\
		X^\prime \arrow{r} & X \arrow{u}{\Delta_X}
	\end{tikzcd}\] and hence $f \circ i_y = g \circ i_y$ by \Cref{helpSep}.
\end{proof}
\begin{corollary}
	Let $k$ be algebraically closed. Let $X$ be a prevariety over $k$ and $t(X)$ the associated scheme over $k$. Then $X$ is seperated $\iff$ $t(X)$ is seperated over $k$.
\end{corollary}
\begin{proof}
	Let $z \in t(X) \times_k t(X)$ be a closed point. Then $$z \in \Delta_{t(X)} (t(X)) \iff p_1 \circ i_z = p_2 \circ i_z \iff p_1(z)=p_2(z)$$
	
	Giving $p_1 \circ i_z \colon \Spec k \to t(X)$ is equivalent to giving $p_1(z)=\qi$ and inclusions $\kappa(\qi) \hookrightarrow k$ over $k$. Therefore, we must have $\kappa(\qi)=k$ canonically.
	Therefore, $p_1 \circ i_z$ is determined by $p_1(z)$.
	
	The scheme $t(X)$ is seperated over $k$ $\iff$ $\Delta_{t(X)} (\Delta_X(X))$ is closed in $t(X) \times_k t(X)$ $\iff$ $\{ \text{closed points in } \Delta_{t(X)}(\Delta_X(X)) \}=\setdef{(x,x)}{x \in X}$ is closed in $X \times X$. 
\end{proof}
\begin{corollary}
	There is an equivalence \[\operatorname{Pre-Vars}/k \longleftarrow \text{integral schemes of finite type}/k\] and \[\operatorname{Vars}/k \longleftarrow \text{seperated integral schemes of finite type}/k\]
\end{corollary}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeo_master"
%%% End:
