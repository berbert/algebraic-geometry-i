\section{Flat Morphisms}
Let $A$ be a ring and $M$ and $A$-module. The functor $(-) \otimes_A M$ is right exact but not exact. The category $A-\Mod$ has enough projectives (every free module is projective). 
\begin{definition}[$\Tor$]
	Let $\Tor^i(-, M)$ be the left derived functor of $(-) \otimes M$. So, \[Tor^i(N,M) = H^i(L_N^\bullet \otimes M) \] where $L^\bullet_N$ is a free or projective resolution of $N$. 
\end{definition}
\begin{lemma}
For any short exact sequence \[\begin{tikzcd}
0 \arrow{r} & N \arrow{r} & N^\prime \arrow{r} & N^{\prime \prime} \arrow{r} & 0 
\end{tikzcd} \]
gives rise to a long exact sequence \[\begin{tikzcd}
\dots \arrow{r} & \Tor^1(N^{\prime \prime},M) \arrow{r} & N \otimes M \arrow{r} & N^\prime \otimes M \arrow{r} & N^{\prime\prime} M \arrow{r} & 0
\end{tikzcd} \,.\]
\end{lemma}
\begin{theorem}
	For any ring $A$ and $M,N$ two $A$-modules we have \[\Tor_A^i(N,M) \cong \Tor_A^i(M,N) \,. \]
\end{theorem}
\begin{definition}[flat module] An $A$-module $M$ is \textbf{flat} if $(-) \otimes_A M$ is exact $\iff$ $\Tor_A^1(N,M)=0$ for all $A$-modules $N$ $\iff$ $I \otimes M \to M$ is injective for every finitely generated ideal $I \subset A$. 
\end{definition}
\begin{definition}[flat $\MO_X$-module, flat morphism]
	Let $f \colon X \to Y$ be a morphism of schemes and let $\F$ be an $\MO_X$-module. 
	\begin{enumerate}
		\item $\F$ is \text{flat over $Y$} is for all $x \in X$, $\F_x$ is flat over $\MO_{Y,f(x)}$. 
		\item $f$ is \textbf{flat} if $\MO_X$ is flat over $Y$.
	\end{enumerate}
\end{definition}
\begin{lemma} Let $X=\Spec A$ and $Y=\Spec B$. Then $\F=\widetilde M$ is flat over $Y$ if and only if $M$ is flat over $B$. 
\end{lemma}
\begin{proof}
	The sheaf $\F$ is flat over $Y$ if and only if for all $\pid \in \Spec A$ and $\qi = \pid \cap B = i^{-1}(\pid)$ where $i \colon B \to A$, $M_\pid$ is flat over $B_\qi$. 
	
	``$\impliedby$'': Assume that $M$ is flat over $B$. Let $N^\bullet$ be an exact sequence of $B_\qi$-modules. We have \[N^\bullet \otimes_{B_\qi} M_\pi = N^\bullet \otimes_B M_\pid \cong (N^\bullet \otimes_B M) \otimes_A A_\pid \] where we use $M \otimes_{S^{-1}A} N \cong M \otimes_A N$  for $M,N$ modules over $S^{-1}A$.  and $M_\pid = M \otimes_A A_\pid$. 
	Since $M$ is flat over $B$, $N^\bullet \otimes_BM$ is flat and $\otimes_A A_\pi$ is flat since is is a  localization. 
	
	``$\implies$'': Let \[\begin{tikzcd}
		0 \arrow{r} & N \arrow{r} & N^\prime \arrow{r} & N^{\prime \prime} \arrow{r} & 0
	\end{tikzcd} \]
	exact sequence of $B$-modules. Consider the exact sequence \[\begin{tikzcd}
		0 \arrow{r} & K \arrow{r} & N \otimes_B M \arrow{r} & N^\prime \otimes_B M \arrow{r} & N^{\prime \prime} \otimes_BM \arrow{r} & 0
	\end{tikzcd} \]
	We want to show $K=0$. Enough to show that $K_\pid=0$ for all $\pid \in \Spec A$. We have \[\begin{tikzcd}
	0 \arrow{r} & K_\pid \arrow{r} & (N \otimes_B M)_\pid \arrow{r} & (N^\prime \otimes_B M)_\pid \arrow{r} & (N^{\prime \prime} \otimes_BM)_\pid \arrow{r} & 0
	\end{tikzcd} \]
	Use that \begin{align*}(N \otimes_B M)_\pid \cong N \otimes_B (M \otimes_A A_\pid) &= N \otimes_B (M_\pid) = N \otimes_B(B_\qi \otimes_{B_\qi} M_\pid)\\& = (N \otimes_B B_\qi) \otimes_{B_\qi} M_\pid = N_\qi \otimes_{B_\qi} M_\pid \end{align*}
	So, $((N \otimes M)_\pid \to (N^\prime \otimes M)_\pid)$ is the same as $(N_\qi \hookrightarrow N^\prime_\qi) \otimes_{B_\qi} M_\pid$. The inclusion is injective and $(-) \otimes_{B_\qi} M_\pid$ is exact by assumption. Hence $K_\pid=0$ for all $\pid$ which finally leads to $K=0$ as desired. 
\end{proof}
\begin{lemma}[base change] \label{prop27062}
	Consider the pullback \[ \begin{tikzcd}
	X^\prime \arrow{r}{p} \arrow{d}{f^\prime}& X \arrow{d}{f} \\
	Y^\prime \arrow{r} & Y
	\end{tikzcd}\]
	\begin{enumerate}
		\item If $\F$ is flat over $Y$, then $p^\ast \F$ is flat over $Y^\prime$. 
		\item If $f$ is flat, then so is $f^\prime$. 
	\end{enumerate}
\end{lemma}
\begin{proof}
	Use the algebraic fact that if $M$ is flat over $B$, then $M \otimes_B B^\prime$ is flat over $B^\prime$. 
\end{proof}
\begin{lemma} \label{prop27063}
	Flatness is transitive. 
\end{lemma}
\begin{lemma}[locally freeness] \label{prop27064}
	Let $X$ be a Noetherian scheme, $\F$ coherent. Then $\F$ is flat over $X$ if and only if $\F$ is a locally free $\MO_X$-module.
\end{lemma}
\begin{proof}
	We use the algebraic equivalent. Let $A$ be a local ring and $M$ an $A$-module of finite presentation. Then $M$ is flat if and only if $M$ is free. The proof uses Nakayama's lemma.
\end{proof}
	Consider \[\begin{tikzcd}
		& & \F \\
		x \arrow[mapsto]{d} & X \arrow{d}{f} \arrow[no head]{ur} \\
		f(x)=y & Y
	\end{tikzcd} \]
	Assume that there is $t \in \MO_{Y,f(x)}$ regular (not a zerodivisor). Consider \[\begin{tikzcd}
		0 \arrow{r} & \MO_{Y,f(x)} \arrow[hook]{r}{\cdot t} & \MO_{Y,f(x)}
	\end{tikzcd} \]
	If $\F$ is flat over $Y$, then \[ \begin{tikzcd}0 \arrow{r} & \F_x \arrow{r}{f^{\#}}(t) \arrow{r} & \F_x \end{tikzcd} \] is exact. ``Regular function on $Y$ pullback to $\F$-regular function on $X$.''
\begin{theorem}[dimension] \label{prop27065}
	Let $f \colon X \to Y$ be a morphism of Noetherian schemes of finite dimension. Let $x \in X$, $y=f(x)$. If $f$ is flat, then \[\dim_x(X_y) = \dim_x(X)-\dim_y(Y) \,.\] Here, $x \in X$ and $\dim_x(X) \coloneqq \dim(\MO_{X,x})$. 
\end{theorem}
\begin{proof}
	We first use some reduction steps
	\begin{enumerate}
		\item Let $X=\Spec A$ and $Y=\Spec B$ be affine and $x = \pid$ and $y=\qi$. 
		\item Elements in $B \setminus \qi$ are already inverted in $A_\pid, (A \otimes \kappa(\qi))_\pid$. Can replace $B$ with local $B_\qi$. We can assume that $\qi$ is a maximal ideal.
		\item If $b \in B_\qi$ is nilpotent, it is contained in all prime ideals of $B_\qi, A_\pid$ and $(A \otimes \kappa(\qi))_\pid$. So we can replace $B$ by $B/\text{nilpotents}=B_{\operatorname{red}}$. 
		
		B is a local reduced ring.
	\end{enumerate}	
	Now, we prove the theorem via induction on $\dim B$. 
	
	If $\dim(B)=0$, then $\qi=\mi_B=0$. So, $A \otimes_B \kappa(\qi) = A \otimes_B B/\mi B = A$. Hence, $\dim(A_\pid) = \dim((A/\mi)_\pid)$. 
	
	If $\dim(B)>0$, then there exists $t \in B$ regular. Hence, $\dim(B/(t)) = \dim(B)-1$ and \[\begin{tikzcd}
		0 \arrow{r} & B \arrow{r}{\cdot t} & B
	\end{tikzcd} \]
	is exact and so is (tensoring with $A_\pid$, flatness) \[\begin{tikzcd}
		0 \arrow{r} & A_\pid \arrow{r} & A_\pid \arrow{r} & A_\pid/f^{\#}(t) \arrow{r} & 0
	\end{tikzcd}\]
	This gives \[\dim (A_\pid/(f^{\#}(t))) = \dim(A_\pid)-1 \] But $f^{\#}(t)=0 \in (A_\pid \otimes_B\kappa(\qi))$, so \[\dim (A_\pid \otimes_B \kappa(\qi))/f^{\#}(t) =\dim (A_\pid \otimes \kappa(\qi)) \,. \] All in all, \[\dim A_\pid - \dim B = \dim(A_\pid/f^{\#}(t))- \dim(B/t) = \dim(A_\pid \otimes \kappa(\qi)) \] by induction.
\end{proof}
\begin{corollary}  \label{dimensionfiberflat}
	Let $X,Y$ be irreducible of finite type over $k$. Let $f \colon X \to Y$ be flat. For every point $y \in Y$, $X_y$ is pure of dimension $n=\dim X - \dim Y$ or empty. 
\end{corollary}
\begin{example} The map $f \colon \Bl_0(\A_k^2) \to \A_{k}^2$ is not flat. 
\end{example}
\begin{example}
	Let $A$ be a ring. Consider $A \to S^{-1}A$. Then $(-) \otimes_A S^{-1}A$ is flat. This implies that for $U \subset X$ open, the inclusion $i \colon U \hookrightarrow X$ is flat. 
\end{example}
\begin{proof}[Proof of \Cref{dimensionfiberflat}]
	Let $x \in X_y$ be a closed point. Then \begin{align*}
		\dim_x(X_y) &= \dim_x(X) - \dim_y(Y) \\
		\dim_x(X) &= \dim \MO_{X,x} = \dim(X) - \dim \overline{\set{x}}^X \\
		\dim_y(Y) & = \dim (Y)-\dim \overline{\set{y}}^Y
	\end{align*}
	As a final step, $\kappa(x)$ is a finite algebraic extension over $\kappa(y)$. Hence, \[\dim \overline{\set{x}}= \tr\deg \kappa(x)/k = \tr \deg \kappa(y)/k = \dim \overline{\set{y}} \\,.\]
	Hence, \[\dim_x(X_y) = \dim_x(X)- \dim_y(Y) = \dim (X)-\dim (Y)  \] proving the claim.
\end{proof}
\begin{proposition} \label{prop27066}
	Let $f \colon C \to Y$ be a morphism of Noetherian schemes. Let $\F$ be a coherent sheaf on $X$, flat over $Y$. If $x \in \Ass(\F)$, then $f(x) \in \Ass(\MO_Y)$. 
\end{proposition}
\begin{proof}	
	We have $y=f(x) \in \Ass(\MO_Y) \iff \mi \subset \MO_{X,y}$ is as associated prime for $\MO_{Y,y}$. Hence, $\mi = \Ann(s)$, $s \in \MO_{Y,y}$. This is the case if and only if $\operatorname{depth}(\MO_{Y,y})=0$. hence if $y \notin \Ass(\MO_Y)$, then there exists a regular $t \in \MO_{Y,y}$. We get \[\begin{tikzcd}
		0 \arrow{r} & \MO_{Y,y} \arrow[hook]{r}{\cdot t} \arrow{r} & \MO_{Y,y}  
	\end{tikzcd} \]
	or after tensoring with $\F_x$ we obtain \[\begin{tikzcd}
		0 \arrow{r} & \F_x \arrow[hook]{r}{\cdot f^{\#}(t)} & \F_x
	\end{tikzcd} \]
	Hence, $x$ is not an associated prime of $\F$ (otherwise there would be no $\F_x$-regular element in $\MO_{X,x}$) but $f^{\#}(t) \in \MO_{X,x}$ is $\F_x$-regular. 
\end{proof}
\begin{corollary} Let $C$ be normal integral Noetherian scheme of dimension $1$ (e.g normal curve). Let $f \colon X \to C$ a morphism where $X$ is also Noetherian. Let $\F$ be a coherent sheaf on $C$. Then $\F$ is flat over $C$ if and only if for all $x \in \Ass(\F)$, $\dim(\MO_{X,f(x)})=0$ (i.e. $f(x)$ is a point of codimension $0$). 
\end{corollary}
\begin{proof}
	We have \[\Ass(\MO_C)=\set{\text{generic points of $X$}} \] since $C$ is integral. So ``$\implies$'' follows from \Cref{prop27066}. 
	
	``$\impliedby$'': If $\F$ is not flat, there exists $x  \in X$ such that $\F_x$ is not flat over $\MO_{C,f(x)}$. There exists an ideal $I \subset \MO_{C,f(x)}$ such that $I \otimes \F_x \to \F_x$ is not injective. 
	
	If $\dim \MO_{C,f(x)}=0$, then $\MO_{C,f(x)}$ is a field, so $\F_x$ is flat, so $\dim \MO_{X,f(x)}=1$. Now, $\MO_{C,f(x)}$ is a discrete valuation ring, so $I=(t^a)$, $t \in \MO_{C,f(x)}$ local generator. Hence, $\F_x \xrightarrow{f^{\#}(t^a)} \F_x$ is not injective, so $\F_x \xrightarrow{f^{\#}(t)} \F_x$ is not injective. 
	
	There are two cases
	\begin{enumerate}
		\item If $f^{\#}(t)=0$, then $f^{\#}(t)$ is contained in all prime ideal so in particular in the generic point of $\F_x$, e.g. $\qi$. Since $\qi$ is generic, $\qi \in \Ass(\F_x)$. But $f^{\#}(t) \in \qi$, so $f(\qi)=y$, the right hand side does not hold. 
		\item If $f^{\#}(t) \neq 0$, then there exists $s \in \F_x$ such that $f^{\#}(t)s=0$. Hence, $\Ann(s) \subset \qi$ where $\qi$ is associated prime of $\F_x$. We again have $f^{\#}(t) \in \qi$, so $f(\qi)=y$. \qedhere
	\end{enumerate} 
\end{proof}
\begin{corollary}
	If $X$ is reduced, $f \colon X \to X$ is flat if and only if every irreducible component of $X$ dominate an irreducible component of $C$.
\end{corollary}
\begin{proof}
	We have \[\Ass(\MO_X) = \{\text{generic points of irreducible component} \} \] if $X$ is reduced. 
\end{proof}
\begin{example}
	Consider $X=V(xy-t) \subset \A^2 \times \A^1$ and consider $f \colon \A^2 \times \A^1$ where $f(x,y,t)=t$. The restriction to $f$ to $X$ is flat. $X$ is irreducible and reduced (since smooth) and $f(\eta_X) ) \eta_{\A^1}$. Hence, $f$ is flat. 
\end{example}
\begin{proposition}[Flat limit]
	Let $C$ be a normal curve over $k$ and $p \in C$ be a closed point, $C^0 = C \setminus \{p\}$. Let $W = C \times Y$ for $Y$ a fixed scheme. Assume that $X \subset C^0 \times Y$ is a closed subscheme, flat over $C^0$. Then there exists a unique closed subscheme $\overline X \subset C \times Y$ which is flat over $C$. 
\end{proposition}
\begin{proof}
	Construction: Take $\overline X$ to be scheme theoretical closure of $X$, i.e. let $\I$ be the kernel of \[\begin{tikzcd}
		0 \arrow{r} & \I \arrow{r} & \MO_{C \times Y} \arrow{r} & i_\ast \MO_X
	\end{tikzcd} \]
	and let $\overline X = V(\I) \subset X \times Y$. By construction, $\overline X$  is the smallest closed subscheme of $C \times Y$ containing $X$. If $\overline X$ is not flat over $C$, then there exists $x \in \overline X_p$ such that $f^{\#}(t)$ is not injective in $\MO_{X,x}$ for $t \in \MO_{C,y}$ local parameter. But there we could replace $\overline X$ by $V(\I , \ker(f^{\#}(t)))$ which contradicts minimality. 
\end{proof}
\begin{example}
	Let $C = \A_t^1$ with $\MO_{C \times Y} = \MO_Y[t]$. We have \[\begin{tikzcd}
		0 \arrow{r} & \I_X \arrow[hook]{r} & \MO_Y[t,t^{-1}] %\arrow{r} & 0 
	\end{tikzcd} \]
	where $\I_X$ is the ideal sheaf of $X$. Let $\I = \I_X \cap \MO_Y[t]$. and $\I_{\overline X_0}=\res{\I}{t=0}$. 
	
	For example, consider $L_t$, $L^\prime \subset \PR^3$ lines such that $L_t, L^\prime$ are disjoint for $t \neq 0$ and $L_t,L^\prime$ meet at a point at $t=0$. The local picture in $\A^3_k$ is $L^\prime = (y=z=0)$ and $L_t = (z-t=0,x=0)$.
	
	For $t \neq 0$, $\I_{L^\prime \sqcup L_t} = (y,z) \cap (z-t,x) = (yz,yx,yz-yt,z^2-zt)$. Take limit to $t=0$. Then $\I_{\overline X_0} = (xz,yx,yz,z^2)$.
\end{example}