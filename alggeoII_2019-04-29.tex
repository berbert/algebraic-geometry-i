%\begin{theorem}
%  Let $X$ be a projective scheme over a Noetherian ring $R$, and let $\F$ be a coherent sheaf on $X$.
%  Then $H^1(X,\F)$ is a finitely generated $R$-module for all $i$.
%\end{theorem}
\begin{definition}[Euler characteristic]
  Let $X$ be a projective scheme over a field $k$, and $\F$ a coherent sheaf on $X$.
  We define
  \begin{equation*}
    \chi(\F) = \sum_{i=0}^{\dim X} (-1)^i \dim_k H^i(X,\F) \in \Z \; ,
  \end{equation*}
  the \textbf{(holomorphic) Euler Characteristic}.
\end{definition}
\begin{lemma}~ \vspace{-\topsep} \label{ree}
  \begin{enumerate}
  \item For a short exact sequence $0 \to \F \to \mathcal G \to \mathcal H \to 0$, we have $\chi(\mathcal G) = \chi(\F) + \chi(\mathcal H)$.
  \item For an exact sequence $0 \to \F_1 \to \dotsb \to \F_n \to 0$, we have $\sum_{i} (-1)^i \chi(\F_i) = 0$.
  \end{enumerate}
\end{lemma} 
\begin{lemma}~ \vspace{-\topsep} \label{reev}
  \begin{enumerate}
  \item For $0 \to V_1 \to V_2 \xrightarrow{f} V_3 \to 0$ a short exact sequence of vector spaces, we have $\dim V_2 = \dim V_3 + \dim V_1$.
  \item For an exact sequence $0 \to V_1 \to \dotsb \to V_n \to 0$, we have $\sum_{i} (-1)^i \dim V_i = 0$.
  \end{enumerate}
\end{lemma}
\begin{proof}~ \vspace{-\topsep}
  \begin{enumerate}
    \setcounter{enumi}{1}
  \item Call $V_i \xrightarrow{f_i} V_{i+1} \xrightarrow{f_{i+1}} V_{i+2}$.  %(b)
    Then $0 \to \im f_i \to V_{i+1} \to V_{i+1}/\im f_i \to 0$, and we have $V_{i+1}/\im f \cong V_{i+1} / \ker f_{i+1} \cong \im f_{i+1}$ by exactness.
    Then $\sum_i (-1)^i \dim V_i = \sum_i (-1)^i (\dim \im f_{i-1} + \dim \im f_i )= 0$. \qedhere
  \end{enumerate}
\end{proof}~ \vspace{-\topsep}
\begin{proof}[Proof of \cref{ree}.]
  \begin{enumerate}
  \item Consider long exact sequence $0 \to H^0(X,\F) \to H^0(X,\mathcal G) \to H^0(X,\mathcal H) \to H^1(X,\F) \to \dotsb$ with signs $+-+-\dotso$ and the result follows with $(b)$ of the previous lemma.
  \item This is just the same argument as with the previous lemma. \qedhere
  \end{enumerate}
\end{proof}
\begin{theorem}[Hilbert] \label{hilbert}
  Let $\F$ be a coherent sheaf on $\PR^n$.
  Then there exists a polynomial $P_\F(x) \in \mathbb Q[x]$ called the \textbf{Hilbert polynomial} such that
  \begin{equation*}
    \chi(\PR^n,\F(m)) = P_\F(m)
  \end{equation*}
  for all $m \in \Z$, such that
  \begin{enumerate}
  \item For all $m \gg 0$ we have $P_\F (m) = \dim_k H^0(\PR^n, \F(m))$.
  \item $\operatorname{\deg} P_\F = \dim \supp \F$
  \item Write $P_\F(x) = \sum_{i=0}^r a_r x^r$ with $a_r \neq 0$.
    Then $r! a_r$ is a positive integer.
  \end{enumerate}
\end{theorem}
\begin{example}
  Let $\F = \MO_{\PR^n}(d)$.
  Then $P_{\MO(d)}(m) = \chi(\PR^n,\MO(d+m)) = P_\MO(d+m)$.
  What is $P_\MO(m)$?

  We know that
  \begin{itemize}
  \item $H^0(\PR^n,\MO(d)) = \pol{k}{x}{0}{n}_d$.
  \item $H^i(\PR^n, \MO(d)) = 0$ for all $0 < i < n$.
  \item $H^n(\PR^n,\MO(d)) = \left(\frac{1}{x_0\dotsb x_n} k\left[ \frac{1}{x_0},\dotsc,\frac{1}{x_n}\right]\right)_d \cong H^0(\PR^n,\MO(-n-1-d))$.
  \end{itemize}
  For n=2:
  \begin{center}
    \begin{tabular}{|r|ccccccc|}
    	\hline
      d&$-4$&$-3$&$-2$&$-1$&$0$&$1$&$2$  \\
      \hline
      $h^0(\MO(d))$ & $0$ & $0$ & $0$ & $0$ & $1$ & $3$ & $6$ \\
      $h^1(\MO(d))$ & $0$ & $0$ & $0$ & $0$ & $0$ & $0$ & $0$ \\
      $h^0(\MO(d))$ & $3$ & $1$ & $0$ & $0$ & $0$ & $0$ & $0$ \\
      \hline
      $\chi(\MO(d))$ & $3$ & $1$ & $0$ & $0$ & $1$ & $3$ & $6$ \\
      \hline
    \end{tabular}
  \end{center}
  Then for $m \geq -n$ by the theorem,
  \begin{align*}
    \chi(\MO_{\PR^n}(m)) &= h^0(\PR^n,\MO(m))  \\
                         &= \dim \pol{k}{x}{0}{n}_m \\
                         &= \binom{n+m}{n}  \; ,
  \end{align*}
  which is a polynomial in $m$.

  For $m<-n$, we have
  \begin{align*}
    \chi(\MO(m)) &= (-1)^n h^n(\MO(m))  \\
                 &= (-1)^n h^0(\MO(-m-n-1))  \\
                 &= (-1)^n \frac{(-m-n-1+n)\dotsb(-m-n-1+1)}{n!}  \\
                 &= \binom{m+n}{n}
  \end{align*}
\end{example}
\begin{remark}
  (a) holds automatically by the previous result: $H^i(X,\F(d)) = 0$ for all $i >0$ and $d \gg 0$.

  For (b), we have $\deg P_\MO = n$, and $P_\MO = 1\cdot \frac{m^n}{n!} + \dotsb$.
\end{remark}
\begin{example}
  Consider $\PR^n$ and $f$ homogeneous of degree $d$ and $\Z = V(f) \subseteq \PR^n$.
  What is $P_{\MO_Z}(m) = P_Z(m)$ ?

  Consider
  \begin{equation*}
    \begin{tikzcd}
      0 \ar[r] & \MO_{\PR^n}(-d) \ar[r,"\cdot f"] & \MO_{\PR^n} \ar[r] & \MO_Z \ar[r] & 0 \; .
    \end{tikzcd}
  \end{equation*}
  Tensoring with $\MO_X(m)$:
  \begin{equation*}
    \begin{tikzcd}
      0 \ar[r] & \MO_{\PR^n}(-d+m) \ar[r,"\cdot f"] & \MO_{\PR^n}(m) \ar[r] & \MO_Z(m) \ar[r] & 0 \; ,
    \end{tikzcd}
  \end{equation*}
  and we have
  \begin{align*}
    \chi(\MO_Z(m)) &= \chi(\MO_{\PR}^n(m)) - \chi(\MO_{\PR^n}(m-d))  \\
                   &= \binom{m+n}{n} - \binom{m-d+n}{n}  \\
                   &= \dots  \\
                   &= -\frac{n \cdot (-d)}{n!} m^{n-1} + \mathcal O(m^{n-2})  \\
                   &= d \cdot \frac{m^{n-1}}{(n-1)!} + \mathcal O(m^{n-2}) \; ,
  \end{align*}
  and (b) and (c) are also fulfilled.
\end{example}
\begin{definition}[degree]
  Let $Z \subseteq \PR^n$ be a closed subscheme of dimension $r$.
  Then the \textbf{degree of $Z$ in $\PR^n$} is defined to be \[\deg Z = r! \operatorname{Lead}\operatorname{Coeff}(P_{\MO_Z}) \,. \]
\end{definition}
\begin{remark}
  This depends on the embedding.

  Let $i_1 \colon \PR^1 \hookrightarrow L \subseteq \PR^2$ be the line, then $P_L(m) = m+1$. 
  Let $i_2 \colon \PR^1 \hookrightarrow C \subseteq \PR^2$ be the conic, then $P_C(m) = 2m+1$.
\end{remark}
\begin{example}
	Let $n=2$ and $Z = C = V(f) \subseteq \PR^2$. Then
	\begin{align*}
	\chi_C(\MO_C(m)) &= \binom{m+2}{2} - \binom{m-d+2}{2}  \\
	&= \frac{1}{2} ((m+2)(m+1)-(m-d+2)(m-d_1))  \\
	&= \frac{1}{2}(2 - (-2dm -2d - d + 2 + d^2))  \\
	&= \frac{1}{2}(2dm+3d-d^2)  \\
	&= dm + \frac{1}{2}(3d-d^2)\; .
	\end{align*}
\end{example}
\begin{definition}[genus]
  Let $Z \subseteq \PR^n$ be a closed subscheme of dimension $r$.
  The \textbf{arithmetic genus} of $Z$ is defined to be
  \begin{equation*}
    p_a(Z) = h^r(\MO_Z) - h^{r-1}(\MO_Z) + \dotsb + (-1)^r(h^0(\MO_Z) -1) = (-1)^r(\chi(\MO_Z) -1) \; .
  \end{equation*}
\end{definition}
The arithmetic genus of $C = V(f) \subseteq \PR^2$ is
\begin{equation*}
  p_a(C) = -\left(\frac{1}{2} (3d -d^2)-1\right) = \frac{1}{2}(d-1)(d-2) \; .
\end{equation*}
This is a conlusion of the previous example.

\begin{remark} How can we interpret the genus geometrically?
	
Let $C = V(f) \subseteq \PR^2$ be a degree $d$ be a hypersurface.
Then
\begin{align*}
  p_a(C) &= \frac{1}{2} (d-1)(d-2)  \\
         &=1 + 2 + 3 + \dotsb + (d-2)
\end{align*}
Later: If $C \subseteq \PR^2$ is nonsingular, then $p_a(C) = p_g(C) = h^0(C,\Omega_{C/k})$ ($p_g$ is the geometric genus).

Observation: $p_a(C)$ does not depend on $f \in \pol{k}{x}{0}{n}_d$, it only depends on $d$.
We can then choose $f$ whichever way we want.
Let $f = f_1\cdot \dotsb \cdot f_d$, $f_i$ linear.
Then $C = L_1 \cup \dotsb \cup L_d$.
Assume there are no three lines that meet at the same point.

Let $\tilde C = L_1 \sqcup \dotsb \sqcup L_d$, and let $\tilde C \xrightarrow{\nu} C$ be the normalization.

Claim: $0 \to \MO_Z \to \nu_\ast \MO_Z \to \bigoplus_{x \in \operatorname{Sing}C} k_x \to 0$ is exact.
%(maybe add sketch from Oberdieck here).

Now: $\chi(\MO_C) = \chi(\nu_\ast \MO_Z) - \sum_{z \in \operatorname{Sing}C} \chi(k_x)$, but since $\nu$ is affine, $\chi(\nu_\ast \MO_Z) = \chi(\MO_Z) = \sum_{i=1}^d \chi(\MO_{L_i}) = d$, and we have $\chi(k_x) = 1$ for all $x$ and $\chi(\MO_{\PR^1}) = 1$.

Therefore $\chi(\MO_C) = d - \card{\operatorname{Sing} C}$.
With $s(d) = \card{\operatorname{Sing} C}$, we have $p_a(C) = -1 \cdot (d - s(d) -1) = s(d) -d + 1$

Let $\Gamma$ denote the dual graph of $C$.
The vertices $v_1,\dotsc,v_d$ correspond to components $L_1,\dotsc,L_d$ of $C$.

Two components $L_i \cap L_j \neq \emptyset$ gives us an edge $v_i e v_j$.

$\chi(\Gamma) = \# \mathrm{vertices} - \# \mathrm{edges} = d - s(d)$, and $h^0 (\Gamma) - h^1(\Gamma) = 1 - h^1(\Gamma)$, so $h^1(\Gamma) = 1+s(d) - d = p_a(C)$, therefore $p_a$ is the number of holes in the dual graph.
%(Maybe insert sketch image here)
\end{remark}
\begin{remark}
  If $C \subseteq \PR^1 \times \PR^1$ is a  $\deg(a,b)$ hypersurface, then $p_a(C) = (a-1)(b-1) = \#\mathrm{holes}$.
\end{remark}
  As an application we can prove the following.
  \begin{theorem}[Bezout]
  	Let $C_1 = V(f_1) \subseteq \PR^2$ of degree $d_1$, $C_2 = V(f_2) \subseteq \PR^2$ of degree $d_2$.
  	Assume $Z = C_1 \cap C_2$ is zero-dimensional ($f_1$, $f_2$ have no common factors). Then \[ \sum_{p \in Z} l(\MO_{Z,p}) = d_1d_2 \] where $l(\MO_{Z,p}) = i(C_1,C_2,p)$ is the intersection multiplicity.
  	We have $l(\MO_{Z,p}) = \dim_k(\MO_{Z,p})$ for $k$ algebraically closed.
  \end{theorem}
\begin{proof}
  Consider
  \begin{equation*}
    0 \to \MO_{\PR^2}(-d_1) \xrightarrow{f_1} \MO_{\PR^2} \to \MO_{C_1} \to 0
  \end{equation*}
  Tensor with $\MO_{C_2}$
  \begin{equation*}
    \begin{tikzcd}
      0 \to \MO_{C_2}(-d_1) \xrightarrow{\varphi} \MO_{C_2} \to \MO_{C_1} \otimes \MO_{C_2} \to 0
    \end{tikzcd}
  \end{equation*}
  with $\MO_{C_1} \otimes \MO_{C_2} \cong \MO_Z$.
  Since $f_1$ and $f_2$ have no common factors, for $A = k[x,y,z]$, 
  \begin{equation*}
    (A/(f_2))(-d_1) \xrightarrow{\cdot f_1} A/f_2
  \end{equation*}
  is injective, hence $\varphi$ is injective as $\sim$ is exact.
  Therefore
  \begin{align*}
    \chi(\MO_Z) &= \chi(\MO_{C_2}) - \chi(\MO_{C_2}(-d_1))  \\
                &=P_{C_2}(0) - P_{C_2}(-d_1)  \\
                &= \mathrm{cst} - (-d_1d_2 + \mathrm{cst}) = d_1d_2
  \end{align*}
  On the other hand, $Z$ is zero-dimensional, therefore
  \begin{align*}
    \chi(\MO_Z) &= h^0(\MO_Z)  \\
                &= \sum_{p \in Z} l(\MO_{Z,p}) \; ,
  \end{align*}
  Comparing both results finishes the proof. 
\end{proof}
\begin{comment}
\begin{proof}[Proof of \cref{hilbert}.]
  We will show existence.

  Let $\F = \widetilde M$ for $M$ a finitely generated graded $A$-module, $A = \pol{k}{x}{0}{n}$.
  By Hilbert's basis theorem $M$ is finitely gerated over $k$.
  Consider
  \begin{equation*}
    \begin{tikzcd}
      0 \ar[r] & M_1 \ar[r] & \bigoplus_i A(-d_{0i}) \ar[r] & M \ar[r] & 0
    \end{tikzcd}
  \end{equation*}
  Repeat:
  \begin{equation*}
    \begin{tikzcd}
      0 \ar[r] & M_2 \ar[r] & \bigoplus_i A(-d_{1i}) \ar[r] & M_1 \ar[r] & 0  \\
      \vdots & \vdots & \vdots & \vdots & \vdots
    \end{tikzcd}
  \end{equation*}
  \textbf{Szyzygy theorem}:  This has to stop after $n+1$ steps.
  \textit{(The following might have some errors as I couldn't read Oberdieck's writing in my blurry photograph of the blackboard.)}
  \begin{equation*}
    \begin{tikzcd}
      0 \ar[r] & \bigoplus_i A(-d_{ni}) \ar[r] & \dots \ar[r] & \bigoplus_i A(-d_{0i}) \ar{r} & M \ar[r] & 0
    \end{tikzcd}
  \end{equation*}
  Apply $\sim$:
  \begin{equation*}
    \begin{tikzcd}
      0 \ar[r] & \bigoplus_i \MO(-d_{ni}) \ar[r] & \dots \ar[r] & \bigoplus_i \MO(-d_{0i}) \ar{r} & \tilde M \ar[r] & 0
    \end{tikzcd}
  \end{equation*}
  We have
  \begin{equation*}
    \chi(\F(m)) = \sum_{i,j} (-1)^j \chi(\MO(-d_{ji}+m))
  \end{equation*}
  which is a polynomial in $m$, as $\binom{m-d_{ji} + n}{n}$ is a polynomial in $m$
\end{proof}
\end{comment}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeo_master"
%%% End:
